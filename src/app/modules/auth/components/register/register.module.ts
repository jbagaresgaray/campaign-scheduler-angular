import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { RegisterRoutingModule } from './register-routing.module';
import { RegisterComponent } from './register.component';

import {
  NbAlertModule,
  NbButtonModule,
  NbCheckboxModule,
  NbIconModule,
  NbInputModule,
  NbSpinnerModule,
  NbTooltipModule,
} from '@nebular/theme';

import { ThemeModule } from 'src/app/shared/@theme/@theme.module';

const NB_MODULES = [
  NbAlertModule,
  NbCheckboxModule,
  NbIconModule,
  NbInputModule,
  NbButtonModule,
  NbSpinnerModule,
  NbTooltipModule,
];

@NgModule({
  declarations: [RegisterComponent],
  imports: [
    CommonModule,
    RegisterRoutingModule,
    ...NB_MODULES,
    ThemeModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class RegisterModule {}
