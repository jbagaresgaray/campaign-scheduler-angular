import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  ViewEncapsulation,
} from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { Router } from '@angular/router';
import { emailValidationRegex } from 'src/app/shared/constants/utils';
import { ToastAlertService } from 'src/app/shared/services/toast-alert.service';

import { IUserRegister } from '../../models/auth.model';
import { AuthService } from '../../services/auth.service';
import { USER_ROLE } from './../../../../shared/constants/utils';

@Component({
  selector: 'app-register',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RegisterComponent implements OnInit {
  showMessages: any;
  strategy: string;
  submitted: boolean;
  errors: string[];
  messages: string[];

  registerForm: FormGroup;
  isFormValid = false;
  isSubmitting = false;

  isManager = false;
  isEmployee = false;
  isStaff = false;

  readonly userRole = USER_ROLE;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private toastService: ToastAlertService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.createForm();
  }

  get f(): any {
    return this.registerForm.controls;
  }

  selectUserRole(role: number) {
    if (role === USER_ROLE.MANAGER) {
      this.isManager = true;
      this.isEmployee = false;
      this.isStaff = false;
      this.registerForm.patchValue({
        roleId: USER_ROLE.MANAGER,
      });
    } else if (role === USER_ROLE.SDR) {
      this.isManager = false;
      this.isEmployee = true;
      this.isStaff = false;
      this.registerForm.patchValue({
        roleId: USER_ROLE.SDR,
      });
    } else if (role === USER_ROLE.STAFF) {
      this.isManager = false;
      this.isEmployee = false;
      this.isStaff = true;
      this.registerForm.patchValue({
        roleId: USER_ROLE.STAFF,
      });
    }
  }

  register() {
    this.isSubmitting = true;
    this.registerForm.disable();

    if (this.registerForm.invalid) {
      this.isSubmitting = false;
      this.registerForm.enable();
      this.validateAllFormFields(this.registerForm);
      return;
    }

    const payload: IUserRegister = this.registerForm.getRawValue();

    this.authService
      .register(payload)
      .toPromise()
      .then((response) => {
        if (response.statusCode === 200) {
          this.isSubmitting = false;
          this.toastService
            .showSuccessAlert(
              'Register successful! Please check your email for the initial password'
            )
            .then(() => {
              this.registerForm.reset();
              setTimeout(() => {
                this.router.navigate(['/thank-you'], {
                  queryParams: {
                    id: response.userId,
                  },
                });
              }, 600);
            });
        } else {
          this.registerForm.enable();
          return this.toastService.showErrorToast('Warning!', response.message);
        }
      })
      .catch((error) => {
        if (error) {
          const err = error.error;
          this.registerForm.enable();
          this.isSubmitting = false;

          if (err && err.errors) {
            const errors = err.errors;
            if (errors) {
              this.toastService.showErrorToast('Warning!', errors[0]);
              return;
            }
          } else if (err && err.message) {
            this.toastService.showErrorToast('Warning!', err.message);
            return;
          }
        }
      });
  }

  private createForm() {
    this.registerForm = this.formBuilder.group({
      firstName: [
        { value: '', disabled: this.isFormValid },
        [Validators.required],
      ],
      lastName: [
        { value: '', disabled: this.isFormValid },
        [Validators.required],
      ],
      email: [
        { value: '', disabled: this.isFormValid },
        [Validators.required, Validators.pattern(emailValidationRegex)],
      ],
      roleId: [
        { value: '', disabled: this.isFormValid },
        [Validators.required],
      ],
    });
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
}
