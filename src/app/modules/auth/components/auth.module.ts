import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthComponent } from './auth.component';

import {
  NbThemeModule,
  NbLayoutModule,
  NbCardModule,
  NbIconModule,
} from '@nebular/theme';

import { MiscellaneousModule } from '../../miscellaneous/miscellaneous.module';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { ThemeModule } from 'src/app/shared/@theme/@theme.module';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [AuthComponent],
  imports: [
    CommonModule,
    NbThemeModule,
    NbLayoutModule,
    NbCardModule,
    NbIconModule,
    ThemeModule,
    MiscellaneousModule,
    ComponentsModule,
    RouterModule,
  ],
})
export class AuthModule {}
