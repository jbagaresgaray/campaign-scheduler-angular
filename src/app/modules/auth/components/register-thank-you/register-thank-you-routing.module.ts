import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisterThankYouComponent } from './register-thank-you.component';

const routes: Routes = [{ path: '', component: RegisterThankYouComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegisterThankYouRoutingModule { }
