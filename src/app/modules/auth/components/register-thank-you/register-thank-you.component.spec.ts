import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterThankYouComponent } from './register-thank-you.component';

describe('RegisterThankYouComponent', () => {
  let component: RegisterThankYouComponent;
  let fixture: ComponentFixture<RegisterThankYouComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterThankYouComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterThankYouComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
