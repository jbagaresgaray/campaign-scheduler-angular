import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GUID_UUID_Pattern } from 'src/app/shared/constants/utils';
@Component({
  selector: 'app-register-thank-you',
  templateUrl: './register-thank-you.component.html',
  styleUrls: ['./register-thank-you.component.scss'],
})
export class RegisterThankYouComponent implements OnInit {
  userId: string;

  constructor(private route: ActivatedRoute, private router: Router) {
    this.route.queryParams.subscribe((params) => {
      if (params && params.id) {
        this.userId = params.id;
        if (params.id) {
          // const result = this.validate(params.id);
          // console.log('result: ', result);
          // if (!this.validate(params.id)) {
          //   this.router.navigateByUrl('/login');
          // }
        } else {
          this.router.navigateByUrl('/login');
        }
      } else {
        this.router.navigateByUrl('/login');
      }
    });
  }

  ngOnInit(): void {}

  private validate(uuid: string) {
    if (!GUID_UUID_Pattern.test(uuid)) {
      console.log('1');
      return false;
    }

    const version = this.extractVersion(uuid);
    console.log('version: ', version);
    if (version === undefined) {
      return false;
    }

    switch (version) {
      // For certain versions, the checks we did up to this point are fine.
      case 1:
      case 2:
        return true;

      // For versions 3 and 4, they must specify a variant.
      case 3:
      case 4:
      case 5:
        const result = ['8', '9', 'a', 'b'].indexOf(uuid.charAt(19)) !== -1;
        console.log('result: ', result);
        return result;

      default:
        // We should only be able to reach this if the consumer explicitly
        // provided an invalid version. Prior to extractVersion we check
        // that it's 1-4 in the regex.
        // throw new Error('Invalid version provided.');
        return false;
    }
  }

  private extractVersion(uuid: any) {
    return uuid.charAt(14) | 0;
  }
}
