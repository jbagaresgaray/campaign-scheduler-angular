import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisterThankYouRoutingModule } from './register-thank-you-routing.module';
import { RegisterThankYouComponent } from './register-thank-you.component';


@NgModule({
  declarations: [RegisterThankYouComponent],
  imports: [
    CommonModule,
    RegisterThankYouRoutingModule
  ]
})
export class RegisterThankYouModule { }
