import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  passwordValidationRegex,
  RESET_CHANGE_PASSWORD_ACTION,
} from 'src/app/shared/constants/utils';
import { ToastAlertService } from 'src/app/shared/services/toast-alert.service';
import { ISetPassword } from '../../models/auth.model';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
})
export class ResetPasswordComponent implements OnInit {
  submitted: boolean;
  errors: string[];
  messages: string[];

  user: any = {};

  userId: string;
  action: string;

  showMessages: any = {};

  resetForm: FormGroup;
  isFormValid = false;
  isSubmitting = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private toastService: ToastAlertService
  ) {
    this.route.queryParams.subscribe((params) => {
      if (params && params.id) {
        this.userId = params.id;
      }

      if (params && params.action) {
        this.action = params.action;
      }
    });
  }

  ngOnInit(): void {
    this.createForm();
  }

  get f(): any {
    return this.resetForm.controls;
  }

  showPassword(event: any): void {
    if (event.type === 'text') {
      event.type = 'password';
    } else {
      event.type = 'text';
    }
  }

  resetPass() {
    this.isSubmitting = true;
    this.resetForm.disable();

    const resetFields: ISetPassword = {
      userId: this.userId,
      ...this.resetForm.getRawValue(),
    };

    this.authService
      .setPassword(resetFields)
      .toPromise()
      .then(
        (response) => {
          console.log('response: ', response);
          if (response.statusCode === 200) {
            this.isSubmitting = false;
            this.resetForm.enable();
            this.toastService.showSuccessAlert(response.message).then(() => {
              if (this.action === RESET_CHANGE_PASSWORD_ACTION.CHANGE) {
                this.router.navigateByUrl('/main/dashboard');
              } else if (this.action === RESET_CHANGE_PASSWORD_ACTION.RESET) {
                this.router.navigateByUrl('/login');
              }
            });
          } else if (response.statusCode === 400) {
            this.isSubmitting = false;
            this.resetForm.enable();
            const message = response.message;
            this.toastService.showErrorToast('Warning!', message);
            return;
          }
        },
        (error) => {
          console.log('resetPassword error: ', error);
          if (error) {
            this.isSubmitting = false;
            this.resetForm.enable();
            const err = error.error;
            console.log('resetPassword err: ', err);
            if (err && err.errors) {
              const errors = err.errors;
              if (errors) {
                this.toastService.showErrorToast('Warning!', errors[0]);
                return;
              }
            } else if (err && err.message) {
              this.toastService.showErrorToast('Warning!', err.message);
              return;
            } else if (error && error.message) {
              this.toastService.showErrorToast('Warning!', error.message);
              return;
            }
          }
        }
      );
  }

  private createForm() {
    this.resetForm = this.formBuilder.group({
      currentPassword: ['', [Validators.required, Validators.minLength(6)]],
      newPassword: [
        '',
        [
          Validators.required,
          Validators.pattern(passwordValidationRegex),
          Validators.minLength(6),
        ],
      ],
      confirmPassword: [
        '',
        [
          Validators.required,
          Validators.pattern(passwordValidationRegex),
          Validators.minLength(6),
        ],
      ],
    });
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  private checkPasswords(control: FormControl): { [s: string]: boolean } {
    if (this.resetForm) {
      if (control.value !== this.resetForm.controls.newPassword.value) {
        return { passwordNotMatch: true };
      }
    }
    return null;
  }
}
