import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {
  NbAlertModule,
  NbButtonModule,
  NbCheckboxModule,
  NbFormFieldModule,
  NbIconModule,
  NbInputModule,
  NbSpinnerModule,
} from '@nebular/theme';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';

import { ThemeModule } from 'src/app/shared/@theme/@theme.module';

const NB_MODULES = [
  NbAlertModule,
  NbCheckboxModule,
  NbIconModule,
  NbInputModule,
  NbButtonModule,
  NbSpinnerModule,
  NbFormFieldModule,
];

@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    LoginRoutingModule,
    ...NB_MODULES,
    ThemeModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class LoginModule {}
