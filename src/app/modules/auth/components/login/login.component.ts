import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';

import * as momentTZ from 'moment-timezone';
import { tap } from 'rxjs/operators';

import {
  MAX_ITEMS_PER_PAGE,
  passwordValidationRegex,
  USER_ROLE,
} from 'src/app/shared/constants/utils';

import { AppState } from 'src/app/shared/reducers';
import { ToastAlertService } from 'src/app/shared/services/toast-alert.service';
import { AuthService } from '../../services/auth.service';

import { IUserLogin, IUserAccount } from '../../models/auth.model';
import { Login } from '../../store/auth.actions';
import { RESET_CHANGE_PASSWORD_ACTION } from '../../../../shared/constants/utils';
import { DataLoaderService } from 'src/app/shared/services/data-loader.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  isFormValid = false;
  isSubmitting = false;

  showMessages: any;

  errors: string[];
  messages: string[];

  constructor(
    private router: Router,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private toastService: ToastAlertService,
    private dataLoader: DataLoaderService
  ) {}

  ngOnInit(): void {
    this.createForm();
  }

  get f(): any {
    return this.loginForm.controls;
  }

  showPassword(event: any): void {
    if (event.type === 'text') {
      event.type = 'password';
    } else {
      event.type = 'text';
    }
  }

  login(event: any): void {
    this.isSubmitting = true;

    if (event && event.keyCode === 13 && !this.isFormValid) {
      event.preventDefault();
      event.stopPropagation();
      return;
    }

    if (this.loginForm.invalid) {
      this.isSubmitting = false;
      this.validateAllFormFields(this.loginForm);
      return;
    }

    const payload: IUserLogin = {
      userName: this.loginForm.value.userName,
      password: this.loginForm.value.password,
      timeZone: momentTZ.tz.guess(),
      lastLocalTimeLoggedIn: momentTZ(new Date()).format('YYYY-MM-DD HH:mm:ss'),
    };

    this.authService
      .login(payload)
      .pipe(
        tap((resp: IUserAccount) => {
          this.store.dispatch(new Login({ user: resp }));
        })
      )
      .toPromise()
      .then(
        (response: IUserAccount) => {
          if (response) {
            const loadMoreData = () => {
              if (response.role === USER_ROLE.ADMIN) {
                this.dataLoader.getAllUsers({
                  PageSize: MAX_ITEMS_PER_PAGE,
                });
                this.dataLoader.getAllDepartments({
                  PageSize: MAX_ITEMS_PER_PAGE,
                });
                this.dataLoader.getAllEmployees({
                  PageSize: MAX_ITEMS_PER_PAGE,
                });
                this.dataLoader.getAllManagers({
                  PageSize: MAX_ITEMS_PER_PAGE,
                });
                this.dataLoader.getAllAdmins({
                  PageSize: MAX_ITEMS_PER_PAGE,
                });
                this.dataLoader.getAllCampaigns({
                  PageSize: MAX_ITEMS_PER_PAGE,
                });
              } else if (response.role === USER_ROLE.SDR) {
                this.dataLoader.getActiveScheduler({
                  PageSize: MAX_ITEMS_PER_PAGE,
                });
              }

              setTimeout(() => {
                this.router.navigateByUrl('/main/dashboard');
              }, 600);
            };

            if (response.hasChangePassword) {
              this.dataLoader.refreshProfileData(response).then(
                () => {
                  loadMoreData();
                },
                () => {
                  loadMoreData();
                }
              );
            } else {
              this.router.navigate(['/reset-password'], {
                queryParams: {
                  id: response.id,
                  action: RESET_CHANGE_PASSWORD_ACTION.CHANGE,
                },
              });
            }
          }
        },
        (err) => {
          console.log('err: ', err);
          this.isSubmitting = false;
          if (err) {
            const error = err.error;
            console.log('error: ', error);
            if (error && error.message) {
              return this.toastService.showErrorToast(
                'Warning!',
                error.message
              );
            }
          }
        }
      );
  }

  private createForm() {
    this.loginForm = this.formBuilder.group({
      userName: ['', [Validators.required, Validators.email]],
      password: [
        '',
        [
          Validators.required,
          // Validators.pattern(passwordValidationRegex),
          Validators.minLength(6),
        ],
      ],
    });
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
}
