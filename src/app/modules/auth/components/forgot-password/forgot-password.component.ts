import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
})
export class ForgotPasswordComponent implements OnInit {
  submitted: boolean;
  errors: string[];
  messages: string[];

  user: any = {};

  showMessages: any = {};

  constructor() {}

  ngOnInit(): void {}

  getConfigValue(key: string): any {}

  requestPass() {}
}
