import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent {
  showBack = true;

  constructor(private location: Location, private router: Router) {
    this.router.events.subscribe((val) => {
      // see also
      if (val instanceof NavigationEnd) {
        if (this.router.url === '/login') {
          this.showBack = false;
        } else if (this.router.url.indexOf('thank-you') !== -1) {
          this.showBack = false;
        } else {
          this.showBack = true;
        }
      }
    });
  }

  back() {
    this.location.back();
    return false;
  }
}
