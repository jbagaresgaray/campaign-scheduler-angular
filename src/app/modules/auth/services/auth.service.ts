import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseService } from 'src/app/shared/services/base.service';
import { APIS } from './../../../shared/constants/utils';

import {
  IUserLogin,
  IRefreshToken,
  IUserRegister,
  ISetPassword,
} from '../models/auth.model';
import { MENU_ITEMS } from 'src/app/shared/constants/page.menu';
import { NbMenuItem } from '@nebular/theme';

@Injectable({
  providedIn: 'root',
})
export class AuthService extends BaseService {
  constructor(http: HttpClient) {
    super(http);
  }

  private queryString(params) {
    return Object.keys(params)
      .map((key) => key + '=' + params[key])
      .join('&');
  }

  login(data: IUserLogin): Observable<any> {
    return this.post(APIS.AUTH, '/accounts/login', data);
  }

  register(data: IUserRegister): Observable<any> {
    return this.post(APIS.AUTH, '/accounts/register', data);
  }

  setPassword(data: ISetPassword): Observable<any> {
    return this.post(APIS.AUTH, '/accounts/setpassword', data);
  }

  currentUser(): Observable<any> {
    return this.get(APIS.AUTH, '/accounts/currentuser');
  }

  hasFeatureAccess(role: number, feature: string): boolean {
    const menuItems = MENU_ITEMS;
    const filtered = menuItems.filter((row: NbMenuItem) => {
      return (
        row.data !== undefined &&
        row.data.role &&
        row.data.role.indexOf(role) !== -1
      );
    });
    return true;
  }

  hasPermission(role: number, feature: string, action: string): boolean {
    const menuItems = MENU_ITEMS;
    const filterRoled = menuItems.filter((row: NbMenuItem) => {
      return (
        row.data !== undefined &&
        row.data.role &&
        row.data.role.indexOf(role) !== -1
      );
    });
    const featureAccess: NbMenuItem = filterRoled.find(
      (element) => element.data.key === feature
    );

    if (featureAccess && featureAccess.data) {
      return featureAccess.data.access[action].indexOf(role) !== -1;
    } else if (featureAccess && featureAccess.children) {
      const filterChildRoled = featureAccess.children.filter(
        (row: NbMenuItem) => {
          return (
            row.data !== undefined &&
            row.data.role &&
            row.data.role.indexOf(role) !== -1
          );
        }
      );
      const featureChildAccess: NbMenuItem = filterChildRoled.find(
        (element) => element.data.key === feature
      );
      if (featureChildAccess && featureChildAccess.data) {
        return featureChildAccess.data.access[action].indexOf(role) !== -1;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
}
