export interface IUserLogin {
  userName: string;
  password: string;
  timeZone?: string;
  lastLocalTimeLoggedIn?: string;
  rememberMe?: boolean;
}

export interface IUserAccount {
  hasChangePassword: boolean;
  id: string;
  refreshToken: string;
  role: number;
  timeZone: string;
  token: string;
  username: string;
}

export interface IUserProfile {
  id?: string;
  email?: string;
  firstName?: string;
  lastName?: string;
  image?: string;
  role?: string;
  employeeId?: string;
}

export interface IRefreshToken {
  token?: string;
  refreshToken?: string;
  timeZone?: string;
  lastLocalTimeLoggedIn?: string;
}

export interface IUserRegister {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  confirmPassword: string;
}

export interface ISetPassword {
  userId: string;
  currentPassword: string;
  newPassword: string;
}
