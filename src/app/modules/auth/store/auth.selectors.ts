import { createSelector } from '@ngrx/store';
import { AuthActionTypes } from './auth.actions';

export const authSelectorState = (state) => state.auth;

export const authSelector = createSelector(
  authSelectorState,
  (profileInfo) => profileInfo,
  (auth) => auth.loggedIn
);

export const isLoggedIn = createSelector(
  authSelectorState,
  (auth) => auth.loggedIn
);

export const isLoggedOut = createSelector(isLoggedIn, (loggedIn) => !loggedIn);

export const profileSelector = createSelector(
  authSelectorState,
  (profileInfo) => profileInfo
);

export function clearState(reducer: any) {
  return (state: any, action: any) => {
    if (action.type === AuthActionTypes.LogoutAction) {
      state = undefined;
    }

    return reducer(state, action);
  };
}
