import { AuthActions, AuthActionTypes } from './auth.actions';
import { IUserAccount, IUserProfile } from '../models/auth.model';

export interface AuthState {
  loggedIn: boolean;
  user: IUserAccount;
  profile: IUserProfile;
}

export const initialAuthState: AuthState = {
  loggedIn: false,
  user: undefined,
  profile: undefined,
};

export function authReducer(
  state = initialAuthState,
  action: AuthActions
): AuthState {
  switch (action.type) {
    case AuthActionTypes.LoginAction:
      return Object.assign({}, state, {
        loggedIn: true,
        user: action.payload.user,
        profile: null
      });

    case AuthActionTypes.LogoutAction:
      return Object.assign({}, state, {
        loggedIn: false,
        user: null,
        profile: null
      });

    case AuthActionTypes.registerAction:
      return Object.assign({}, state, {
        loggedIn: false,
        user: null,
        profile: null
      });

    case AuthActionTypes.profileInfoAction:
      return Object.assign({}, state, {
        loggedIn: true,
        profile: action.payload.profileInfo,
        user: action.payload.user
      });

    default:
      return state;
  }
}
