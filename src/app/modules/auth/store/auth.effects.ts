import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { tap } from 'rxjs/operators';
import { defer, of } from 'rxjs';
import { Router } from '@angular/router';

import {
  Login,
  AuthActionTypes,
  Logout,
  ProfileInfoAction,
} from './auth.actions';

import {
  CURRENT_USER,
  CURRENT_USER_TOKEN,
  CURRENT_USER_PROFILE
} from '../../../shared/constants/utils';

import { LocalStorageService } from '../../../shared/services/local-storage.service';


@Injectable()
export class AuthEffects {
  constructor(
    private actions$: Actions,
    private router: Router,
    private storage: LocalStorageService,
  ) {}

  @Effect()
  init$ = defer(() => {
    const userData = this.storage.getItem(CURRENT_USER);
    if (userData && userData !== 'undefined') {
      return of(new Login(userData));
    } else {
      return of();
    }
  });

  @Effect({ dispatch: false })
  public login$ = this.actions$.pipe(
    ofType<Login>(AuthActionTypes.LoginAction),
    tap((action) => {
      this.storage.setItem(CURRENT_USER, action.payload.user);
    })
  );

  @Effect({ dispatch: false })
  logout$ = this.actions$.pipe(
    ofType<Logout>(AuthActionTypes.LogoutAction),
    tap(() => {
      this.storage.removeItem(CURRENT_USER);
      this.storage.removeItem(CURRENT_USER_TOKEN);
      this.storage.removeItem(CURRENT_USER_PROFILE);

      setTimeout(() => {
        this.router.navigateByUrl('/login');
      }, 600);
    })
  );

  @Effect({ dispatch: false })
  public profileInfo$ = this.actions$.pipe(
    ofType<ProfileInfoAction>(AuthActionTypes.profileInfoAction),
    tap((action) => {
      this.storage.setItem(
        CURRENT_USER_PROFILE,
        action.payload.profileInfo
      );
    })
  );
}
