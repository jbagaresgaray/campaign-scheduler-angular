import { Component, OnDestroy, ViewEncapsulation } from '@angular/core';
import { NbMenuItem, NbSidebarService } from '@nebular/theme';
import { select, Store } from '@ngrx/store';
import { take } from 'rxjs/operators';
import filter from 'lodash-es/filter';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { BsModalService } from 'ngx-bootstrap/modal';

import { MENU_ITEMS, MENU_ITEM_NAME } from 'src/app/shared/constants/page.menu';
import { DataLoaderService } from 'src/app/shared/services/data-loader.service';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { IUserAccount, IUserProfile } from '../../auth/models/auth.model';
import { AuthState } from '../../auth/store/auth.reducers';
import {
  isLoggedIn,
  isLoggedOut,
  profileSelector,
} from '../../auth/store/auth.selectors';
import {
  ACTIVE_CAMPAIGN,
  CURRENT_USER,
  FORMSTATE,
  MAX_ITEMS_PER_PAGE,
  PERMISSION_ACTION_DESC,
  USER_ROLE,
} from 'src/app/shared/constants/utils';
import { PubsubService } from 'src/app/shared/services/pubsub.service';
import { ICampaign } from '../models/campaign.model';
import { SchedulerService } from '../services/scheduler.service';
import { IEmployees } from '../models/employee.model';
import { AuthService } from '../../auth/services/auth.service';
import { CampaignEntryComponent } from './campaign/components/campaign-entry/campaign-entry.component';
import { ToastAlertService } from 'src/app/shared/services/toast-alert.service';
import { UserService } from 'src/app/shared/mocks/services/employee.mock';

@Component({
  selector: 'app-main',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnDestroy {
  menu: NbMenuItem[] = [];
  user: IUserAccount;
  profile: IUserProfile;
  campaign: ICampaign;
  manager: IEmployees;

  private destroy$: Subject<void> = new Subject<void>();

  isLoggedIn$: Observable<boolean>;
  isLoggedOut$: Observable<boolean>;
  campaignInfoSidebar = false;
  campaignInfoSidebarButton = false;
  managerInfoSidebarButton = false;

  private unsubscribe$ = new Subject<any>();
  readonly PERMISSION_ACTION_DESC = PERMISSION_ACTION_DESC;

  constructor(
    private store: Store<AuthState>,
    private dataLoader: DataLoaderService,
    private storage: LocalStorageService,
    private sidebarService: NbSidebarService,
    private pubsub: PubsubService,
    private schedulerService: SchedulerService,
    private authService: AuthService,
    private modalService: BsModalService,
    private toastr: ToastAlertService,
    private mockUserService: UserService
  ) {
    this.initApp();
    this.filterNavigationsByRoles();

    this.sidebarService.onToggle().subscribe((resp) => {
      if (resp.tag === 'settings-sidebar') {
        this.campaignInfoSidebar = !this.campaignInfoSidebar;
      }
    });

    this.pubsub.$sub('settings-sidebar', (val) => {
      if (val === 'campaign') {
        this.campaignInfoSidebarButton = true;
      } else if (val === 'manager') {
        this.managerInfoSidebarButton = true;
      } else {
        this.campaignInfoSidebarButton = false;
        this.managerInfoSidebarButton = false;
      }
    });

    this.pubsub.$sub(ACTIVE_CAMPAIGN, () => {
      this.campaign = this.schedulerService.activeCampaign;
    });

    this.pubsub.$sub('selected-sdrm', (manager) => {
      this.manager = manager;
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  toggleNewCampaign() {
    this.user = this.storage.getItem(CURRENT_USER);
    if (this.user.role !== USER_ROLE.ADMIN) {
      return this.toastr.showWarningToast(
        'WARNING!',
        'Invalid access due to invalid role'
      );
    }

    const initialState = {
      action: FORMSTATE.ADD,
    };
    this.modalService.show(CampaignEntryComponent, {
      initialState,
      class: 'modal-lg',
    });
  }

  hasFeatureAccess(action: string): boolean {
    this.user = this.storage.getItem(CURRENT_USER);
    if (this.user) {
      return this.authService.hasPermission(
        this.user.role,
        MENU_ITEM_NAME.CAMPAIGN.TAG,
        action
      );
    } else {
      return false;
    }
  }

  private filterNavigationsByRoles() {
    const menuItems = MENU_ITEMS;
    this.store
      .select(profileSelector)
      .pipe(takeUntil(this.destroy$))
      .subscribe((user: any) => {
        if (user && (user.profile || user.user)) {
          this.user = user.user;
          this.profile = user.profile;

          if (this.user) {
            const filtered = filter(menuItems, (row: NbMenuItem) => {
              return (
                row.data !== undefined &&
                row.data.role &&
                row.data.role.indexOf(this.user.role) !== -1
              );
            });
            this.menu = filtered;

            this.initPendingApprovals();
          }
        }
      });
  }

  private initApp() {
    this.isLoggedIn$ = this.store.pipe(select(isLoggedIn));
    this.isLoggedOut$ = this.store.pipe(select(isLoggedOut));
    const user: IUserAccount = this.storage.getItem(CURRENT_USER);
    this.isLoggedIn$.pipe(take(1)).subscribe((response) => {
      if (response) {
        // if (user.role === USER_ROLE.SDR || user.role === USER_ROLE.MANAGER) {
        //   this.campaignInfoSidebarButton = true;
        // } else {
        //   this.campaignInfoSidebarButton = false;
        // }

        setTimeout(() => {
          if (user.role === USER_ROLE.ADMIN) {
            this.dataLoader.getAllUsers({
              PageSize: MAX_ITEMS_PER_PAGE,
            });
            this.dataLoader.getAllDepartments({
              PageSize: MAX_ITEMS_PER_PAGE,
            });
            this.dataLoader.getAllEmployees({
              PageSize: MAX_ITEMS_PER_PAGE,
            });
            this.dataLoader.getAllManagers({
              PageSize: MAX_ITEMS_PER_PAGE,
            });
            this.dataLoader.getAllAdmins({
              PageSize: MAX_ITEMS_PER_PAGE,
            });
            this.dataLoader.getAllCampaigns({
              PageSize: MAX_ITEMS_PER_PAGE,
            });
            this.dataLoader.getAllStaffs({
              PageSize: MAX_ITEMS_PER_PAGE,
            });
          } else if (user.role === USER_ROLE.SDR) {
            this.dataLoader.getActiveScheduler({
              PageSize: MAX_ITEMS_PER_PAGE,
            });
          }

          this.dataLoader.refreshProfileData(user);
        }, 1000);
      }
    });
  }

  private initPendingApprovals() {
    this.mockUserService.getPendingApprovals().subscribe((response) => {
      const pendingArr = [...response];
      this.menu = this.menu.map((item) => {
        if (item.data.key.indexOf('pending approval') != -1) {
          return {
            ...item,
            ...{
              badge: {
                status: 'primary',
                text: pendingArr.length.toString(),
              },
            },
          };
        } else {
          return {
            ...item,
          };
        }
      });
    });
  }
}
