import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Subject, Subscription } from 'rxjs';
import { filter, map, takeUntil } from 'rxjs/operators';
import { select, Store } from '@ngrx/store';
import { NbMenuItem, NbMenuService } from '@nebular/theme';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import cloneDeep from 'lodash-es/cloneDeep';
import isNull from 'lodash-es/isNull';
import size from 'lodash-es/size';
import { GridOptions } from 'ag-grid-community';
import { AgGridAngular } from 'ag-grid-angular';

import { CampaignEntryComponent } from './components/campaign-entry/campaign-entry.component';
import { CampaignManagerEntryComponent } from './components/campaign-manager-entry/campaign-manager-entry.component';

import {
  IUserAccount,
  IUserProfile,
} from 'src/app/modules/auth/models/auth.model';
import { MENU_ITEM_NAME } from 'src/app/shared/constants/page.menu';
import {
  CAMPAIGN_STATUS,
  CURRENT_USER,
  CURRENT_USER_PROFILE,
  FORMSTATE,
  GLOBAL_AG_GRID_OPTIONS,
  PERMISSION_ACTION_DESC,
  USER_ROLE,
} from 'src/app/shared/constants/utils';
import { MainState } from '../../store/main.reducers';
import { DataLoaderService } from 'src/app/shared/services/data-loader.service';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { MainService } from 'src/app/modules/main/services/main.service';
import { AuthService } from 'src/app/modules/auth/services/auth.service';
import { IGenericParams } from './../../models/generic.model';
import {
  ADMIN_CAMPAIGNS,
  MAX_ITEMS_PER_PAGE,
} from 'src/app/shared/constants/utils';
import { ICampaignResponse, ICampaigns } from './../../models/campaign.model';
import { mainSelector } from '../../store/main.selectors';
import { ECampaignStatus } from 'src/app/shared/constants/enums';
import { LoadingController } from '@ionic/angular';
import { SchedulerService } from '../../services/scheduler.service';
import { CAMPAIGN_COLUMNS } from './campaign.constant';
import { CampaignActionButtonRendererComponent } from './components/campaign-action-button-renderer/campaign-action-button-renderer.component';
import { StatusCellRendererComponent } from 'src/app/shared/components/status-cell-renderer/status-cell-renderer.component';
import { DateFormatCellRendererComponent } from 'src/app/shared/components/date-format-cell-renderer/date-format-cell-renderer.component';

@Component({
  selector: 'app-campaign',
  templateUrl: './campaign.component.html',
  styleUrls: ['./campaign.component.scss'],
})
export class CampaignComponent implements OnInit {
  user: IUserAccount;
  profile: IUserProfile;

  campaignArr: ICampaigns[] = [];
  campaignStatusArr: NbMenuItem[] = [];
  selectedCampaign: ICampaigns;

  fakeArr: any[] = [];
  columnDefs: any[] = [];

  itemsPerPage = MAX_ITEMS_PER_PAGE;
  totalItems: number;
  currentPage = 1;

  showContent = false;

  readonly PERMISSION_ACTION_DESC = PERMISSION_ACTION_DESC;
  readonly USER_ROLE = USER_ROLE;
  readonly ECampaignStatus = ECampaignStatus;

  private gridApi: any;
  frameworkComponents = {
    actionButtonRenderer: CampaignActionButtonRendererComponent,
    dateCellRenderer: DateFormatCellRendererComponent,
    statusCellRenderer: StatusCellRendererComponent,
  };
  gridOptions: GridOptions = GLOBAL_AG_GRID_OPTIONS;
  @ViewChild('agGrid') agGrid: AgGridAngular;

  private destroy$ = new Subject<any>();
  private unsubscribe$ = new Subject<any>();
  private deleteSubs$: Subscription = new Subscription();
  private updateStatusSub$: Subscription = new Subscription();

  constructor(
    private router: Router,
    private modalService: BsModalService,
    private mainStore: Store<MainState>,
    private dataLoader: DataLoaderService,
    private storage: LocalStorageService,
    private mainService: MainService,
    private authService: AuthService,
    private menuService: NbMenuService,
    private schedulerService: SchedulerService,
    private loadingController: LoadingController
  ) {
    this.fakeArr = Array.from({
      length: 15,
    });

    for (const [key, value] of Object.entries(CAMPAIGN_COLUMNS)) {
      this.columnDefs.push({
        field: String(value.FIELD),
        headerName: String(value.LABEL),
        sortable: false,
        pinned: value.PINNED,
        headerClass: value.HEADERCLASS,
        cellStyle: {
          textAlign: value.TEXTALIGN,
        },
        editable: false,
        type: 'nonEditableColumn',
        width: value.WIDTH,
        cellRenderer: value.CELL_RENDERER,
        hide: value.HIDE,
        wrapText: value.WRAPTEXT,
        cellRendererParams: () => {
          if (value.LABEL === 'Action') {
            return {
              clicked: (field: any) => {
                const { action, data } = field;
                this.onGridActionButton(action, data);
              },
            };
          }
        },
      });
    }

    this.menuService
      .onItemClick()
      .pipe(
        filter(({ tag }) => tag === 'campaign-context-menu'),
        map(({ item: { data } }) => data)
      )
      .subscribe((action) => {
        switch (Number(action)) {
          case CAMPAIGN_STATUS.ACTIVE:
            this.updateCampaignStatus(ECampaignStatus.ACTIVE);
            break;
          case CAMPAIGN_STATUS.INACTIVE:
            this.updateCampaignStatus(ECampaignStatus.INACTIVE);
            break;
          case CAMPAIGN_STATUS.TERMINATED:
            this.updateCampaignStatus(ECampaignStatus.TERMINATED);
            break;
        }
      });
  }

  ngOnInit(): void {
    this.user = this.storage.getItem(CURRENT_USER);
    this.profile = this.storage.getItem(CURRENT_USER_PROFILE);
    if (this.user.role === USER_ROLE.ADMIN) {
      this.initAdminCampaigns();
    } else if (this.user.role === USER_ROLE.MANAGER) {
      this.initSDRMCampaigns();
    } else if (this.user.role === USER_ROLE.SDR) {
      this.initSDRCampaigns();
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  onGridReady(params: any) {
    this.gridApi = params.api;
    // if (this.agGrid && this.agGrid.api) {
    //   this.agGrid.api.showLoadingOverlay();
    // }
  }

  onGridActionButton(action: string, data: any) {
    if (action === 'update_campaign_status') {
      this.toggleUpdateStatus(data);
    } else if (action === 'view') {
      this.viewCampaign(data);
    } else if (action === 'delete') {
      this.deleteCampaign(data);
    } else if (action === 'manage_sdr') {
      this.toggleAddSDR(data);
    } else if (action === 'add_sdrm') {
      this.toggleAddCampaignManager(data);
    }
  }

  toggleNewCampaign() {
    const initialState = {
      action: FORMSTATE.ADD,
    };
    this.modalService.show(CampaignEntryComponent, {
      initialState,
      class: 'modal-lg',
    });
  }

  toggleAddSDR(campaign: ICampaigns) {
    this.router.navigate(['/main/campaign/sdr'], {
      queryParams: {
        id: campaign.id,
      },
    });
  }

  toggleAddCampaignManager(campaign: ICampaigns) {
    const initialState = {
      campaignId: campaign.id,
      managerId: campaign.managerId,
      action: isNull(campaign.managerId) ? FORMSTATE.ADD : FORMSTATE.UPDATE,
    };
    this.modalService.show(CampaignManagerEntryComponent, {
      initialState,
    });
  }

  viewCampaign(campaign: ICampaigns) {
    this.router.navigate(['/main/campaign/details'], {
      queryParams: {
        id: campaign.id,
      },
    });
  }

  viewScheduler(campaign: ICampaigns) {
    if (this.user.role === USER_ROLE.SDR) {
      this.router.navigate(['/main/scheduler']);
    } else if (this.user.role === USER_ROLE.MANAGER) {
      this.router.navigate(['/main/scheduler']);
    } else {
      this.router.navigate(['/main/campaign/details'], {
        queryParams: {
          id: campaign.id,
        },
      });
    }
  }

  deleteCampaign(campaign: ICampaigns) {
    const deleteCampaign = async () => {
      const loading = await this.loadingController.create();
      loading.present();

      this.deleteSubs$ = this.mainService.deleteCampaign(campaign.id).subscribe(
        () => {
          this.deleteSubs$.unsubscribe();
          this.refreshCampaigns();

          Swal.fire('Deleted!', '', 'success');
        },
        () => loading.dismiss(),
        () => loading.dismiss()
      );
    };

    Swal.fire({
      title: 'Do you want to delete this campaign?',
      showCancelButton: true,
      confirmButtonText: `Confirm`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        deleteCampaign();
      }
    });
  }

  toggleUpdateStatus(campaign: ICampaigns) {
    this.selectedCampaign = campaign;
  }

  pageChanged(event: PageChangedEvent) {
    const { page } = event;
    this.refreshCampaigns(page);
  }

  search(event: any) {
    const val = event.target.value;
    this.refreshCampaigns(1, val);
  }

  clearSearch() {
    this.refreshCampaigns();
  }

  hasFeatureAccess(action: string): boolean {
    this.user = this.storage.getItem(CURRENT_USER);
    if (this.user) {
      return this.authService.hasPermission(
        this.user.role,
        MENU_ITEM_NAME.CAMPAIGN.TAG,
        action
      );
    } else {
      return false;
    }
  }

  private async initAdminCampaigns() {
    // const CACHE_DATA = this.storage.getItem(ADMIN_CAMPAIGNS);
    const loading = await this.loadingController.create();
    loading.present();

    const handleResponse = (campaignsArr: ICampaigns[]) => {
      this.campaignArr = [...campaignsArr];
      setTimeout(() => {
        this.showContent = true;
        loading.dismiss();
      }, 300);
    };

    this.mainStore
      .pipe(
        select(mainSelector),
        takeUntil(this.unsubscribe$),
        map((res) => res.campaigns)
      )
      .subscribe(
        (response: ICampaignResponse) => {
          if (response && response.data) {
            const data = cloneDeep(response.data);
            this.totalItems = response.count;
            handleResponse(data);
          }
        },
        () => {
          setTimeout(() => {
            this.showContent = true;
            loading.dismiss();
          }, 300);
        }
      );
  }

  private async initSDRCampaigns() {
    const loading = await this.loadingController.create();
    loading.present();

    this.schedulerService
      .getActiveCampaigns()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (response) => {
          if (response) {
            console.log('initSDRCampaigns: ', response);
            this.campaignArr = [...response];
            this.totalItems = size(response);
          }
        },
        () => {
          setTimeout(() => {
            this.showContent = true;
            loading.dismiss();
          }, 300);
        },
        () => {
          setTimeout(() => {
            this.showContent = true;
            loading.dismiss();
          }, 300);
        }
      );
  }

  private async initSDRMCampaigns() {
    const loading = await this.loadingController.create();
    loading.present();

    this.mainService
      .getAllSDRMCampaign(Number(this.profile.id))
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (response) => {
          if (response) {
            console.log('initSDRMCampaigns: ', response);
            this.campaignArr = [...response];
            this.totalItems = size(response);
          }
        },
        () => {
          setTimeout(() => {
            this.showContent = true;
            loading.dismiss();
          }, 300);
        },
        () => {
          setTimeout(() => {
            this.showContent = true;
            loading.dismiss();
          }, 300);
        }
      );
  }

  private refreshCampaigns(page: number = 1, Search?: string) {
    const params: IGenericParams = {
      PageIndex: page,
      PageSize: this.itemsPerPage,
    };

    if (Search) {
      params.Search = Search;
    }

    this.showContent = false;
    this.dataLoader.getAllCampaigns(params);
  }

  private updateCampaignStatus(statusId: ECampaignStatus) {
    const updateCampaignStatus = async () => {
      const loading = await this.loadingController.create();
      loading.present();

      this.updateStatusSub$ = this.mainService
        .changeCampaignStatus({
          campaignId: this.selectedCampaign.id,
          statusId,
        })
        .subscribe(
          (response) => {
            console.log('updateUserStatus: ', response);
            loading.dismiss();
            this.dataLoader.getAllCampaigns();
            Swal.fire('Updated!', '', 'success');
          },
          (err) => {
            loading.dismiss();
          },
          () => this.updateStatusSub$.unsubscribe()
        );
    };

    Swal.fire({
      title: 'Update Campaign status?',
      showCancelButton: true,
      confirmButtonText: `Confirm`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        updateCampaignStatus();
      }
    });
  }
}
