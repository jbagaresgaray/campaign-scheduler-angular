import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  NbButtonModule,
  NbCardModule,
  NbContextMenuModule,
  NbDatepickerModule,
  NbIconModule,
  NbInputModule,
  NbListModule,
  NbProgressBarModule,
  NbSelectModule,
  NbSpinnerModule,
  NbTooltipModule,
  NbUserModule,
} from '@nebular/theme';
import { IonicModule } from '@ionic/angular';

import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { ThemeModule } from 'src/app/shared/@theme/@theme.module';

import { CampaignRoutingModule } from './campaign-routing.module';
import { CampaignComponent } from './campaign.component';
import { SchedulerComponentsModule } from '../scheduler/components/scheduler-components.module';

import { CampaignEntryComponent } from './components/campaign-entry/campaign-entry.component';
import { CampaignSdrEntryComponent } from './components/campaign-sdr-entry/campaign-sdr-entry.component';
import { CampaignManagerEntryComponent } from './components/campaign-manager-entry/campaign-manager-entry.component';
import { CampaignSchedulerComponent } from './components/campaign-scheduler/campaign-scheduler.component';
import { CampaignActionButtonRendererComponent } from './components/campaign-action-button-renderer/campaign-action-button-renderer.component';
import { AgGridModule } from 'ag-grid-angular';

@NgModule({
  declarations: [
    CampaignComponent,
    CampaignEntryComponent,
    CampaignSdrEntryComponent,
    CampaignManagerEntryComponent,
    CampaignSchedulerComponent,
    CampaignActionButtonRendererComponent,
  ],
  imports: [
    CommonModule,
    CampaignRoutingModule,
    SchedulerComponentsModule,
    ComponentsModule,
    NbButtonModule,
    NbCardModule,
    NbIconModule,
    NbInputModule,
    NbTooltipModule,
    NbProgressBarModule,
    NbListModule,
    NbUserModule,
    NbDatepickerModule,
    NbSelectModule,
    NbSpinnerModule,
    NbContextMenuModule,
    ThemeModule,
    ModalModule,
    ReactiveFormsModule,
    FormsModule,
    PaginationModule,
    IonicModule,
    AgGridModule,
  ],
})
export class CampaignModule {}
