import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CampaignComponent } from './campaign.component';

const routes: Routes = [
  { path: '', component: CampaignComponent },
  {
    path: 'details',
    loadChildren: () =>
      import('./components/campaign-details/campaign-details.module').then(
        (m) => m.CampaignDetailsModule
      ),
  },
  {
    path: 'sdr',
    loadChildren: () =>
      import('./components/campaign-sdr/campaign-sdr.module').then(
        (m) => m.CampaignSdrModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CampaignRoutingModule {}
