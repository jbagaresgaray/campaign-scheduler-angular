import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignSdrComponent } from './campaign-sdr.component';

describe('CampaignSdrComponent', () => {
  let component: CampaignSdrComponent;
  let fixture: ComponentFixture<CampaignSdrComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CampaignSdrComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignSdrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
