import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subject, Subscription } from 'rxjs';
import { filter, map, takeUntil } from 'rxjs/operators';
import { NbMenuItem, NbMenuService, NbWindowService } from '@nebular/theme';
import Swal from 'sweetalert2';

import { IEmployees } from 'src/app/modules/main/models/employee.model';
import { IGenericParams } from 'src/app/modules/main/models/generic.model';
import { MainService } from 'src/app/modules/main/services/main.service';
import { DataLoaderService } from 'src/app/shared/services/data-loader.service';

import { CampaignSdrEntryComponent } from '../campaign-sdr-entry/campaign-sdr-entry.component';
import { CampaignUploadComponent } from '../campaign-upload/campaign-upload.component';
import { CampaignSchedulerComponent } from '../campaign-scheduler/campaign-scheduler.component';

import {
  CAMPAIGN_SDR_STATUS,
  CAMPAIGN_SDR_STATUS_ENUM,
  CURRENT_USER,
  MAX_ITEMS_PER_PAGE,
  PERMISSION_ACTION_DESC,
  USER_ROLE,
} from 'src/app/shared/constants/utils';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { ToastAlertService } from 'src/app/shared/services/toast-alert.service';
import { ICampaign } from 'src/app/modules/main/models/campaign.model';
import { ECampaignStatus } from 'src/app/shared/constants/enums';
import { NgxSpinnerService } from 'ngx-spinner';
import { LoadingController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { IUserAccount } from 'src/app/modules/auth/models/auth.model';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { AuthService } from 'src/app/modules/auth/services/auth.service';
import { MENU_ITEM_NAME } from 'src/app/shared/constants/page.menu';

@Component({
  selector: 'app-campaign-sdr',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './campaign-sdr.component.html',
  styleUrls: ['./campaign-sdr.component.scss'],
})
export class CampaignSdrComponent implements OnInit {
  sdrArr: IEmployees[] = [];
  selectedSDR: IEmployees;
  user: IUserAccount;
  sdrStatusArr: NbMenuItem[] = [];
  campaign: ICampaign;
  fakeArr: any[] = [];

  campaignId: number;
  bsModalRef: BsModalRef;
  isRemoving = false;
  showContent = false;

  itemsPerPage = MAX_ITEMS_PER_PAGE;
  totalItems: number;
  currentPage = 1;

  unsubscribe$ = new Subject<any>();
  private updateStatusSub$: Subscription = new Subscription();
  private removeSDRSub: Subscription = new Subscription();

  readonly CAMPAIGN_SDR_STATUS_ENUM = CAMPAIGN_SDR_STATUS_ENUM;
  readonly PERMISSION_ACTION_DESC = PERMISSION_ACTION_DESC;
  readonly USER_ROLE = USER_ROLE;

  constructor(
    private route: ActivatedRoute,
    private modalService: BsModalService,
    private mainService: MainService,
    private dataLoader: DataLoaderService,
    private toastService: ToastAlertService,
    private menuService: NbMenuService,
    private windowService: NbWindowService,
    private loadingController: LoadingController,
    private storage: LocalStorageService,
    private authService: AuthService
  ) {
    this.fakeArr = Array.from({
      length: 20,
    });

    this.menuService
      .onItemClick()
      .pipe(
        filter(({ tag }) => tag === 'sdr-context-menu'),
        map(({ item: { data } }) => data)
      )
      .subscribe((action) => {
        switch (Number(action)) {
          case CAMPAIGN_SDR_STATUS_ENUM.ACTIVE:
            this.updateSDRCampaignStatus(CAMPAIGN_SDR_STATUS_ENUM.ACTIVE);
            break;
          case CAMPAIGN_SDR_STATUS_ENUM.INACTIVE:
            this.updateSDRCampaignStatus(CAMPAIGN_SDR_STATUS_ENUM.INACTIVE);
            break;
          case CAMPAIGN_SDR_STATUS_ENUM.TERMINATED:
            this.updateSDRCampaignStatus(CAMPAIGN_SDR_STATUS_ENUM.TERMINATED);
            break;
          case CAMPAIGN_SDR_STATUS_ENUM.AVAILABLE:
            this.updateSDRCampaignStatus(CAMPAIGN_SDR_STATUS_ENUM.AVAILABLE);
            break;
        }
      });

    for (const [key, value] of Object.entries(this.CAMPAIGN_SDR_STATUS_ENUM)) {
      this.sdrStatusArr.push({
        title: String(key),
        data: String(value),
      });
    }

    this.route.queryParams.subscribe((params) => {
      if (params && params.id) {
        this.campaignId = params.id;
        this.getCampaign(this.campaignId);
        this.refreshEmployees();
      }
    });
  }

  ngOnInit(): void {
    this.user = this.storage.getItem(CURRENT_USER);
  }

  hasFeatureAccess(action): boolean {
    this.user = this.storage.getItem(CURRENT_USER);
    if (this.user) {
      return this.authService.hasPermission(
        this.user.role,
        MENU_ITEM_NAME.CAMPAIGN.TAG,
        action
      );
    } else {
      return false;
    }
  }

  toggleAddSDR() {
    const initialState = {
      campaignId: this.campaignId,
    };
    this.bsModalRef = this.modalService.show(CampaignSdrEntryComponent, {
      initialState,
      class: 'modal-lg',
    });
    this.bsModalRef.content.onRefreshCampaign.subscribe((res) => {
      this.getCampaign(this.campaignId);
      this.refreshEmployees();
    });
  }

  search(event: any) {
    const val = event.target.value;
    this.refreshEmployees(1, val);
  }

  clearSearch() {
    this.refreshEmployees();
  }

  pageChanged(event: PageChangedEvent) {
    console.log('event: ', event);
    const { page } = event;
    this.refreshEmployees(page);
  }

  removeSDR(employee: IEmployees) {
    const removeSRR = async () => {
      const loading = await this.loadingController.create();
      loading.present();
      this.isRemoving = true;
      this.removeSDRSub = this.mainService
        .unAssignSDRToCampaign({
          campaignId: this.campaignId,
          userProfileId: Number(employee.id),
        })
        .subscribe(
          (response) => {
            if (response && response.statusCode === 200) {
              this.removeSDRSub.unsubscribe();
              this.isRemoving = false;
              loading.dismiss();

              Swal.fire('Removed!', '', 'success');

              if (this.campaignId) {
                this.getCampaign(this.campaignId);
              }
            }
          },
          () => {
            this.isRemoving = false;
            loading.dismiss();
          },
          () => loading.dismiss()
        );
    };

    Swal.fire({
      title: 'Do you want to remove this SDR?',
      showCancelButton: true,
      confirmButtonText: `Confirm`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        removeSRR();
      }
    });
  }

  uploadScheduler(employee: IEmployees) {
    if (String(employee.statusId) !== String(CAMPAIGN_SDR_STATUS_ENUM.ACTIVE)) {
      return this.toastService.showWarningToast(
        'WARNING!',
        'Uploading only available for active SDR!'
      );
    }

    const initialState = {
      employee,
      campaign: this.campaign,
    };
    this.modalService.show(CampaignUploadComponent, {
      initialState,
      ignoreBackdropClick: true,
      keyboard: false,
      class: 'modal-lg',
    });
  }

  toggleSDRUpdateStatus(sdr: IEmployees) {
    this.selectedSDR = sdr;
  }

  viewCampaignScheduler(employee: IEmployees) {
    this.windowService
      .open(CampaignSchedulerComponent, {
        title: `${employee.firstName} ${employee.lastName}`,
        context: {
          employee,
          campaign: this.campaign,
          campaignId: this.campaignId,
        },
        windowClass: 'window-fullscreen',
      })
      .fullScreen();
  }

  getSDRStatus(statusId: any) {
    switch (statusId) {
      case CAMPAIGN_SDR_STATUS_ENUM.ACTIVE:
        return {
          class: 'success',
          label: CAMPAIGN_SDR_STATUS.ACTIVE,
        };
      case CAMPAIGN_SDR_STATUS_ENUM.TERMINATED:
        return {
          class: 'danger',
          label: CAMPAIGN_SDR_STATUS.TERMINATED,
        };
      case CAMPAIGN_SDR_STATUS_ENUM.AVAILABLE:
        return {
          class: 'info',
          label: CAMPAIGN_SDR_STATUS.AVAILABLE,
        };

      default:
        return {
          class: 'warning',
          label: CAMPAIGN_SDR_STATUS.INACTIVE,
        };
    }
  }

  private refreshEmployees(page: number = 1, Search?: string) {
    const params: IGenericParams = {
      PageIndex: page,
      PageSize: this.itemsPerPage,
    };

    if (Search) {
      params.Search = Search;
    }

    this.mainService
      .getAllCampaignSDR(this.campaignId, params)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (response) => {
          if (response && response.data) {
            this.sdrArr = [...response.data];
          }
          this.showContent = true;
        },
        () => (this.showContent = true),
        () => (this.showContent = true)
      );
  }

  private getCampaign(id) {
    this.user = this.storage.getItem(CURRENT_USER);
    if (this.user.role === USER_ROLE.ADMIN) {
      this.showContent = false;
      this.mainService
        .getCampaign(id)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe((response: ICampaign) => {
          if (response) {
            this.campaign = response;
            this.campaign.id = String(this.campaignId);
          }
        });

      // Refresh Campaign and Campaign ngRX
      this.dataLoader.getAllCampaigns();
    }
  }

  private updateSDRCampaignStatus(statusId: ECampaignStatus) {
    const updateSDRCampaignStatus = async () => {
      const loading = await this.loadingController.create();
      loading.present();
      this.updateStatusSub$ = this.mainService
        .changeSDRCampaignStatus({
          campaignId: Number(this.campaign.id) || this.campaignId,
          statusId,
          userProfileId: Number(this.selectedSDR.id),
        })
        .subscribe(
          (response) => {
            console.log('updateSDRCampaignStatus: ', response);

            this.getCampaign(this.campaignId);

            Swal.fire('Updated!', '', 'success');
          },
          (err) => loading.dismiss(),
          () => {
            this.updateStatusSub$.unsubscribe();
            loading.dismiss();
          }
        );
    };

    Swal.fire({
      title: 'Update SDR status?',
      showCancelButton: true,
      confirmButtonText: `Confirm`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        updateSDRCampaignStatus();
      }
    });
  }
}
