import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CampaignSdrComponent } from './campaign-sdr.component';

const routes: Routes = [{ path: '', component: CampaignSdrComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CampaignSdrRoutingModule { }
