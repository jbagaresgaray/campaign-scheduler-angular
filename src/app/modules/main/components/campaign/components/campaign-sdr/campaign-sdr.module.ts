import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  NbButtonModule,
  NbCardModule,
  NbContextMenuModule,
  NbDatepickerModule,
  NbIconModule,
  NbInputModule,
  NbListModule,
  NbProgressBarModule,
  NbSelectModule,
  NbSpinnerModule,
  NbTabsetModule,
  NbTooltipModule,
  NbUserModule,
} from '@nebular/theme';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { IonicModule } from '@ionic/angular';
import { PaginationModule } from 'ngx-bootstrap/pagination';

import { ThemeModule } from 'src/app/shared/@theme/@theme.module';

import { CampaignSdrRoutingModule } from './campaign-sdr-routing.module';
import { SchedulerComponentsModule } from '../../../scheduler/components/scheduler-components.module';
import { CampaignSdrComponent } from './campaign-sdr.component';

import { ComponentsModule } from 'src/app/shared/components/components.module';

@NgModule({
  declarations: [CampaignSdrComponent],
  imports: [
    CommonModule,
    CampaignSdrRoutingModule,
    SchedulerComponentsModule,
    ComponentsModule,
    NbButtonModule,
    NbCardModule,
    NbIconModule,
    NbInputModule,
    NbListModule,
    NbUserModule,
    NbTooltipModule,
    NbSpinnerModule,
    NbSelectModule,
    NbDatepickerModule,
    NbContextMenuModule,
    NbProgressBarModule,
    NbListModule,
    ThemeModule,
    ModalModule,
    FormsModule,
    NgxDropzoneModule,
    IonicModule,
    PaginationModule,
  ],
})
export class CampaignSdrModule {}
