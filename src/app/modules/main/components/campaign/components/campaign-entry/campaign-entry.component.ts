import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { select, Store } from '@ngrx/store';
import Swal from 'sweetalert2';
import { Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { LoadingController } from '@ionic/angular';

import { ToastAlertService } from './../../../../../../shared/services/toast-alert.service';
import { DataLoaderService } from './../../../../../../shared/services/data-loader.service';
import { MainService } from './../../../../services/main.service';
import {
  CAMPAIGN_TYPE_ENUM,
  emailValidationRegex,
  FORMSTATE,
  MAX_ITEMS_PER_PAGE,
} from './../../../../../../shared/constants/utils';
import { ICampaign } from 'src/app/modules/main/models/campaign.model';
import {
  IEmployees,
  IEmployeesResponse,
} from 'src/app/modules/main/models/employee.model';
import { MainState } from 'src/app/modules/main/store/main.reducers';
import { mainSelector } from 'src/app/modules/main/store/main.selectors';

@Component({
  selector: 'app-campaign-entry',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './campaign-entry.component.html',
  styleUrls: ['./campaign-entry.component.scss'],
})
export class CampaignEntryComponent implements OnInit, OnDestroy {
  campaignForm: FormGroup;
  employeeArr: IEmployees[] = [];
  campaignTypeArr: {
    id: number;
    name: string;
  }[] = [];

  isFormValid = false;
  isSubmitting = false;

  campaignId: string;
  action: string;
  buttonLabel: string;
  unsubscribe$ = new Subject<any>();
  unsubscribe2$ = new Subject<any>();

  constructor(
    private store: Store<MainState>,
    private formBuilder: FormBuilder,
    public bsModalRef: BsModalRef,
    private mainService: MainService,
    private dataLoader: DataLoaderService,
    private toastService: ToastAlertService,
    private loadingController: LoadingController
  ) {
    for (const [key, value] of Object.entries(CAMPAIGN_TYPE_ENUM)) {
      this.campaignTypeArr.push({
        id: value,
        name: key,
      });
    }
  }

  ngOnInit(): void {
    this.buttonLabel = this.action === FORMSTATE.UPDATE ? 'Update' : 'Save';
    this.createForm();
    this.initEmployees();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();

    this.unsubscribe2$.next();
    this.unsubscribe2$.complete();
  }

  get f(): any {
    return this.campaignForm.controls;
  }

  createCampaign() {
    this.isSubmitting = true;
    this.campaignForm.disable();

    if (this.campaignForm.invalid) {
      this.isSubmitting = false;
      this.campaignForm.enable();
      this.validateAllFormFields(this.campaignForm);
      return;
    }

    const payload: ICampaign = this.campaignForm.getRawValue();
    this.mainService
      .createCampaign(payload)
      .toPromise()
      .then(
        () => {
          this.isSubmitting = false;

          Swal.fire(
            this.action === FORMSTATE.ADD ? 'Created!' : 'Updated!',
            '',
            'success'
          ).then(() => this.bsModalRef.hide());

          this.dataLoader.getAllCampaigns({
            PageSize: MAX_ITEMS_PER_PAGE,
          });
        },
        (error) => {
          if (error) {
            const err = error.error;
            this.campaignForm.enable();
            this.isSubmitting = false;
            if (err && err.errors) {
              const errors = err.errors;
              if (errors) {
                this.toastService.showErrorToast('Warning!', errors[0]);
                return;
              }
            } else if (err && err.message) {
              this.toastService.showErrorToast('Warning!', err.message);
              return;
            }
          }
        }
      );
  }

  private createForm() {
    this.campaignForm = this.formBuilder.group({
      campaignName: ['', [Validators.required]],
      plan: ['', [Validators.required]],
      client: ['', [Validators.required]],
      started: ['', [Validators.required]],
      clientEmail: [
        '',
        [Validators.required, Validators.pattern(emailValidationRegex)],
      ],
      accountsExecutiveId: [''],
      trelloEditorId: [''],
      campaignType: ['', Validators.required],
    });
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  private async initEmployees() {
    const loading = await this.loadingController.create();
    loading.present();

    const handleResponse = (employeeArr) => {
      this.employeeArr = [...employeeArr];
      loading.dismiss();
    };

    this.store
      .pipe(
        select(mainSelector),
        takeUntil(this.unsubscribe2$),
        map((res) => res.employeeAllStaffs)
      )
      .subscribe(
        (response: IEmployeesResponse) => {
          if (response && response.data) {
            handleResponse(response.data);
          }
        },
        () => {
          loading.dismiss();
        }
      );
  }
}
