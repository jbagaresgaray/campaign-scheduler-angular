import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import Swal from 'sweetalert2';
import { of, Subject } from 'rxjs';

import { ICampaign } from 'src/app/modules/main/models/campaign.model';
import { IEmployees } from 'src/app/modules/main/models/employee.model';
import { MainService } from 'src/app/modules/main/services/main.service';
import { SchedulerService } from 'src/app/modules/main/services/scheduler.service';
import { DataLoaderService } from 'src/app/shared/services/data-loader.service';
import {
  HttpErrorResponse,
  HttpEventType,
  HttpResponse,
} from '@angular/common/http';
import { ToastAlertService } from 'src/app/shared/services/toast-alert.service';
import { catchError, map } from 'rxjs/operators';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-campaign-upload',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './campaign-upload.component.html',
  styleUrls: ['./campaign-upload.component.scss'],
})
export class CampaignUploadComponent implements OnInit, OnDestroy {
  employee: IEmployees;
  campaign: ICampaign;

  fileUploads: any[] = [];
  progressInfos: any[] = [];
  isUploading: boolean;
  progressBarCount: number;
  progressBarPercent: number;
  totalProgressCount: number;
  MAXPERCENT = 100;

  acceptedFile =
    'application/vnd.ms-excel,application/vnd.msexcel,application/csv,application/excel,text/csv';

  unsubscribe$ = new Subject<any>();

  constructor(
    public bsModalRef: BsModalRef,
    private mainService: MainService,
    private schedulerService: SchedulerService,
    private dataLoader: DataLoaderService,
    private toastService: ToastAlertService,
    private loadingController: LoadingController
  ) {}

  ngOnInit(): void {
    this.isUploading = false;
    this.progressBarCount = 0;
    this.progressBarPercent = 0;
    console.log('campaign: ', this.campaign);
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  onSelect(event) {
    console.log('onSelect: ', event);
    const fileUploads = [];
    fileUploads.push(...event.addedFiles);
    this.fileUploads = fileUploads.map((item) => {
      return {
        data: item,
        inProgress: false,
        progress: 0,
      };
    });
    this.totalProgressCount = this.MAXPERCENT * this.fileUploads.length;
  }

  onRemove(event) {
    this.fileUploads.splice(this.fileUploads.indexOf(event), 1);
    this.totalProgressCount = this.MAXPERCENT * this.fileUploads.length;
  }

  async uploadFile(): Promise<void> {
    if (this.isUploading) {
      return;
    }

    this.isUploading = true;
    this.progressBarCount = 0;
    this.progressBarPercent = 0;

    return new Promise((resolve, reject) => {
      Promise.all(
        this.fileUploads.map((image, index) => {
          return this.upload(index, {
            data: image.data,
            inProgress: image.inProgress,
            progress: image.progress,
          });
        })
      )
        .then(() => {
          this.isUploading = false;

          if (this.campaign.id) {
            this.getCampaign(this.campaign.id);
          }
          resolve();
        })
        .catch((reason) => {
          this.isUploading = false;
          console.log('reasons: ', reason);
        });
    });
  }

  private getCampaign(id) {
    this.mainService.getAllCampaignSDR(id);
    this.dataLoader.getAllCampaigns();
  }

  private upload(
    index,
    file: {
      data: File;
      inProgress: boolean;
      progress: number;
    }
  ): Promise<void> {
    this.fileUploads[index].inProgress = true;
    const loading = this.loadingController.create({
      message:
        'Importing Scheduler... Please wait as this process takes some time',
    });
    return this.schedulerService
      .uploadCampaignScheduler({
        UserProfileId: this.employee.id,
        CampaignId: this.campaign.id,
        CsvData: file.data,
      })
      .pipe(
        map((event: any) => {
          switch (event.type) {
            case HttpEventType.UploadProgress:
              const progress = Math.round((event.loaded * 100) / event.total);
              this.fileUploads[index].progress = progress;
              if (progress >= 100) {
                this.toastService.showSuccessToast(
                  'Success',
                  'CSV imported successfully!'
                );
                loading.then((l) => l.present());
              }
              break;
            case HttpEventType.Response:
              return event;
          }
        }),
        catchError((error: HttpErrorResponse) => {
          loading.then((l) => l.dismiss());
          this.fileUploads[index].inProgress = false;

          this.toastService.showErrorAlert(
            'Could not upload the file:' + file.data.name
          );
          return of(`Upload failed: ${file.data.name}`);
        })
      )
      .toPromise()
      .then(
        (event) => {
          if (typeof event === 'object') {
            loading.then((l) => l.dismiss());
            Swal.fire('Uploaded successfully!', '', 'success').then(() => {
              this.bsModalRef.hide();
            });
          }
        },
        (reason) => {
          console.log('uploadCampaignScheduler reasons: ', reason);
        }
      );
  }
}
