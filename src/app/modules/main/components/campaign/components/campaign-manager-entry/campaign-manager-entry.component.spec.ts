import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignManagerEntryComponent } from './campaign-manager-entry.component';

describe('CampaignManagerEntryComponent', () => {
  let component: CampaignManagerEntryComponent;
  let fixture: ComponentFixture<CampaignManagerEntryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CampaignManagerEntryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignManagerEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
