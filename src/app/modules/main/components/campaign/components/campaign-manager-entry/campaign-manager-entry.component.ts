import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Subject, Subscription } from 'rxjs';
import Swal from 'sweetalert2';
import cloneDeep from 'lodash-es/cloneDeep';

import { MainService } from 'src/app/modules/main/services/main.service';
import {
  IEmployees,
  IEmployeesResponse,
} from 'src/app/modules/main/models/employee.model';
import { DataLoaderService } from 'src/app/shared/services/data-loader.service';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { ADMIN_MANAGERS } from 'src/app/shared/constants/utils';
import { select, Store } from '@ngrx/store';
import { MainState } from 'src/app/modules/main/store/main.reducers';
import { mainSelector } from 'src/app/modules/main/store/main.selectors';
import { map, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-campaign-manager-entry',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './campaign-manager-entry.component.html',
  styleUrls: ['./campaign-manager-entry.component.scss'],
})
export class CampaignManagerEntryComponent implements OnInit, OnDestroy {
  managersArr: IEmployees[] = [];

  campaignId: number;
  managerId: number;
  action: string;

  unsubscribe$ = new Subject<any>();

  isSettingManager = false;
  private assignManagerSub: Subscription = new Subscription();

  constructor(
    private store: Store<MainState>,
    public bsModalRef: BsModalRef,
    private mainService: MainService,
    private dataLoader: DataLoaderService,
    private storage: LocalStorageService
  ) {}

  ngOnInit(): void {
    this.initManagers();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  assignCampaignManager() {
    Swal.fire({
      title: 'Do you want to assign this Manager on the campaign?',
      showCancelButton: true,
      confirmButtonText: `Confirm`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        this.isSettingManager = true;
        this.assignManagerSub = this.mainService
          .assignManagerToCampaign({
            campaignId: this.campaignId,
            userProfileId: this.managerId,
          })
          .subscribe(
            (response) => {
              if (response && response.statusCode === 200) {
                this.assignManagerSub.unsubscribe();
                this.isSettingManager = false;
                this.dataLoader.getAllCampaigns();

                Swal.fire('Updated!', '', 'success').then(() => {
                  this.bsModalRef.hide();
                });
              }
            },
            () => {
              this.isSettingManager = false;
            }
          );
      }
    });
  }

  private initManagers() {
    console.log('managerId: ', this.managerId);
    const CACHE_DATA = this.storage.getItem(ADMIN_MANAGERS);
    const handleResponse = (managersArr) => {
      this.managersArr = [...managersArr];
    };

    this.store
      .pipe(
        select(mainSelector),
        takeUntil(this.unsubscribe$),
        map((res) => res.employeeMgr)
      )
      .subscribe((response: IEmployeesResponse) => {
        if (response && response.data) {
          const employees = cloneDeep(response.data);
          handleResponse(employees);
        } else {
          const data = CACHE_DATA.data;
          handleResponse(data);
        }
      });
  }
}
