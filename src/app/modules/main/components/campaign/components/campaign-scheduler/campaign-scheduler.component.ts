import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ICampaign } from 'src/app/modules/main/models/campaign.model';
import { IEmployees } from 'src/app/modules/main/models/employee.model';
import { IGenericParams } from 'src/app/modules/main/models/generic.model';
import { IScheduler } from 'src/app/modules/main/models/scheduler.model';
import { SchedulerService } from 'src/app/modules/main/services/scheduler.service';
import { MAX_ITEMS_PER_PAGE } from 'src/app/shared/constants/utils';

@Component({
  selector: 'app-campaign-scheduler',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './campaign-scheduler.component.html',
  styleUrls: ['./campaign-scheduler.component.scss'],
})
export class CampaignSchedulerComponent implements OnInit, OnDestroy {
  schedulerArr: IScheduler[] = [];
  employee: IEmployees;
  campaign: ICampaign;
  campaignId: number;

  itemsPerPage = MAX_ITEMS_PER_PAGE;
  totalItems: number;
  currentPage = 1;

  showContent = false;

  private unsubscribe = new Subject<any>();

  constructor(private schedulerService: SchedulerService) {}

  ngOnInit(): void {
    console.log('campaign: ', this.campaign);
    this.initScheduler();
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  private initScheduler(page: number = 1, Search?: string) {
    this.totalItems = 0;
    this.schedulerArr = [];

    const params: IGenericParams = {
      PageIndex: page,
      PageSize: this.itemsPerPage,
    };

    if (Search) {
      params.Search = Search;
    }

    this.schedulerService
      .getCampaignScheduler(this.campaignId, Number(this.employee.id), params)
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(
        (response) => {
          console.log('getCampaignScheduler: ', response);
          if (response && response.data) {
            this.schedulerArr = response.data;
            this.totalItems = response.count;
          }
          this.showContent = true;
        },
        () => (this.showContent = true),
        () => (this.showContent = true)
      );
  }
}
