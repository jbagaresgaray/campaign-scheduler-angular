import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewEncapsulation,
} from '@angular/core';
import { NbMenuItem, NbMenuService, NbWindowService } from '@nebular/theme';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import Swal from 'sweetalert2';

import { ICampaign } from 'src/app/modules/main/models/campaign.model';
import { IEmployees } from 'src/app/modules/main/models/employee.model';
import { MainService } from 'src/app/modules/main/services/main.service';
import { ECampaignStatus } from 'src/app/shared/constants/enums';
import {
  CAMPAIGN_SDR_STATUS,
  CAMPAIGN_SDR_STATUS_ENUM,
} from 'src/app/shared/constants/utils';
import { ToastAlertService } from 'src/app/shared/services/toast-alert.service';
``;
import { CampaignSdrEntryComponent } from '../../campaign-sdr-entry/campaign-sdr-entry.component';
import { CampaignUploadComponent } from '../../campaign-upload/campaign-upload.component';
import { CampaignSchedulerComponent } from '../../campaign-scheduler/campaign-scheduler.component';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-campaign-details-sdr-tab',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './campaign-details-sdr-tab.component.html',
  styleUrls: ['./campaign-details-sdr-tab.component.scss'],
})
export class CampaignDetailsSdrTabComponent implements OnInit {
  isSettingManager: boolean;
  isRemoving: boolean;

  private assignManagerSub: Subscription = new Subscription();
  private removeSDRSub: Subscription = new Subscription();
  private updateStatusSub$: Subscription = new Subscription();

  @Input() campaignId: number;
  @Input() managerId: number;
  @Input() showContent: boolean;
  @Input() campaign: ICampaign;
  @Input() managersData: IEmployees[];
  @Input() sdrData: IEmployees[];
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onRefreshComponent = new EventEmitter<any>();
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onSearch = new EventEmitter<any>();
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onClearSearch = new EventEmitter<any>();

  sdrStatusArr: NbMenuItem[] = [];
  fakeArr: any[] = [];
  bsModalRef: BsModalRef;
  selectedSDR: IEmployees;

  readonly CAMPAIGN_SDR_STATUS_ENUM = CAMPAIGN_SDR_STATUS_ENUM;

  constructor(
    private mainService: MainService,
    private modalService: BsModalService,
    private toastService: ToastAlertService,
    private menuService: NbMenuService,
    private windowService: NbWindowService,
    private loadingController: LoadingController
  ) {
    this.fakeArr = Array.from({
      length: 20,
    });
    this.menuService
      .onItemClick()
      .pipe(
        filter(({ tag }) => tag === 'sdr-context-menu'),
        map(({ item: { data } }) => data)
      )
      .subscribe((action) => {
        switch (Number(action)) {
          case CAMPAIGN_SDR_STATUS_ENUM.ACTIVE:
            this.updateSDRCampaignStatus(CAMPAIGN_SDR_STATUS_ENUM.ACTIVE);
            break;
          case CAMPAIGN_SDR_STATUS_ENUM.INACTIVE:
            this.updateSDRCampaignStatus(CAMPAIGN_SDR_STATUS_ENUM.INACTIVE);
            break;
          case CAMPAIGN_SDR_STATUS_ENUM.TERMINATED:
            this.updateSDRCampaignStatus(CAMPAIGN_SDR_STATUS_ENUM.TERMINATED);
            break;
          case CAMPAIGN_SDR_STATUS_ENUM.AVAILABLE:
            this.updateSDRCampaignStatus(CAMPAIGN_SDR_STATUS_ENUM.AVAILABLE);
            break;
        }
      });

    for (const [key, value] of Object.entries(this.CAMPAIGN_SDR_STATUS_ENUM)) {
      this.sdrStatusArr.push({
        title: String(key),
        data: String(value),
      });
    }
    console.log('this.sdrStatusArr: ', this.sdrStatusArr);
  }

  ngOnInit(): void {
    this.isSettingManager = false;
    this.isRemoving = false;
  }

  toggleSDRUpdateStatus(sdr: IEmployees) {
    this.selectedSDR = sdr;
  }

  toggleAddSDR() {
    const initialState = {
      campaignId: this.campaignId,
    };
    this.bsModalRef = this.modalService.show(CampaignSdrEntryComponent, {
      initialState,
      class: 'modal-lg',
    });
    this.bsModalRef.content.onRefreshCampaign.subscribe((res) => {
      this.onRefreshComponent.emit();
    });
  }

  viewCampaignScheduler(employee: IEmployees) {
    this.windowService
      .open(CampaignSchedulerComponent, {
        title: `Scheduler - ${employee.firstName} ${employee.lastName}`,
        context: {
          employee,
          campaign: this.campaign,
          campaignId: this.campaignId,
        },
        windowClass: 'window-fullscreen',
      })
      .fullScreen();
  }

  getSDRStatus(statusId: any) {
    switch (statusId) {
      case CAMPAIGN_SDR_STATUS_ENUM.ACTIVE:
        return {
          class: 'success',
          label: CAMPAIGN_SDR_STATUS.ACTIVE,
        };
      case CAMPAIGN_SDR_STATUS_ENUM.TERMINATED:
        return {
          class: 'danger',
          label: CAMPAIGN_SDR_STATUS.TERMINATED,
        };
      case CAMPAIGN_SDR_STATUS_ENUM.AVAILABLE:
        return {
          class: 'info',
          label: CAMPAIGN_SDR_STATUS.AVAILABLE,
        };

      default:
        return {
          class: 'warning',
          label: CAMPAIGN_SDR_STATUS.INACTIVE,
        };
    }
  }

  uploadScheduler(employee: IEmployees) {
    if (String(employee.statusId) !== String(CAMPAIGN_SDR_STATUS_ENUM.ACTIVE)) {
      return this.toastService.showWarningToast(
        'WARNING!',
        'Uploading only available for active SDR!'
      );
    }

    const initialState = {
      employee,
      campaign: this.campaign,
    };
    this.modalService.show(CampaignUploadComponent, {
      initialState,
      ignoreBackdropClick: true,
      keyboard: false,
      class: 'modal-lg',
    });
  }

  removeSDR(employee: IEmployees) {
    const removeSDR = async () => {
      const loading = await this.loadingController.create();
      loading.present();

      this.isRemoving = true;
      this.removeSDRSub = this.mainService
        .unAssignSDRToCampaign({
          campaignId: this.campaignId,
          userProfileId: Number(employee.id),
        })
        .subscribe(
          (response) => {
            if (response && response.statusCode === 200) {
              this.removeSDRSub.unsubscribe();
              this.isRemoving = false;
              loading.dismiss();
              Swal.fire('Removed!', '', 'success');

              if (this.campaignId) {
                this.onRefreshComponent.emit();
              }
            }
          },
          () => {
            this.isRemoving = false;
            loading.dismiss();
          },
          () => loading.dismiss()
        );
    };
    Swal.fire({
      title: 'Do you want to remove this SDR?',
      showCancelButton: true,
      confirmButtonText: `Confirm`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        removeSDR();
      }
    });
  }

  assignCampaignManager() {
    const assignCampaignManager = async () => {
      const loading = await this.loadingController.create();
      loading.present();

      this.isSettingManager = true;
      this.assignManagerSub = this.mainService
        .assignManagerToCampaign({
          campaignId: this.campaignId,
          userProfileId: this.managerId,
        })
        .subscribe(
          (response) => {
            if (response && response.statusCode === 200) {
              this.assignManagerSub.unsubscribe();
              this.isSettingManager = false;
              loading.dismiss();

              Swal.fire('Updated!', '', 'success');

              this.onRefreshComponent.emit();
            }
          },
          () => {
            this.isSettingManager = false;
            loading.dismiss();
          },
          () => loading.dismiss()
        );
    };

    Swal.fire({
      title: 'Do you want to assign this Manager on the campaign?',
      showCancelButton: true,
      confirmButtonText: `Confirm`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        assignCampaignManager();
      }
    });
  }

  private updateSDRCampaignStatus(statusId: ECampaignStatus) {
    const updateSDRCampaignStatus = async () => {
      const loading = await this.loadingController.create();
      loading.present();

      this.updateStatusSub$ = this.mainService
        .changeSDRCampaignStatus({
          campaignId: Number(this.campaign.id) || this.campaignId,
          statusId,
          userProfileId: Number(this.selectedSDR.id),
        })
        .subscribe(
          (response) => {
            console.log('updateSDRCampaignStatus: ', response);
            loading.dismiss();
            this.onRefreshComponent.emit();

            Swal.fire('Updated!', '', 'success');
          },
          (err) => loading.dismiss(),
          () => {
            this.updateStatusSub$.unsubscribe();
            loading.dismiss();
          }
        );
    };

    Swal.fire({
      title: 'Update SDR status?',
      showCancelButton: true,
      confirmButtonText: `Confirm`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        updateSDRCampaignStatus();
      }
    });
  }
}
