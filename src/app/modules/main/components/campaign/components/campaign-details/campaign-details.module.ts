import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  NbButtonModule,
  NbCardModule,
  NbContextMenuModule,
  NbDatepickerModule,
  NbIconModule,
  NbInputModule,
  NbListModule,
  NbProgressBarModule,
  NbSelectModule,
  NbSpinnerModule,
  NbTabsetModule,
  NbTooltipModule,
  NbUserModule,
} from '@nebular/theme';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { IonicModule } from '@ionic/angular';
import { ThemeModule } from 'src/app/shared/@theme/@theme.module';

import { CampaignDetailsRoutingModule } from './campaign-details-routing.module';
import { SchedulerComponentsModule } from '../../../scheduler/components/scheduler-components.module';

import { CampaignDetailsComponent } from './campaign-details.component';
import { CampaignUploadComponent } from '../campaign-upload/campaign-upload.component';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { CampaignDetailsSdrTabComponent } from './campaign-details-sdr-tab/campaign-details-sdr-tab.component';
import { CampaignDetailsInfoTabComponent } from './campaign-details-info-tab/campaign-details-info-tab.component';
import { CampaignDetailsContentTabComponent } from './campaign-details-content-tab/campaign-details-content-tab.component';

@NgModule({
  declarations: [
    CampaignDetailsComponent,
    CampaignUploadComponent,
    CampaignDetailsSdrTabComponent,
    CampaignDetailsInfoTabComponent,
    CampaignDetailsContentTabComponent,
  ],
  imports: [
    CommonModule,
    CampaignDetailsRoutingModule,
    SchedulerComponentsModule,
    ComponentsModule,
    NbButtonModule,
    NbCardModule,
    NbIconModule,
    NbInputModule,
    NbListModule,
    NbUserModule,
    NbTooltipModule,
    NbSpinnerModule,
    NbSelectModule,
    NbDatepickerModule,
    NbContextMenuModule,
    NbProgressBarModule,
    NbTabsetModule,
    NbListModule,
    ThemeModule,
    ModalModule,
    ReactiveFormsModule,
    FormsModule,
    NgxDropzoneModule,
    IonicModule,
    CKEditorModule,
  ],
})
export class CampaignDetailsModule {}
