import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewEncapsulation,
} from '@angular/core';
import { CKEditor5 } from '@ckeditor/ckeditor5-angular';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import Swal from 'sweetalert2';

import {
  ICampaign,
  ICampaignContent,
} from 'src/app/modules/main/models/campaign.model';
import { MainService } from 'src/app/modules/main/services/main.service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-campaign-details-content-tab',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './campaign-details-content-tab.component.html',
  styleUrls: ['./campaign-details-content-tab.component.scss'],
})
export class CampaignDetailsContentTabComponent implements OnInit {
  @Input() campaignId: number;
  @Input() managerId: number;
  @Input() campaign: ICampaign;
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onRefreshComponent = new EventEmitter<any>();

  campaignContent = {
    message1: '',
    message2: '',
    message3: '',
    message4: '',
    message5: '',
    inviteMessage: '',
  };

  isSubmitting = false;
  Editor = ClassicEditor;
  editorConfig: CKEditor5.Config = {
    placeholder: ' Type the content here!',
    alignment: {
      options: ['left', 'right', 'center'],
    },
    // height: '320',
  };

  private unsubscribe = new Subject<any>();
  private unsubscribe2 = new Subject<any>();

  constructor(
    private mainService: MainService,
    private loadingController: LoadingController
  ) {}

  ngOnInit(): void {
    this.getCampaignContent();
  }

  onReady(editor: any) {
    editor.ui
      .getEditableElement()
      .parentElement.insertBefore(
        editor.ui.view.toolbar.element,
        editor.ui.getEditableElement()
      );
  }

  updateContent() {
    const updateContent = async () => {
      const loading = await this.loadingController.create();
      loading.present();

      const content: ICampaignContent = {
        ...this.campaignContent,
        campaignId: this.campaignId,
      };
      console.log('content: ', content);

      this.mainService
        .createCampaignContent(content)
        .pipe(takeUntil(this.unsubscribe2))
        .subscribe(
          (response) => {
            if (response && response.statusCode === 200) {
              loading.dismiss();
              this.getCampaignContent();
              Swal.fire('Saved!', '', 'success');
            }
          },
          () => loading.dismiss()
        );
    };

    Swal.fire({
      title: 'Update Campaign Content?',
      showCancelButton: true,
      confirmButtonText: `Confirm`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        updateContent();
      }
    });
  }

  private getCampaignContent() {
    this.mainService
      .getCampaignContent(this.campaignId)
      .pipe(takeUntil(this.unsubscribe))
      .subscribe((response) => {
        if (response) {
          console.log('response: ', response);
          this.campaignContent = response;
        }
      });
  }
}
