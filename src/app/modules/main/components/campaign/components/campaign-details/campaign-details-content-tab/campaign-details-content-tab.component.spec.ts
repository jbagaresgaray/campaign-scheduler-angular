import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignDetailsContentTabComponent } from './campaign-details-content-tab.component';

describe('CampaignDetailsContentTabComponent', () => {
  let component: CampaignDetailsContentTabComponent;
  let fixture: ComponentFixture<CampaignDetailsContentTabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CampaignDetailsContentTabComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignDetailsContentTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
