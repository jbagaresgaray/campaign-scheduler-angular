import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignDetailsInfoTabComponent } from './campaign-details-info-tab.component';

describe('CampaignDetailsInfoTabComponent', () => {
  let component: CampaignDetailsInfoTabComponent;
  let fixture: ComponentFixture<CampaignDetailsInfoTabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CampaignDetailsInfoTabComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignDetailsInfoTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
