import { Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { map, takeUntil } from 'rxjs/operators';

import cloneDeep from 'lodash-es/cloneDeep';
import lowerCase from 'lodash-es/lowerCase';
import isNull from 'lodash-es/isNull';

import { ToastAlertService } from 'src/app/shared/services/toast-alert.service';
import { MainService } from 'src/app/modules/main/services/main.service';
import { ADMIN_MANAGERS } from './../../../../../../shared/constants/utils';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import {
  IEmployeesResponse,
  IEmployees,
} from 'src/app/modules/main/models/employee.model';
import { ICampaign } from 'src/app/modules/main/models/campaign.model';
import { MainState } from 'src/app/modules/main/store/main.reducers';
import { mainSelector } from 'src/app/modules/main/store/main.selectors';
import { DataLoaderService } from 'src/app/shared/services/data-loader.service';
import { PubsubService } from 'src/app/shared/services/pubsub.service';

@Component({
  selector: 'app-campaign-details',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './campaign-details.component.html',
  styleUrls: ['./campaign-details.component.scss'],
})
export class CampaignDetailsComponent implements OnInit, OnDestroy {
  sdrArr: IEmployees[] = [];
  managersArr: IEmployees[] = [];

  sdrArrCopy: any[] = [];

  campaignId: number;
  managerId: number;

  campaign: ICampaign;
  isFormValid = false;

  isSettingManager = false;
  isRemoving = false;
  showContent = false;

  unsubscribe$ = new Subject<any>();
  unsubscribe$2 = new Subject<any>();

  constructor(
    private store: Store<MainState>,
    private route: ActivatedRoute,
    private mainService: MainService,
    private storage: LocalStorageService,
    private dataLoader: DataLoaderService,
    private pubsub: PubsubService
  ) {
    this.pubsub.$sub('getCampaign', () => {
      if (!isNull(this.campaignId)) {
        this.getCampaign(this.campaignId);
      }
    });
  }

  ngOnInit(): void {
    this.initManagers();

    this.route.queryParams.subscribe((params) => {
      if (params && params.id) {
        this.campaignId = params.id;
        this.getCampaign(this.campaignId);
      }
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();

    this.unsubscribe$2.next();
    this.unsubscribe$2.complete();
  }

  search(event: any) {
    let val = event.target.value;
    if (val) {
      val = lowerCase(val);
      const fileList = this.sdrArrCopy.filter((emp: IEmployees) => {
        return (
          lowerCase(emp.firstName).indexOf(val) > -1 ||
          lowerCase(emp.lastName).indexOf(val) > -1 ||
          lowerCase(emp.email).indexOf(val) > -1
        );
      });
      this.sdrArr = fileList;
    } else {
      console.log('1');
      this.sdrArr = this.sdrArrCopy;
    }
  }

  clearSearch() {
    this.sdrArr = this.sdrArrCopy;
  }

  getCampaign(id) {
    this.showContent = false;
    this.mainService
      .getCampaign(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((response: ICampaign) => {
        if (response) {
          this.campaign = response;
          this.managerId = response.userProfileId;
          this.campaign.id = String(this.campaignId);
        }
      });

    this.mainService
      .getAllCampaignSDR(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (response) => {
          if (response && response.data) {
            this.sdrArr = [...response.data];
            this.sdrArrCopy = [...response.data];
          }
        },
        () => (this.showContent = true),
        () => (this.showContent = true)
      );

    // Refresh Campaign and Campaign ngRX
    this.dataLoader.getAllCampaigns();
  }

  private initManagers() {
    const CACHE_DATA = this.storage.getItem(ADMIN_MANAGERS);
    const handleResponse = (managersArr) => {
      this.managersArr = [...managersArr];
    };

    this.store
      .pipe(
        select(mainSelector),
        takeUntil(this.unsubscribe$),
        map((res) => res.employeeMgr)
      )
      .subscribe((response: IEmployeesResponse) => {
        if (response && response.data) {
          const employees = cloneDeep(response.data);
          handleResponse(employees);
        } else {
          const data = CACHE_DATA.data;
          handleResponse(data);
        }
      });
  }
}
