import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  ViewEncapsulation,
} from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { NbMenuItem, NbMenuService } from '@nebular/theme';
import { Subject, Subscription } from 'rxjs';
import { filter, map, takeUntil } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { select, Store } from '@ngrx/store';

import { ECampaignStatus } from 'src/app/shared/constants/enums';
import {
  CAMPAIGN_STATUS,
  CAMPAIGN_STATUS_ITEM,
  CAMPAIGN_TYPE_ENUM,
  emailValidationRegex,
} from 'src/app/shared/constants/utils';
import { ICampaign } from 'src/app/modules/main/models/campaign.model';
import { MainService } from 'src/app/modules/main/services/main.service';
import { PubsubService } from 'src/app/shared/services/pubsub.service';
import { ToastAlertService } from 'src/app/shared/services/toast-alert.service';
import {
  IEmployees,
  IEmployeesResponse,
} from 'src/app/modules/main/models/employee.model';
import { MainState } from 'src/app/modules/main/store/main.reducers';
import { mainSelector } from 'src/app/modules/main/store/main.selectors';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-campaign-details-info-tab',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './campaign-details-info-tab.component.html',
  styleUrls: ['./campaign-details-info-tab.component.scss'],
})
export class CampaignDetailsInfoTabComponent
  implements OnInit, OnChanges, OnDestroy {
  @Input() campaignId: number;
  @Input() managerId: number;
  @Input() campaign: ICampaign;
  @Output() onRefreshComponent = new EventEmitter<any>();

  campaignForm: FormGroup;
  campaignStatusArr: NbMenuItem[] = [];
  employeeArr: IEmployees[] = [];
  campaignTypeArr: {
    id: number;
    name: string;
  }[] = [];

  isSubmitting = false;

  private updateStatusSub$: Subscription = new Subscription();
  private unsubscribe2$ = new Subject<any>();
  private readonly CAMPAIGN_STATUS = CAMPAIGN_STATUS;

  constructor(
    private store: Store<MainState>,
    private formBuilder: FormBuilder,
    private menuService: NbMenuService,
    private mainService: MainService,
    private pubsub: PubsubService,
    private toastService: ToastAlertService,
    private loadingController: LoadingController
  ) {
    for (const [key, value] of Object.entries(this.CAMPAIGN_STATUS)) {
      if (key === 'ARCHIVED') {
        continue;
      }

      this.campaignStatusArr.push({
        title: String(key),
        data: String(value),
      });
    }

    for (const [key, value] of Object.entries(CAMPAIGN_TYPE_ENUM)) {
      this.campaignTypeArr.push({
        id: value,
        name: key,
      });
    }

    this.menuService
      .onItemClick()
      .pipe(
        filter(({ tag }) => tag === 'campaign-context-menu'),
        map(({ item: { data } }) => data)
      )
      .subscribe((action) => {
        switch (Number(action)) {
          case CAMPAIGN_STATUS.ACTIVE:
            this.updateCampaignStatus(ECampaignStatus.ACTIVE);
            break;
          case CAMPAIGN_STATUS.INACTIVE:
            this.updateCampaignStatus(ECampaignStatus.INACTIVE);
            break;
          case CAMPAIGN_STATUS.TERMINATED:
            this.updateCampaignStatus(ECampaignStatus.TERMINATED);
            break;
        }
      });
  }

  ngOnInit(): void {
    this.createForm();
    this.initEmployees();
  }

  ngOnDestroy(): void {
    this.unsubscribe2$.next();
    this.unsubscribe2$.complete();
  }

  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {
      if (changes.hasOwnProperty(propName)) {
        switch (propName) {
          case 'campaign': {
            if (this.campaign) {
              this.campaignForm.patchValue(this.campaign);
              this.campaignForm.patchValue({
                id: this.campaignId,
              });

              if (this.campaign.userProfileId) {
                this.managerId = this.campaign.userProfileId;
              }
            }
          }
        }
      }
    }
  }

  get f(): any {
    return this.campaignForm.controls;
  }

  getStatus(statusId: ECampaignStatus) {
    switch (statusId) {
      case ECampaignStatus.ACTIVE:
        return {
          class: 'success',
          label: CAMPAIGN_STATUS_ITEM.ACTIVE,
        };
      case ECampaignStatus.INACTIVE:
        return {
          class: 'warning',
          label: CAMPAIGN_STATUS_ITEM.INACTIVE,
        };
      case ECampaignStatus.TERMINATED:
        return {
          class: 'danger',
          label: CAMPAIGN_STATUS_ITEM.TERMINATED,
        };
    }
  }

  updateCampaign() {
    this.isSubmitting = true;
    this.campaignForm.disable();

    if (this.campaignForm.invalid) {
      this.isSubmitting = false;
      this.campaignForm.enable();
      this.validateAllFormFields(this.campaignForm);
      return;
    }

    const payload: ICampaign = this.campaignForm.getRawValue();
    this.mainService
      .updateCampaign(payload)
      .toPromise()
      .then(
        () => {
          this.isSubmitting = false;

          Swal.fire('Updated!', '', 'success').then(() => {
            this.campaignForm.enable();
          });

          if (this.campaignId) {
            this.onRefreshComponent.emit();
          }
        },
        (error) => {
          if (error) {
            const err = error.error;
            this.campaignForm.enable();
            this.isSubmitting = false;

            if (err && err.errors) {
              const errors = err.errors;
              if (errors) {
                this.toastService.showErrorToast('Warning!', errors[0]);
                return;
              }
            } else if (err && err.message) {
              this.toastService.showErrorToast('Warning!', err.message);
              return;
            }
          }
        }
      );
  }

  private createForm() {
    this.campaignForm = this.formBuilder.group({
      campaignName: ['', [Validators.required]],
      plan: ['', [Validators.required]],
      client: ['', [Validators.required]],
      started: ['', [Validators.required]],
      clientEmail: [
        '',
        [Validators.required, Validators.pattern(emailValidationRegex)],
      ],
      accountsExecutiveId: [''],
      trelloEditorId: [''],
      id: ['', Validators.required],
      campaignType: ['', Validators.required],
    });
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  private updateCampaignStatus(statusId: ECampaignStatus) {
    const updateCampaignStatus = () => {
      this.updateStatusSub$ = this.mainService
        .changeCampaignStatus({
          campaignId: Number(this.campaign.id) || this.campaignId,
          statusId,
        })
        .subscribe(
          (response) => {
            console.log('updateUserStatus: ', response);

            if (this.campaignId) {
              this.onRefreshComponent.emit();
            }

            Swal.fire('Updated!', '', 'success');
          },
          (err) => {},
          () => this.updateStatusSub$.unsubscribe()
        );
    };

    Swal.fire({
      title: 'Update Campaign status?',
      showCancelButton: true,
      confirmButtonText: `Confirm`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        updateCampaignStatus();
      }
    });
  }

  private async initEmployees() {
    const loading = await this.loadingController.create();
    loading.present();

    const handleResponse = (employeeArr) => {
      this.employeeArr = [...employeeArr];
      loading.dismiss();
    };

    this.store
      .pipe(
        select(mainSelector),
        takeUntil(this.unsubscribe2$),
        map((res) => res.employeeAllStaffs)
      )
      .subscribe(
        (response: IEmployeesResponse) => {
          if (response && response.data) {
            handleResponse(response.data);
          }
        },
        () => {
          loading.dismiss();
        }
      );
  }
}
