import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignDetailsSdrTabComponent } from './campaign-details-sdr-tab.component';

describe('CampaignDetailsSdrTabComponent', () => {
  let component: CampaignDetailsSdrTabComponent;
  let fixture: ComponentFixture<CampaignDetailsSdrTabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CampaignDetailsSdrTabComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignDetailsSdrTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
