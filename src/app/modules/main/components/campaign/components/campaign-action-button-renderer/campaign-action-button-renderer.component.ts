import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { IUserAccount } from 'src/app/modules/auth/models/auth.model';
import { AuthService } from 'src/app/modules/auth/services/auth.service';
import { ECampaignStatus } from 'src/app/shared/constants/enums';
import { MENU_ITEM_NAME } from 'src/app/shared/constants/page.menu';
import {
  CAMPAIGN_STATUS,
  CAMPAIGN_STATUS_ITEM,
  CURRENT_USER,
  PERMISSION_ACTION_DESC,
  USER_ROLE,
} from 'src/app/shared/constants/utils';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';

@Component({
  selector: 'app-campaign-action-button-renderer',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './campaign-action-button-renderer.component.html',
  styleUrls: ['./campaign-action-button-renderer.component.scss'],
})
export class CampaignActionButtonRendererComponent
  implements ICellRendererAngularComp {
  params: any;
  statusId: number;
  managerId: number;

  campaignStatusArr: any[] = [];

  user: IUserAccount;
  private readonly CAMPAIGN_STATUS = CAMPAIGN_STATUS;
  readonly PERMISSION_ACTION_DESC = PERMISSION_ACTION_DESC;
  readonly USER_ROLE = USER_ROLE;
  readonly ECampaignStatus = ECampaignStatus;

  constructor(
    private storage: LocalStorageService,
    private authService: AuthService
  ) {
    for (const [key, value] of Object.entries(this.CAMPAIGN_STATUS)) {
      if (key === 'ARCHIVED') {
        continue;
      }

      this.campaignStatusArr.push({
        title: String(key),
        data: String(value),
      });
    }
  }

  hasFeatureAccess(action: string): boolean {
    this.user = this.storage.getItem(CURRENT_USER);
    return this.authService.hasPermission(
      this.user.role,
      MENU_ITEM_NAME.USERS.TAG,
      action
    );
  }

  agInit(params: any): void {
    this.params = params;

    if (params && params.data) {
      this.statusId = params.data.statusId;
      this.managerId = params.data.managerId;
    }
  }

  refresh(): boolean {
    return false;
  }

  getStatus(statusId: ECampaignStatus) {
    switch (statusId) {
      case ECampaignStatus.ACTIVE:
        return {
          class: 'success',
          label: CAMPAIGN_STATUS_ITEM.ACTIVE,
        };
      case ECampaignStatus.INACTIVE:
        return {
          class: 'warning',
          label: CAMPAIGN_STATUS_ITEM.INACTIVE,
        };
      case ECampaignStatus.TERMINATED:
        return {
          class: 'danger',
          label: CAMPAIGN_STATUS_ITEM.TERMINATED,
        };
      default:
        return {
          class: 'warning',
          label: CAMPAIGN_STATUS_ITEM.INACTIVE,
        };
    }
  }

  viewCampaign() {
    this.params.clicked({
      action: 'view',
      data: this.params.data,
    });
  }

  deleteCampaign() {
    this.params.clicked({
      action: 'delete',
      data: this.params.data,
    });
  }

  toggleAddSDR() {
    this.params.clicked({
      action: 'manage_sdr',
      data: this.params.data,
    });
  }

  toggleAddCampaignManager() {
    this.params.clicked({
      action: 'add_sdrm',
      data: this.params.data,
    });
  }

  toggleUpdateStatus() {
    this.params.clicked({
      action: 'update_campaign_status',
      data: this.params.data,
    });
  }

  viewScheduler() {
    this.params.clicked({
      action: 'view_scheduler',
      data: this.params.data,
    });
  }
}
