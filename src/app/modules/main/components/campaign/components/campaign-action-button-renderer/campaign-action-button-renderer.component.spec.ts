import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignActionButtonRendererComponent } from './campaign-action-button-renderer.component';

describe('CampaignActionButtonRendererComponent', () => {
  let component: CampaignActionButtonRendererComponent;
  let fixture: ComponentFixture<CampaignActionButtonRendererComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CampaignActionButtonRendererComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignActionButtonRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
