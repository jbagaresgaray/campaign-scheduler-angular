import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignSdrEntryComponent } from './campaign-sdr-entry.component';

describe('CampaignSdrEntryComponent', () => {
  let component: CampaignSdrEntryComponent;
  let fixture: ComponentFixture<CampaignSdrEntryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CampaignSdrEntryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignSdrEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
