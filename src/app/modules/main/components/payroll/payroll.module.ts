import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  NbButtonModule,
  NbCardModule,
  NbDatepickerModule,
  NbDialogModule,
  NbFormFieldModule,
  NbIconModule,
  NbInputModule,
  NbListModule,
  NbProgressBarModule,
  NbSelectModule,
  NbSpinnerModule,
  NbTooltipModule,
  NbUserModule,
} from '@nebular/theme';
import { FormsModule } from '@angular/forms';
import { AgGridModule } from 'ag-grid-angular';

import { PayrollRoutingModule } from './payroll-routing.module';
import { PayrollComponent } from './payroll.component';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { PayrollMainComponent } from './payroll-main/payroll-main.component';
import { CheckboxRendererComponent } from './checkbox-renderer/checkbox-renderer.component';
import { IconButtonRendererComponent } from './icon-button-renderer/icon-button-renderer.component';
import { PayrollDialogComponent } from './payroll-dialog/payroll-dialog.component';

const COMPONENTS = [
  PayrollComponent,
  PayrollMainComponent,
  IconButtonRendererComponent,
  CheckboxRendererComponent,
  PayrollDialogComponent
]
@NgModule({
  declarations: [...COMPONENTS],
  imports: [
    CommonModule,
    PayrollRoutingModule,
    ComponentsModule,
    NbButtonModule,
    NbCardModule,
    NbIconModule,
    NbInputModule,
    NbTooltipModule,
    NbProgressBarModule,
    NbListModule,
    NbUserModule,
    NbDatepickerModule,
    NbSelectModule,
    NbSpinnerModule,
    NbFormFieldModule,
    FormsModule,
    AgGridModule,
    NbDialogModule.forRoot(),
  ],
})
export class PayrollModule {}
