import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-payroll',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './payroll.component.html',
  styleUrls: ['./payroll.component.scss'],
})
export class PayrollComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
