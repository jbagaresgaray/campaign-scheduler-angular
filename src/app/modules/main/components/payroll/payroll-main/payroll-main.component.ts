import { Component, OnInit, ViewChild } from '@angular/core';
import { AgGridAngular } from 'ag-grid-angular';
import { GridOptions } from 'ag-grid-community';
import { PAYROLL_COLUMNS } from 'src/app/shared/constants/payroll';
import { GLOBAL_AG_GRID_OPTIONS } from 'src/app/shared/constants/utils';
import isEmpty from 'lodash-es/isEmpty';
import { PayrollData } from '../../../models/payroll.model';
import { IconButtonRendererComponent } from '../icon-button-renderer/icon-button-renderer.component';
import { CheckboxRendererComponent } from '../checkbox-renderer/checkbox-renderer.component';
import { PayrollDialogComponent } from '../payroll-dialog/payroll-dialog.component';
import { BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-payroll-main',
  templateUrl: './payroll-main.component.html',
  styleUrls: ['./payroll-main.component.scss']
})
export class PayrollMainComponent implements OnInit {

  payrollData: PayrollData[] = [
    {
      action: 'test',
      employeeName: 'Deun Randy Bucoy',
      campaign: 'Growthlever',
      mon: true,
      tue: false,
      wed: false,
      thu: false,
      fri: false,
      daysActive: 0,
      incentiveType: 'Appointment',
      wage: 3,
      total: 0,
    }
  ];

  private gridApi;
  @ViewChild('agGrid') agGrid: AgGridAngular;

  payrollColumnDefs: any[] = [];

  selectedPayroll: PayrollData;

  private readonly PAYROLL_COLUMNS = PAYROLL_COLUMNS;

  headerComponentParams = {
    // headerComponentParams: {
    //   template:
    //     '<div class="ag-cell-label-container" role="presentation">' +
    //     '  <span ref="eMenu" class="ag-header-icon ag-header-cell-menu-button"></span>' +
    //     '  <div ref="eLabel" class="ag-header-cell-label" role="presentation">' +
    //     '    <span ref="eSortOrder" class="ag-header-icon ag-sort-order"></span>' +
    //     '    <span ref="eSortAsc" class="ag-header-icon ag-sort-ascending-icon"></span>' +
    //     '    <span ref="eSortDesc" class="ag-header-icon ag-sort-descending-icon"></span>' +
    //     '    <span ref="eSortNone" class="ag-header-icon ag-sort-none-icon"></span>' +
    //     '    <span ref="eText" class="ag-header-cell-text" role="columnheader" style="white-space: normal;"></span>' +
    //     '    <span ref="eFilter" class="ag-header-icon ag-filter-icon"></span>' +
    //     '  </div>' +
    //     '</div>',
    // },
  };

  gridOptions: GridOptions = {};

  frameworkComponents = {
    checkboxRenderer: CheckboxRendererComponent,
    // actionButtonRenderer: ActionButtonRendererComponent,
    iconButtonRenderer: IconButtonRendererComponent,
  };

  constructor(
    private modalService: BsModalService,
  ) {
    this.gridOptions = {
      ...GLOBAL_AG_GRID_OPTIONS,
      defaultColDef: {
        ...GLOBAL_AG_GRID_OPTIONS.defaultColDef,
        // ...this.headerComponentParams,
      },
    };

    for (const [key, value] of Object.entries(this.PAYROLL_COLUMNS)) {
      this.payrollColumnDefs.push({
        field: String(value.FIELD),
        headerName: String(value.LABEL),
        sortable: false,
        pinned: value.PINNED,
        headerClass: value.HEADERCLASS,
        cellStyle: {
          textAlign: value.TEXTALIGN,
        },
        editable: false,
        type: 'nonEditableColumn',
        width: value.WIDTH,
        cellRenderer: value.CELL_RENDERER,
        hide: value.HIDE,
        wrapText: value.WRAPTEXT,
        resizable: value.RESIZABLE,
        cellRendererParams: () => {
          if (
            value.LABEL === 'Action' ||
            value.CELL_RENDERER === 'iconButtonRenderer'
          ) {
            return {
              clicked: (field: any) => {
                const { action, data } = field;
                this.onGridActionButton(action, data);
              },
            };
          }
          // else if (
          //   value.CELL_RENDERER === 'checkboxRenderer'
          // ) {
          //   return {
          //     clicked: () => {
          //     }
          //   };
          // }
        },
      });
    }


  }

  ngOnInit(): void {
  }

  onGridReady(params) {
    this.gridApi = params.api;
    if (this.agGrid && this.agGrid.api) {
      if (isEmpty(this.payrollData)) {
        this.agGrid.api.hideOverlay();
      }
    }
  }

  onGridActionButton(action: string, data: any) {
    const initialState = {
      data: data
    }
    this.modalService.show(PayrollDialogComponent, {
      initialState,
      class: 'modal-lg',
    });
    // this.selectedPayroll = this.formatSchedulerData(data);
    // if (action === FORMSTATE.DELETE) {
    //   this.deleteSchedule(data);
    // } else if (action === FORMSTATE.EDIT) {
    //   this.updateSchedule(data);
    // } else if (
    //   action === SCHEDULER_MORE_ACTION.NEUTRAL_POSITIVE_RESPONSE.FIELD
    // ) {
    //   this.onNeutralPositive(
    //     momentTZ(new Date()).format('YYYY-MM-DD HH:mm:ss')
    //   );
    // } else if (action === SCHEDULER_MORE_ACTION.NEGATIVE_NO_FOLLOW_UP.FIELD) {
    //   this.onNegativeNoFollowUp(
    //     momentTZ(new Date()).format('YYYY-MM-DD HH:mm:ss')
    //   );
    // }
  }



  headerHeightSetter() {
    const padding = 40;
    const allColumnIds = [];

    const height = this.headerHeightGetter() + padding;
    this.gridApi.setHeaderHeight(height);
    this.gridApi.resetRowHeights();

    this.agGrid.columnApi.getAllColumns().forEach((column: any) => {
      allColumnIds.push(column.colId);
    });
    this.agGrid.columnApi.autoSizeColumns(allColumnIds, false);
  }

  private headerHeightGetter() {
    const columnHeaderTexts = document.querySelectorAll('.ag-header-cell-text');
    const columnHeaderTextsArray = [];
    columnHeaderTexts.forEach((node) => columnHeaderTextsArray.push(node));
    const clientHeights = columnHeaderTextsArray.map(
      (headerText) => headerText.clientHeight
    );
    const tallestHeaderTextHeight = Math.max(...clientHeights);
    return tallestHeaderTextHeight;
  }

}
