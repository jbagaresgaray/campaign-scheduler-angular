import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { PayrollData } from '../../../models/payroll.model';

@Component({
  selector: 'app-payroll-dialog',
  templateUrl: './payroll-dialog.component.html',
  styleUrls: ['./payroll-dialog.component.scss']
})
export class PayrollDialogComponent implements OnInit {

  data: PayrollData;
  constructor(
    public bsModalRef: BsModalRef
  ) { }

  ngOnInit(): void {

  }

}
