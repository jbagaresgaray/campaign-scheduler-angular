import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IconButtonRendererComponent } from './icon-button-renderer.component';

describe('IconButtonRendererComponent', () => {
  let component: IconButtonRendererComponent;
  let fixture: ComponentFixture<IconButtonRendererComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IconButtonRendererComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IconButtonRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
