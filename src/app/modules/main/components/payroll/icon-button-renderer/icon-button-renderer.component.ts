import { Component, OnInit } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import {
  ICellRendererParams,
  IAfterGuiAttachedParams,
} from 'ag-grid-community';
import { PAYROLL_MORE_ACTION } from 'src/app/shared/constants/payroll';
import { PayrollData } from '../../../models/payroll.model';

@Component({
  selector: 'app-icon-button-renderer',
  templateUrl: './icon-button-renderer.component.html',
  styleUrls: ['./icon-button-renderer.component.scss'],
})
export class IconButtonRendererComponent implements ICellRendererAngularComp {
  params: any;
  payroll: PayrollData;
  readonly MORE_ACTION = PAYROLL_MORE_ACTION;

  constructor() {}

  agInit(params: any): void {
    this.params = params;
    const data = params.data;
    if (params && data) {
      this.payroll = data;
    }
  }

  refresh(): boolean {
    return false;
  }

  ngOnInit(): void {}

  onViewPayroll() {
    this.params.clicked({
      data: this.params.data,
    });
  }
}
