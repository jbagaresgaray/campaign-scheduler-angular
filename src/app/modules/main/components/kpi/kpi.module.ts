import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  NbButtonModule,
  NbCardModule,
  NbDatepickerModule,
  NbFormFieldModule,
  NbIconModule,
  NbInputModule,
  NbListModule,
  NbProgressBarModule,
  NbSelectModule,
  NbSpinnerModule,
  NbTooltipModule,
  NbUserModule,
} from '@nebular/theme';
import { FormsModule } from '@angular/forms';
import { AgGridModule } from 'ag-grid-angular';

import { KpiRoutingModule } from './kpi-routing.module';
import { KpiComponent } from './kpi.component';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { KpiCardHeaderComponent } from './components/kpi-card-header/kpi-card-header.component';
import { KpiActivityListComponent } from './components/kpi-activity-list/kpi-activity-list.component';

@NgModule({
  declarations: [KpiComponent, KpiCardHeaderComponent, KpiActivityListComponent],
  imports: [
    CommonModule,
    KpiRoutingModule,
    ComponentsModule,
    NbButtonModule,
    NbCardModule,
    NbIconModule,
    NbInputModule,
    NbTooltipModule,
    NbProgressBarModule,
    NbListModule,
    NbUserModule,
    NbDatepickerModule,
    NbSelectModule,
    NbSpinnerModule,
    NbFormFieldModule,
    FormsModule,
    AgGridModule,
  ],
})
export class KpiModule {}
