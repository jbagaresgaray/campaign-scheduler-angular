import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KpiActivityListComponent } from './kpi-activity-list.component';

describe('KpiActivityListComponent', () => {
  let component: KpiActivityListComponent;
  let fixture: ComponentFixture<KpiActivityListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KpiActivityListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KpiActivityListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
