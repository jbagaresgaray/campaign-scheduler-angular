import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-kpi-activity-list',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './kpi-activity-list.component.html',
  styleUrls: ['./kpi-activity-list.component.scss'],
})
export class KpiActivityListComponent implements OnInit {
  @Input() activityData: any;

  constructor() {}

  ngOnInit(): void {
    console.log('activityData: ', this.activityData);
  }

  trackByDate(_, item) {
    return item.date;
  }
}
