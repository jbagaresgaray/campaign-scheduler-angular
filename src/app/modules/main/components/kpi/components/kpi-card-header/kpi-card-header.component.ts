import {
  Component,
  EventEmitter,
  Input,
  Output,
  ViewEncapsulation,
} from '@angular/core';
import { ELegendItemColor } from 'src/app/shared/constants/enums';

@Component({
  selector: 'app-kpi-card-header',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './kpi-card-header.component.html',
  styleUrls: ['./kpi-card-header.component.scss'],
})
export class KpiCardHeaderComponent {
  @Input() type: string = 'week';
  @Input()
  legendItems: {
    iconColor: ELegendItemColor;
    title: string;
    value: number;
  }[] = [];
  @Output() periodChange = new EventEmitter<string>();

  types: string[] = ['week', 'month', 'year'];

  constructor() {
    console.log('summaryLegend: ', this.legendItems);
  }

  changePeriod(period: string): void {
    this.type = period;
    this.periodChange.emit(period);
  }
}
