import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ELegendItemColor } from 'src/app/shared/constants/enums';
import { KPIList } from 'src/app/shared/mocks/data/kpi-data';
import { TrafficList } from 'src/app/shared/mocks/data/traffic-bar';
import { KPIListService } from 'src/app/shared/mocks/services/kpi.mock';

@Component({
  selector: 'app-kpi',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './kpi.component.html',
  styleUrls: ['./kpi.component.scss'],
})
export class KpiComponent implements OnInit, OnDestroy {
  period: string = 'week';
  kpiListData: KPIList;

  summaryLegend = [
    {
      title: 'Invites',
      value: 3654,
      iconColor: ELegendItemColor.PRIMARY,
    },
    {
      title: 'Connections',
      value: 946,
      iconColor: ELegendItemColor.SUCCESS,
    },
    {
      title: 'Message Sent',
      value: 654,
      iconColor: ELegendItemColor.WARNING,
    },
    {
      title: 'Total Actions',
      value: 230,
      iconColor: ELegendItemColor.DANGER,
    },
  ];

  private unsubscribe = new Subject<any>();

  constructor(private trafficListService: KPIListService) {
    this.getKPIData(this.period);
  }

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  setPeriodAngGetData(value: string): void {
    this.period = value;

    this.getKPIData(value);
  }

  getKPIData(period: string) {
    this.trafficListService
      .getKPIListData(period)
      .pipe(takeUntil(this.unsubscribe))
      .subscribe((data) => {
        this.kpiListData = data;
        console.log('this.kpiListData: ', this.kpiListData);
      });
  }
}
