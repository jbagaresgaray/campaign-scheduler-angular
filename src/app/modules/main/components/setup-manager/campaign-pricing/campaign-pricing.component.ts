import {
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { Store } from '@ngrx/store';
import { AgGridAngular } from 'ag-grid-angular';
import { GridOptions } from 'ag-grid-community';
import { BsModalService } from 'ngx-bootstrap/modal';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { Subject, Subscription } from 'rxjs';
import Swal from 'sweetalert2';
import isEmpty from 'lodash-es/isEmpty';

import { IUserAccount } from 'src/app/modules/auth/models/auth.model';
import {
  FORMSTATE,
  GLOBAL_AG_GRID_OPTIONS,
  MAX_ITEMS_PER_PAGE,
} from 'src/app/shared/constants/utils';
import { DataLoaderService } from 'src/app/shared/services/data-loader.service';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { ICampaignPricing } from '../../../models/campaign.model';
import { MainService } from '../../../services/main.service';
import { MainState } from '../../../store/main.reducers';
import { CAMPAIGN_PRICING_COLUMNS } from './campaign-pricing.constant';
import { CampaignPricingButtonRendererComponent } from './components/campaign-pricing-button-renderer/campaign-pricing-button-renderer.component';
import { IGenericParams } from '../../../models/generic.model';
import { CampaignPricingEntryComponent } from './components/campaign-pricing-entry/campaign-pricing-entry.component';
import { CampaignPricingDurationRendererComponent } from './components/campaign-pricing-duration-renderer/campaign-pricing-duration-renderer.component';
import { CampaignPricingAmountRendererComponent } from './components/campaign-pricing-amount-renderer/campaign-pricing-amount-renderer.component';

@Component({
  selector: 'app-campaign-pricing',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './campaign-pricing.component.html',
  styleUrls: ['./campaign-pricing.component.scss'],
})
export class CampaignPricingComponent implements OnInit, OnDestroy {
  user: IUserAccount;
  pricingArr: ICampaignPricing[] = [];
  fakeArr: any[] = [];

  itemsPerPage = MAX_ITEMS_PER_PAGE;
  totalItems: number;
  currentPage = 1;
  showContent = false;

  columnDefs: any[] = [];
  private gridApi;
  readonly GRID_COLUMNS = CAMPAIGN_PRICING_COLUMNS;
  frameworkComponents = {
    actionButtonRenderer: CampaignPricingButtonRendererComponent,
    durationBadgeRenderer: CampaignPricingDurationRendererComponent,
    pricingAmountRender: CampaignPricingAmountRendererComponent,
  };
  gridOptions: GridOptions = GLOBAL_AG_GRID_OPTIONS;
  @ViewChild('agGrid') agGrid: AgGridAngular;

  private unsubscribe$ = new Subject<any>();
  private deleteSub$: Subscription = new Subscription();

  constructor(
    private store: Store<MainState>,
    private dataLoader: DataLoaderService,
    private storage: LocalStorageService,
    private modalService: BsModalService,
    private mainService: MainService
  ) {
    this.fakeArr = Array.from({
      length: 10,
    });

    for (const [key, value] of Object.entries(this.GRID_COLUMNS)) {
      this.columnDefs.push({
        field: String(value.FIELD),
        headerName: String(value.LABEL),
        sortable: false,
        pinned: value.PINNED,
        headerClass: value.HEADERCLASS,
        cellStyle: {
          textAlign: value.TEXTALIGN,
        },
        editable: false,
        type: 'nonEditableColumn',
        width: value.WIDTH,
        cellRenderer: value.CELL_RENDERER,
        hide: value.HIDE,
        wrapText: value.WRAPTEXT,
        cellRendererParams: () => {
          if (value.LABEL === 'Action') {
            return {
              clicked: (field: any) => {
                const { action, data } = field;
                this.onGridActionButton(action, data);
              },
            };
          }
        },
      });
    }
  }

  ngOnInit(): void {
    this.initCampaignPricing();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  onGridReady(params) {
    this.gridApi = params.api;
  }

  onGridActionButton(action: string, data: any) {
    if (action === FORMSTATE.DELETE) {
      this.deletePricing(data);
    } else if (action === FORMSTATE.UPDATE) {
      this.updatePricing(data);
    }
  }

  pageChanged(event: PageChangedEvent) {
    console.log('event: ', event);
    const { page } = event;
    this.refreshPricing(page);
  }

  toogleNew() {
    const initialState = {
      action: FORMSTATE.ADD,
    };
    this.modalService.show(CampaignPricingEntryComponent, {
      initialState,
      // class: 'modal-lg',
    });
  }

  search(event: any) {
    const val = event.target.value;
    this.refreshPricing(1, val);
  }

  clearSearch() {
    this.refreshPricing();
  }

  private updatePricing(pricing: ICampaignPricing) {
    const initialState = {
      action: FORMSTATE.UPDATE,
      pricingId: pricing.id,
      pricing,
    };
    this.modalService.show(CampaignPricingEntryComponent, {
      initialState,
      // class: 'modal-lg',
    });
  }

  private deletePricing(pricing: ICampaignPricing) {
    Swal.fire({
      title: 'Do you want to delete this pricing?',
      showCancelButton: true,
      confirmButtonText: `Confirm`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        // this.deleteSub$ = this.mainService
        //   .deleteDepartment(pricing.id)
        //   .subscribe(() => {
        //     this.deleteSub$.unsubscribe();
        //     this.refreshDepartment();
        //   });

        setTimeout(() => {
          Swal.fire('Deleted!', '', 'success');
        }, 800);
      }
    });
  }

  private initCampaignPricing() {
    const getRandom = (roundTo: number) => Math.round(Math.random() * roundTo);
    for (let index = 0; index < 10; index++) {
      this.pricingArr.push({
        name: 'Pricing-' + index,
        pricing: getRandom(1000),
        durationId: getRandom(3),
      });
    }
  }

  private refreshPricing(page: number = 1, Search?: string) {
    const params: IGenericParams = {
      PageIndex: page,
      PageSize: this.itemsPerPage,
    };

    if (Search) {
      params.Search = Search;
    }

    this.showContent = false;
    // this.dataLoader.getAllDepartments(params);
  }
}
