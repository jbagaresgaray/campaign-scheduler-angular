import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  NbButtonModule,
  NbCardModule,
  NbDatepickerModule,
  NbFormFieldModule,
  NbIconModule,
  NbInputModule,
  NbListModule,
  NbProgressBarModule,
  NbSelectModule,
  NbSpinnerModule,
  NbTooltipModule,
  NbUserModule,
} from '@nebular/theme';
import { AgGridModule } from 'ag-grid-angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CampaignPricingRoutingModule } from './campaign-pricing-routing.module';
import { CampaignPricingComponent } from './campaign-pricing.component';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { CampaignPricingButtonRendererComponent } from './components/campaign-pricing-button-renderer/campaign-pricing-button-renderer.component';
import { CampaignPricingEntryComponent } from './components/campaign-pricing-entry/campaign-pricing-entry.component';
import { CampaignPricingDurationRendererComponent } from './components/campaign-pricing-duration-renderer/campaign-pricing-duration-renderer.component';
import { CampaignPricingAmountRendererComponent } from './components/campaign-pricing-amount-renderer/campaign-pricing-amount-renderer.component';

@NgModule({
  declarations: [CampaignPricingComponent, CampaignPricingButtonRendererComponent, CampaignPricingEntryComponent, CampaignPricingDurationRendererComponent, CampaignPricingAmountRendererComponent],
  imports: [
    CommonModule,
    CampaignPricingRoutingModule,
    ComponentsModule,
    NbButtonModule,
    NbCardModule,
    NbIconModule,
    NbInputModule,
    NbTooltipModule,
    NbProgressBarModule,
    NbListModule,
    NbUserModule,
    NbDatepickerModule,
    NbSelectModule,
    NbSpinnerModule,
    NbFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    AgGridModule,
  ],
})
export class CampaignPricingModule {}
