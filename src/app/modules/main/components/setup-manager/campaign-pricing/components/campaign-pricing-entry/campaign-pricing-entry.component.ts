import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';

import Swal from 'sweetalert2';
import { Subject } from 'rxjs';
import {
  CAMPAIGN_DURATION,
  emailValidationRegex,
  FORMSTATE,
} from 'src/app/shared/constants/utils';
import { MainService } from 'src/app/modules/main/services/main.service';
import { DataLoaderService } from 'src/app/shared/services/data-loader.service';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { ToastAlertService } from 'src/app/shared/services/toast-alert.service';
import { ICampaignPricing } from 'src/app/modules/main/models/campaign.model';

@Component({
  selector: 'app-campaign-pricing-entry',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './campaign-pricing-entry.component.html',
  styleUrls: ['./campaign-pricing-entry.component.scss'],
})
export class CampaignPricingEntryComponent implements OnInit, OnDestroy {
  pricingForm: FormGroup;
  isFormValid = false;
  isSubmitting = false;

  campaignDurationArr: any[] = [];
  action: string;
  pricingId: number;
  buttonLabel: string;
  unsubscribe$ = new Subject<any>();
  unsubscribe2$ = new Subject<any>();

  constructor(
    private formBuilder: FormBuilder,
    public bsModalRef: BsModalRef,
    private mainService: MainService,
    private dataLoader: DataLoaderService,
    private toastService: ToastAlertService,
    private storage: LocalStorageService
  ) {
    for (const [_key, value] of Object.entries(CAMPAIGN_DURATION)) {
      this.campaignDurationArr.push({
        title: value.LABEL,
        value: value.VALUE,
      });
      console.log('this.campaignDurationArr: ', this.campaignDurationArr);
    }
  }

  ngOnInit(): void {
    this.buttonLabel = this.action === FORMSTATE.UPDATE ? 'Update' : 'Save';
    this.createForm();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();

    this.unsubscribe2$.next();
    this.unsubscribe2$.complete();
  }

  get f(): any {
    return this.pricingForm.controls;
  }

  createPricing() {
    this.isSubmitting = true;
    this.pricingForm.disable();

    if (this.pricingForm.invalid) {
      this.isSubmitting = false;
      this.pricingForm.enable();
      this.validateAllFormFields(this.pricingForm);
      return;
    }

    const payload: ICampaignPricing = this.pricingForm.getRawValue();

    setTimeout(() => {
      this.isSubmitting = false;

      Swal.fire(
        this.action === FORMSTATE.ADD ? 'Created!' : 'Updated!',
        '',
        'success'
      ).then(() => this.bsModalRef.hide());
    }, 800);
  }

  private createForm() {
    this.pricingForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      durationId: ['', [Validators.required]],
      pricing: ['', [Validators.required]],
      id: [''],
    });
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
}
