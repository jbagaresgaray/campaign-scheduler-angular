import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignPricingEntryComponent } from './campaign-pricing-entry.component';

describe('CampaignPricingEntryComponent', () => {
  let component: CampaignPricingEntryComponent;
  let fixture: ComponentFixture<CampaignPricingEntryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CampaignPricingEntryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignPricingEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
