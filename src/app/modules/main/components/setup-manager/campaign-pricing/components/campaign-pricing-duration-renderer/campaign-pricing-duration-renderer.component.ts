import { Component, ViewEncapsulation } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { CAMPAIGN_DURATION } from 'src/app/shared/constants/utils';

@Component({
  selector: 'app-campaign-pricing-duration-renderer',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './campaign-pricing-duration-renderer.component.html',
  styleUrls: ['./campaign-pricing-duration-renderer.component.scss'],
})
export class CampaignPricingDurationRendererComponent
  implements ICellRendererAngularComp {
  params: any;
  durationId: any;

  constructor() {}

  agInit(params: any): void {
    this.params = params;
    if (params && params.data) {
      this.durationId = params.data.durationId;
    }
  }

  refresh(): boolean {
    return false;
  }

  getStatus(durationId: number) {
    switch (durationId) {
      case CAMPAIGN_DURATION.MONTHLY.VALUE:
        return {
          class: 'primary',
          label: CAMPAIGN_DURATION.MONTHLY.LABEL,
        };
      case CAMPAIGN_DURATION.QUARTERLY.VALUE:
        return {
          class: 'success',
          label: CAMPAIGN_DURATION.QUARTERLY.LABEL,
        };
      case CAMPAIGN_DURATION.ANNUAL.VALUE:
        return {
          class: 'danger',
          label: CAMPAIGN_DURATION.ANNUAL.LABEL,
        };
    }
  }
}
