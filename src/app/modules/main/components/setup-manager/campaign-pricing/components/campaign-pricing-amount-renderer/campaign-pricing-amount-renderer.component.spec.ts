import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignPricingAmountRendererComponent } from './campaign-pricing-amount-renderer.component';

describe('CampaignPricingAmountRendererComponent', () => {
  let component: CampaignPricingAmountRendererComponent;
  let fixture: ComponentFixture<CampaignPricingAmountRendererComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CampaignPricingAmountRendererComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignPricingAmountRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
