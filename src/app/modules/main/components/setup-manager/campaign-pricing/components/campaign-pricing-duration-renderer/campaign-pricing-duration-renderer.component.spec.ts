import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignPricingDurationRendererComponent } from './campaign-pricing-duration-renderer.component';

describe('CampaignPricingDurationRendererComponent', () => {
  let component: CampaignPricingDurationRendererComponent;
  let fixture: ComponentFixture<CampaignPricingDurationRendererComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CampaignPricingDurationRendererComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignPricingDurationRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
