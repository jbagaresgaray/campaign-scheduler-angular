import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignPricingButtonRendererComponent } from './campaign-pricing-button-renderer.component';

describe('CampaignPricingButtonRendererComponent', () => {
  let component: CampaignPricingButtonRendererComponent;
  let fixture: ComponentFixture<CampaignPricingButtonRendererComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CampaignPricingButtonRendererComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignPricingButtonRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
