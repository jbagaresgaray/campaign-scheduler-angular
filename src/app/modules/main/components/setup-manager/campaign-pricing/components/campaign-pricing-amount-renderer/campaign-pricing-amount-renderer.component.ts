import { Component, ViewEncapsulation } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
  selector: 'app-campaign-pricing-amount-renderer',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './campaign-pricing-amount-renderer.component.html',
  styleUrls: ['./campaign-pricing-amount-renderer.component.scss'],
})
export class CampaignPricingAmountRendererComponent
  implements ICellRendererAngularComp {
  params: any;
  pricing: any;

  constructor() {}

  ngOnInit(): void {}

  agInit(params: any): void {
    this.params = params;
    if (params && params.data) {
      this.pricing = params.data.pricing;
    }
  }

  refresh(): boolean {
    return false;
  }
}
