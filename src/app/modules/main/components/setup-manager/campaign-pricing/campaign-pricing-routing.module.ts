import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CampaignPricingComponent } from './campaign-pricing.component';

const routes: Routes = [{ path: '', component: CampaignPricingComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CampaignPricingRoutingModule { }
