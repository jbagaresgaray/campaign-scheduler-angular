import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignPricingComponent } from './campaign-pricing.component';

describe('CampaignPricingComponent', () => {
  let component: CampaignPricingComponent;
  let fixture: ComponentFixture<CampaignPricingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CampaignPricingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignPricingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
