import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  NbButtonModule,
  NbCardModule,
  NbContextMenuModule,
  NbDatepickerModule,
  NbIconModule,
  NbInputModule,
  NbListModule,
  NbProgressBarModule,
  NbSelectModule,
  NbTooltipModule,
  NbUserModule,
} from '@nebular/theme';
import { AgGridModule } from 'ag-grid-angular';

import { FormsModule } from '@angular/forms';

import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { UsersActionButtonRendererComponent } from './components/users-action-button-renderer/users-action-button-renderer.component';

@NgModule({
  declarations: [UsersComponent, UsersActionButtonRendererComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    NbButtonModule,
    NbCardModule,
    NbIconModule,
    NbInputModule,
    NbTooltipModule,
    NbProgressBarModule,
    NbListModule,
    NbUserModule,
    NbDatepickerModule,
    NbSelectModule,
    NbContextMenuModule,
    FormsModule,
    ComponentsModule,
    AgGridModule,
  ],
})
export class UsersModule {}
