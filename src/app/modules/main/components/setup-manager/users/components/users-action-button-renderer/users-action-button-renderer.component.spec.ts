import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersActionButtonRendererComponent } from './users-action-button-renderer.component';

describe('UsersActionButtonRendererComponent', () => {
  let component: UsersActionButtonRendererComponent;
  let fixture: ComponentFixture<UsersActionButtonRendererComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UsersActionButtonRendererComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersActionButtonRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
