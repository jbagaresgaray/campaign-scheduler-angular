import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { IUserAccount } from 'src/app/modules/auth/models/auth.model';
import { AuthService } from 'src/app/modules/auth/services/auth.service';
import { MENU_ITEM_NAME } from 'src/app/shared/constants/page.menu';
import {
  CURRENT_USER,
  PERMISSION_ACTION_DESC,
  USER_ACCOUNT_STATUS,
} from 'src/app/shared/constants/utils';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';

@Component({
  selector: 'app-users-action-button-renderer',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './users-action-button-renderer.component.html',
  styleUrls: ['./users-action-button-renderer.component.scss'],
})
export class UsersActionButtonRendererComponent implements ICellRendererAngularComp{
  params: any;
  user: IUserAccount;
  userStatusArr: NbMenuItem[] = [];

  readonly VIEW_TOOLTIP = 'VIEW';
  readonly EDIT_TOOLTIP = 'Update Status';

  readonly PERMISSION_ACTION_DESC = PERMISSION_ACTION_DESC;
  readonly USER_ACCOUNT_STATUS = USER_ACCOUNT_STATUS;

  constructor(
    private storage: LocalStorageService,
    private authService: AuthService
  ) {
    for (const [key, value] of Object.entries(this.USER_ACCOUNT_STATUS)) {
      this.userStatusArr.push({
        title: String(value),
        data: String(key),
      });
    }
  }

  hasFeatureAccess(action): boolean {
    this.user = this.storage.getItem(CURRENT_USER);
    return this.authService.hasPermission(
      this.user.role,
      MENU_ITEM_NAME.USERS.TAG,
      action
    );
  }

  agInit(params: any): void {
    this.params = params;
  }

  refresh(): boolean {
    return false;
  }

  viewEmployee() {
    this.params.clicked({
      action: 'view',
      data: this.params.data,
    });
  }

  toggleUpdateStatus() {
    this.params.clicked({
      action: 'update_status',
      data: this.params.data,
    });
  }
}
