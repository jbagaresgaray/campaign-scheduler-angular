import {
  IUserAccounts,
  IUserAccountsResponse,
} from './../../../models/users.model';
import { mainSelector } from './../../../store/main.selectors';
import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import {
  NbContextMenuDirective,
  NbMenuItem,
  NbMenuService,
} from '@nebular/theme';
import { select, Store } from '@ngrx/store';
import { filter, map, takeUntil } from 'rxjs/operators';
import { Subject, Subscription } from 'rxjs';
import Swal from 'sweetalert2';
import cloneDeep from 'lodash-es/cloneDeep';
import { AgGridAngular } from 'ag-grid-angular';
import { GridOptions } from 'ag-grid-community';

import {
  ADMIN_USER_ACCOUNTS,
  GLOBAL_AG_GRID_OPTIONS,
  MAX_ITEMS_PER_PAGE,
  USER_ACCOUNT_STATUS,
} from 'src/app/shared/constants/utils';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';

import { MainState } from '../../../store/main.reducers';
import { EUserAccountStatus } from 'src/app/shared/constants/enums';
import { UsersService } from '../../../services/users.service';
import { DataLoaderService } from 'src/app/shared/services/data-loader.service';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { IUserAccount } from 'src/app/modules/auth/models/auth.model';
import { IGenericParams } from '../../../models/generic.model';
import { USERS_COLUMNS } from './user.constants';
import { UsersActionButtonRendererComponent } from './components/users-action-button-renderer/users-action-button-renderer.component';
import { StatusCellRendererComponent } from 'src/app/shared/components/status-cell-renderer/status-cell-renderer.component';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-users',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
  user: IUserAccount;
  usersArr: IUserAccounts[] = [];
  selectedUser: IUserAccounts = null;

  fakeArr: any[] = [];
  @ViewChild(NbContextMenuDirective) contextMenu: NbContextMenuDirective;

  itemsPerPage = MAX_ITEMS_PER_PAGE;
  totalItems: number;
  currentPage = 1;
  numOfPages = 0;
  showContent = false;

  unsubscribe$ = new Subject<any>();
  private updateStatusSub$: Subscription = new Subscription();

  schedulerColumnDefs: any[] = [];
  private gridApi;
  readonly USERS_COLUMNS = USERS_COLUMNS;
  frameworkComponents = {
    actionButtonRenderer: UsersActionButtonRendererComponent,
    statusCellRenderer: StatusCellRendererComponent,
  };
  gridOptions: GridOptions = GLOBAL_AG_GRID_OPTIONS;
  @ViewChild('agGrid') agGrid: AgGridAngular;

  constructor(
    private store: Store<MainState>,
    private menuService: NbMenuService,
    private userService: UsersService,
    private dataLoader: DataLoaderService,
    private storage: LocalStorageService,
    private loadingController: LoadingController
  ) {
    this.fakeArr = Array.from({
      length: 15,
    });

    for (const [key, value] of Object.entries(this.USERS_COLUMNS)) {
      this.schedulerColumnDefs.push({
        field: String(value.FIELD),
        headerName: String(value.LABEL),
        sortable: false,
        pinned: value.PINNED,
        headerClass: value.HEADERCLASS,
        cellStyle: {
          textAlign: value.TEXTALIGN,
        },
        editable: false,
        type: 'nonEditableColumn',
        width: value.WIDTH,
        cellRenderer: value.CELL_RENDERER,
        hide: value.HIDE,
        wrapText: value.WRAPTEXT,
        cellRendererParams: () => {
          if (value.LABEL === 'Action') {
            return {
              clicked: (field: any) => {
                const { action, data } = field;
                this.onGridActionButton(action, data);
              },
            };
          }
        },
      });
    }

    this.menuService
      .onItemClick()
      .pipe(
        filter(({ tag }) => tag === 'users-context-menu'),
        map(({ item: { data } }) => data)
      )
      .subscribe((action) => {
        switch (action) {
          case USER_ACCOUNT_STATUS.ACTIVE.toUpperCase():
            this.updateAccountStatus(EUserAccountStatus.ACTIVE);
            break;
          case USER_ACCOUNT_STATUS.SUSPENDED.toUpperCase():
            this.updateAccountStatus(EUserAccountStatus.SUSPENDED);
            break;
          case USER_ACCOUNT_STATUS.TERMINATED.toUpperCase():
            this.updateAccountStatus(EUserAccountStatus.TERMINATED);
            break;
          case USER_ACCOUNT_STATUS.WAITING.toUpperCase():
            this.updateAccountStatus(EUserAccountStatus.WAITING);
            break;
        }
      });
  }

  ngOnInit(): void {
    this.initUserAccounts();
  }

  onGridReady(params) {
    this.gridApi = params.api;
    // if (this.agGrid && this.agGrid.api) {
    //   this.agGrid.api.showLoadingOverlay();
    // }
  }

  onGridActionButton(action: string, data: any) {
    if (action === 'update_status') {
      this.toggleUpdateStatus(data);
    }
  }

  viewEmployee() {}

  // deleteAccount() {
  //   Swal.fire({
  //     title: 'Do you want to remove this account?',
  //     showCancelButton: true,
  //     confirmButtonText: `Confirm`,
  //   }).then((result) => {
  //     /* Read more about isConfirmed, isDenied below */
  //     if (result.isConfirmed) {
  //       Swal.fire('Removed!', '', 'success');
  //     }
  //   });
  // }

  pageChanged(event: PageChangedEvent) {
    const { page } = event;
    this.refreshUserAccounts(page);
  }

  search(event: any) {
    const val = event.target.value;
    this.refreshUserAccounts(1, val);
  }

  clearSearch() {
    this.refreshUserAccounts();
  }

  private async initUserAccounts() {
    // const CACHE_DATA = this.storage.getItem(ADMIN_USER_ACCOUNTS);

    const loading = await this.loadingController.create();
    loading.dismiss();

    const handleResponse = (userAccounts) => {
      this.usersArr = [...userAccounts];
      setTimeout(() => {
        this.showContent = true;
        loading.dismiss();
      }, 300);
    };

    this.store
      .pipe(
        select(mainSelector),
        takeUntil(this.unsubscribe$),
        map((res) => res.userAccounts)
      )
      .subscribe(
        (response: IUserAccountsResponse) => {
          if (response && response.data) {
            const userAccounts = cloneDeep(response.data);
            this.totalItems = response.count;
            handleResponse(userAccounts);
          }
        },
        () => {
          setTimeout(() => {
            this.showContent = true;
            loading.dismiss();
          }, 300);
        }
      );
  }

  private toggleUpdateStatus(user: IUserAccounts) {
    console.log('toggleUpdateStatus: ', user);
    this.selectedUser = user;
  }

  private updateAccountStatus(statusId: EUserAccountStatus) {
    const updateUserStatus = async () => {
      const loading = await this.loadingController.create();
      loading.present();

      this.updateStatusSub$ = this.userService
        .updateUserStatus({ userId: this.selectedUser.id, statusId })
        .subscribe(
          (response) => {
            this.dataLoader.getAllUsers();
            loading.dismiss();
            Swal.fire('Updated!', '', 'success');
          },
          (err) => loading.dismiss(),
          () => {
            loading.dismiss();
            this.updateStatusSub$.unsubscribe();
          }
        );
    };

    Swal.fire({
      title: 'Update account status?',
      showCancelButton: true,
      confirmButtonText: `Confirm`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        updateUserStatus();
      }
    });
  }

  private refreshUserAccounts(page: number = 1, Search?: string) {
    const params: IGenericParams = {
      PageIndex: page,
      PageSize: this.itemsPerPage,
    };

    if (Search) {
      params.Search = Search;

      if (this.agGrid && this.agGrid.api) {
        this.agGrid.api.showLoadingOverlay();
      }
    }

    this.showContent = false;
    this.dataLoader.getAllUsers(params);
  }
}
