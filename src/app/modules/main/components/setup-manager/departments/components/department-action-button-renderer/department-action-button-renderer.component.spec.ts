import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartmentActionButtonRendererComponent } from './department-action-button-renderer.component';

describe('DepartmentActionButtonRendererComponent', () => {
  let component: DepartmentActionButtonRendererComponent;
  let fixture: ComponentFixture<DepartmentActionButtonRendererComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DepartmentActionButtonRendererComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartmentActionButtonRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
