import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';

import Swal from 'sweetalert2';
import isEmpty from 'lodash-es/isEmpty';

import { IDepartment } from 'src/app/modules/main/models/department.model';
import { MainService } from 'src/app/modules/main/services/main.service';
import { FORMSTATE, MAX_ITEMS_PER_PAGE } from 'src/app/shared/constants/utils';
import { DataLoaderService } from 'src/app/shared/services/data-loader.service';
import { ToastAlertService } from 'src/app/shared/services/toast-alert.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-department-details',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './department-details.component.html',
  styleUrls: ['./department-details.component.scss'],
})
export class DepartmentDetailsComponent implements OnInit, OnDestroy {
  departmentForm: FormGroup;
  isFormValid = false;
  isSubmitting = false;

  action: string;
  departmentId: string;
  department: IDepartment;

  buttonLabel = 'Save';

  unsubscribe$ = new Subject<any>();

  constructor(
    private formBuilder: FormBuilder,
    public bsModalRef: BsModalRef,
    private mainService: MainService,
    private dataLoader: DataLoaderService,
    private toastService: ToastAlertService
  ) {}

  ngOnInit(): void {
    this.buttonLabel = this.action === FORMSTATE.UPDATE ? 'Update' : 'Save';
    this.createForm();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  get f(): any {
    return this.departmentForm.controls;
  }

  register() {
    this.isSubmitting = true;
    this.departmentForm.disable();

    if (this.departmentForm.invalid) {
      this.isSubmitting = false;
      this.departmentForm.enable();
      this.validateAllFormFields(this.departmentForm);
      return;
    }

    const payload: IDepartment = this.departmentForm.getRawValue();
    let appService: any = null;

    if (this.action === FORMSTATE.ADD) {
      appService = this.mainService.createDepartment(payload);
    } else if (this.action === FORMSTATE.UPDATE) {
      appService = this.mainService.updateDepartment(payload);
    }

    appService.toPromise().then(
      () => {
        this.isSubmitting = false;

        Swal.fire(
          this.action === FORMSTATE.ADD ? 'Created!' : 'Updated!',
          '',
          'success'
        ).then(() => this.bsModalRef.hide());

        this.dataLoader.getAllDepartments({
          PageSize: MAX_ITEMS_PER_PAGE,
        });
      },
      (error) => {
        if (error) {
          const err = error.error;
          this.departmentForm.enable();
          if (err && err.errors) {
            const errors = err.errors;
            if (errors) {
              this.toastService.showErrorToast('Warning!', errors[0]);
              return;
            }
          } else if (err && err.message) {
            this.toastService.showErrorToast('Warning!', err.message);
            return;
          }
        }
      }
    );
  }

  private createForm() {
    let formGroup: any = {
      name: ['', [Validators.required]],
      shortName: ['', [Validators.required]],
      description: [''],
    };

    if (this.action === FORMSTATE.UPDATE) {
      formGroup = {
        ...formGroup,
        id: [!isEmpty(this.departmentId) ? this.departmentId : ''],
        statusId: [''],
      };
    }
    this.departmentForm = this.formBuilder.group(formGroup);

    if (this.action === FORMSTATE.UPDATE) {
      if (!isEmpty(this.department)) {
        this.departmentForm.patchValue(this.department);
      }

      this.mainService
        .getDepartment(this.departmentId)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe((response: IDepartment) => {
          if (response) {
            this.departmentForm.patchValue(this.department);
          }
        });
    }
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
}
