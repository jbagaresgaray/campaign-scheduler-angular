import { Component, ViewEncapsulation } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { IUserAccount } from 'src/app/modules/auth/models/auth.model';
import { AuthService } from 'src/app/modules/auth/services/auth.service';
import { MENU_ITEM_NAME } from 'src/app/shared/constants/page.menu';
import {
  CURRENT_USER,
  FORMSTATE,
  PERMISSION_ACTION_DESC,
} from 'src/app/shared/constants/utils';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';

@Component({
  selector: 'app-department-action-button-renderer',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './department-action-button-renderer.component.html',
  styleUrls: ['./department-action-button-renderer.component.scss'],
})
export class DepartmentActionButtonRendererComponent
  implements ICellRendererAngularComp {
  params: any;
  user: IUserAccount;
  readonly PERMISSION_ACTION_DESC = PERMISSION_ACTION_DESC;

  constructor(
    private storage: LocalStorageService,
    private authService: AuthService
  ) {}

  agInit(params: any): void {
    this.params = params;
  }

  refresh(): boolean {
    return false;
  }

  hasFeatureAccess(action): boolean {
    this.user = this.storage.getItem(CURRENT_USER);
    return this.authService.hasPermission(
      this.user.role,
      MENU_ITEM_NAME.DEPARTMENTS.TAG,
      action
    );
  }

  updateDepartment() {
    this.params.clicked({
      action: FORMSTATE.UPDATE,
      data: this.params.data,
    });
  }

  deleteDepartment() {
    this.params.clicked({
      action: FORMSTATE.DELETE,
      data: this.params.data,
    });
  }

  viewEmployees() {
    this.params.clicked({
      action: FORMSTATE.READ,
      data: this.params.data,
    });
  }
}
