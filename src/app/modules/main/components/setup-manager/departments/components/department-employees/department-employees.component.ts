import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { IDepartment } from 'src/app/modules/main/models/department.model';
import {
  IEmployees,
  IEmployeesResponse,
} from 'src/app/modules/main/models/employee.model';
import { IGenericParams } from 'src/app/modules/main/models/generic.model';
import { UsersService } from 'src/app/modules/main/services/users.service';
import { MAX_ITEMS_PER_PAGE } from 'src/app/shared/constants/utils';

import { EmployeeDetailsComponent } from './../../../employee/components/employee-details/employee-details.component';

@Component({
  selector: 'app-department-employees',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './department-employees.component.html',
  styleUrls: ['./department-employees.component.scss'],
})
export class DepartmentEmployeesComponent implements OnInit {
  employeeArr: IEmployees[] = [];
  fakeArr: any[] = [];
  department: IDepartment;

  itemsPerPage = MAX_ITEMS_PER_PAGE;
  totalItems: number;
  currentPage = 1;

  unsubscribe = new Subject<any>();

  constructor(
    public bsModalRef: BsModalRef,
    private modalService: BsModalService,
    private userService: UsersService
  ) {
    this.fakeArr = Array.from({
      length: 10,
    });
  }

  ngOnInit(): void {
    console.log('this.department: ', this.department);
    if (this.department && this.department.id) {
      this.initEmployees(this.department.id);
    }
  }

  viewEmployee() {
    const initialState = {};
    this.modalService.show(EmployeeDetailsComponent, {
      initialState,
      class: 'modal-lg',
    });
  }

  search(event: any) {
    const val = event.target.value;
    this.initEmployees(this.department.id, null, val);
  }

  clearSearch() {
    this.initEmployees(this.department.id);
  }

  pageChanged(event: PageChangedEvent) {
    console.log('event: ', event);
    const { page } = event;
    this.initEmployees(this.department.id, String(page));
  }

  private initEmployees(departmentId, page?: string, search?: string) {
    const params: IGenericParams = {};

    if (page) params.PageIndex = Number(page);
    if (search) params.Search = search;

    this.userService
      .getAllEmployeesByDepartment(departmentId, params)
      .pipe(takeUntil(this.unsubscribe))
      .subscribe((response: IEmployeesResponse) => {
        if (response && response.data) {
          this.employeeArr = response.data;
        }
      });
  }
}
