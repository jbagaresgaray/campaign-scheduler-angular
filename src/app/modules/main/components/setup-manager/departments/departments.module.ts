import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  NbButtonModule,
  NbCardModule,
  NbDatepickerModule,
  NbFormFieldModule,
  NbIconModule,
  NbInputModule,
  NbListModule,
  NbProgressBarModule,
  NbSelectModule,
  NbSpinnerModule,
  NbTooltipModule,
  NbUserModule,
} from '@nebular/theme';
import { AgGridModule } from 'ag-grid-angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DepartmentsRoutingModule } from './departments-routing.module';
import { ComponentsModule } from 'src/app/shared/components/components.module';

import { DepartmentsComponent } from './departments.component';
import { DepartmentDetailsComponent } from './components/department-details/department-details.component';
import { DepartmentEmployeesComponent } from './components/department-employees/department-employees.component';
import { DepartmentActionButtonRendererComponent } from './components/department-action-button-renderer/department-action-button-renderer.component';


@NgModule({
  declarations: [
    DepartmentsComponent,
    DepartmentDetailsComponent,
    DepartmentEmployeesComponent,
    DepartmentActionButtonRendererComponent,
  ],
  imports: [
    CommonModule,
    DepartmentsRoutingModule,
    ComponentsModule,
    NbButtonModule,
    NbCardModule,
    NbIconModule,
    NbInputModule,
    NbTooltipModule,
    NbProgressBarModule,
    NbListModule,
    NbUserModule,
    NbDatepickerModule,
    NbSelectModule,
    NbSpinnerModule,
    NbFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    AgGridModule,
  ],
})
export class DepartmentsModule {}
