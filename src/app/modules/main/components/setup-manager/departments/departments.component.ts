import {
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { select, Store } from '@ngrx/store';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Subject, Subscription } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import Swal from 'sweetalert2';
import cloneDeep from 'lodash-es/cloneDeep';

import {
  ADMIN_DEPARTMENTS,
  FORMSTATE,
  GLOBAL_AG_GRID_OPTIONS,
  MAX_ITEMS_PER_PAGE,
} from 'src/app/shared/constants/utils';
import { DataLoaderService } from 'src/app/shared/services/data-loader.service';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import {
  IDepartment,
  IDepartmentResponse,
} from '../../../models/department.model';
import { MainState } from '../../../store/main.reducers';
import { mainSelector } from '../../../store/main.selectors';

import { DepartmentDetailsComponent } from './components/department-details/department-details.component';
import { DepartmentEmployeesComponent } from './components/department-employees/department-employees.component';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { MainService } from '../../../services/main.service';
import { IGenericParams } from '../../../models/generic.model';
import { AuthService } from 'src/app/modules/auth/services/auth.service';
import { IUserAccount } from 'src/app/modules/auth/models/auth.model';
import { GridOptions } from 'ag-grid-community';
import { DepartmentActionButtonRendererComponent } from './components/department-action-button-renderer/department-action-button-renderer.component';
import { DEPARTMENT_COLUMNS } from './department.constants';
import { AgGridAngular } from 'ag-grid-angular';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-departments',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './departments.component.html',
  styleUrls: ['./departments.component.scss'],
})
export class DepartmentsComponent implements OnInit, OnDestroy {
  user: IUserAccount;
  departmentsArr: IDepartment[] = [];
  fakeArr: any[] = [];

  itemsPerPage = MAX_ITEMS_PER_PAGE;
  totalItems: number;
  currentPage = 1;
  showContent = false;

  columnDefs: any[] = [];
  private gridApi;
  readonly DEPARTMENT_COLUMNS = DEPARTMENT_COLUMNS;
  frameworkComponents = {
    actionButtonRenderer: DepartmentActionButtonRendererComponent,
  };
  gridOptions: GridOptions = GLOBAL_AG_GRID_OPTIONS;
  @ViewChild('agGrid') agGrid: AgGridAngular;

  private unsubscribe$ = new Subject<any>();
  private deleteDepartmentSub$: Subscription = new Subscription();

  constructor(
    private store: Store<MainState>,
    private dataLoader: DataLoaderService,
    private storage: LocalStorageService,
    private modalService: BsModalService,
    private mainService: MainService,
    private loadingController: LoadingController
  ) {
    this.fakeArr = Array.from({
      length: 10,
    });

    for (const [key, value] of Object.entries(this.DEPARTMENT_COLUMNS)) {
      this.columnDefs.push({
        field: String(value.FIELD),
        headerName: String(value.LABEL),
        sortable: false,
        pinned: value.PINNED,
        headerClass: value.HEADERCLASS,
        cellStyle: {
          textAlign: value.TEXTALIGN,
        },
        editable: false,
        type: 'nonEditableColumn',
        width: value.WIDTH,
        cellRenderer: value.CELL_RENDERER,
        hide: value.HIDE,
        wrapText: value.WRAPTEXT,
        cellRendererParams: () => {
          if (value.LABEL === 'Action') {
            return {
              clicked: (field: any) => {
                const { action, data } = field;
                this.onGridActionButton(action, data);
              },
            };
          }
        },
      });
    }
  }

  ngOnInit(): void {
    this.initDepartments();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  onGridReady(params) {
    this.gridApi = params.api;
    // if (this.agGrid && this.agGrid.api) {
    //   this.agGrid.api.showLoadingOverlay();
    // }
  }

  onGridActionButton(action: string, data: any) {
    if (action === FORMSTATE.READ) {
      this.viewEmployees(data);
    } else if (action === FORMSTATE.DELETE) {
      this.deleteDepartment(data);
    } else if (action === FORMSTATE.UPDATE) {
      this.updateDepartment(data);
    }
  }

  pageChanged(event: PageChangedEvent) {
    console.log('event: ', event);
    const { page } = event;
    this.refreshDepartment(page);
  }

  toggleNewDepartment() {
    const initialState = {
      action: FORMSTATE.ADD,
    };
    this.modalService.show(DepartmentDetailsComponent, {
      initialState,
      // class: 'modal-lg',
    });
  }

  search(event: any) {
    const val = event.target.value;
    this.refreshDepartment(1, val);
  }

  clearSearch() {
    this.refreshDepartment();
  }

  private updateDepartment(department: IDepartment) {
    const initialState = {
      action: FORMSTATE.UPDATE,
      departmentId: department.id,
      department,
    };
    this.modalService.show(DepartmentDetailsComponent, {
      initialState,
      // class: 'modal-lg',
    });
  }

  private deleteDepartment(department: IDepartment) {
    Swal.fire({
      title: 'Do you want to delete this department?',
      showCancelButton: true,
      confirmButtonText: `Confirm`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        this.deleteDepartmentSub$ = this.mainService
          .deleteDepartment(department.id)
          .subscribe(() => {
            this.deleteDepartmentSub$.unsubscribe();
            this.refreshDepartment();

            Swal.fire('Deleted!', '', 'success');
          });
      }
    });
  }

  private viewEmployees(department: IDepartment) {
    const initialState = {
      department,
    };
    this.modalService.show(DepartmentEmployeesComponent, {
      initialState,
      class: 'modal-lg',
    });
  }

  private async initDepartments() {
    // const CACHE_DATA = this.storage.getItem(ADMIN_DEPARTMENTS);

    const loading = await this.loadingController.create();
    loading.present();

    const handleResponse = (departmentsArr) => {
      this.departmentsArr = [...departmentsArr];
      setTimeout(() => {
        this.showContent = true;
        loading.dismiss();
      }, 300);
    };

    this.store
      .pipe(
        select(mainSelector),
        takeUntil(this.unsubscribe$),
        map((res) => res.departments)
      )
      .subscribe(
        (response: IDepartmentResponse) => {
          if (response && response.data) {
            const userAccounts = cloneDeep(response.data);
            this.totalItems = response.count;
            handleResponse(userAccounts);
          }
        },
        () => {
          setTimeout(() => {
            this.showContent = true;
            loading.dismiss();
          }, 300);
        }
      );
  }

  private refreshDepartment(page: number = 1, Search?: string) {
    const params: IGenericParams = {
      PageIndex: page,
      PageSize: this.itemsPerPage,
    };

    if (Search) {
      params.Search = Search;
    }

    this.showContent = false;
    this.dataLoader.getAllDepartments(params);
  }
}
