import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  NbButtonModule,
  NbCardModule,
  NbDatepickerModule,
  NbIconModule,
  NbInputModule,
  NbListModule,
  NbProgressBarModule,
  NbSelectModule,
  NbSpinnerModule,
  NbTabsetModule,
  NbTooltipModule,
  NbUserModule,
} from '@nebular/theme';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgGridModule } from 'ag-grid-angular';

import { EmployeeRoutingModule } from './employee-routing.module';
import { ComponentsModule } from 'src/app/shared/components/components.module';

import { EmployeeComponent } from './employee.component';
import { EmployeeTabComponent } from './tabs/employee-tab/employee-tab.component';
import { AdminTabComponent } from './tabs/admin-tab/admin-tab.component';
import { ManagerTabComponent } from './tabs/manager-tab/manager-tab.component';
import { AllStaffsTabComponent } from './tabs/all-staffs-tab/all-staffs-tab.component';

import { EmployeeDetailsComponent } from './components/employee-details/employee-details.component';
import { EmployeeAssignDepartmentComponent } from './components/employee-assign-department/employee-assign-department.component';
import { EmployeeActionButtonRendererComponent } from './components/employee-action-button-renderer/employee-action-button-renderer.component';

@NgModule({
  declarations: [
    EmployeeComponent,
    EmployeeDetailsComponent,
    EmployeeTabComponent,
    AdminTabComponent,
    ManagerTabComponent,
    EmployeeAssignDepartmentComponent,
    AllStaffsTabComponent,
    EmployeeActionButtonRendererComponent,
  ],
  imports: [
    CommonModule,
    EmployeeRoutingModule,
    ComponentsModule,
    NbButtonModule,
    NbCardModule,
    NbIconModule,
    NbInputModule,
    NbTooltipModule,
    NbProgressBarModule,
    NbListModule,
    NbUserModule,
    NbDatepickerModule,
    NbSelectModule,
    NbTabsetModule,
    NbSpinnerModule,
    NbUserModule,
    FormsModule,
    ReactiveFormsModule,
    AgGridModule,
  ],
})
export class EmployeeModule {}
