import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmployeeComponent } from './employee.component';

const routes: Routes = [
  { path: '', component: EmployeeComponent },
  {
    path: 'campaigns',
    loadChildren: () =>
      import('./employee-campaign/employee-campaign.module').then(
        (m) => m.EmployeeCampaignModule
      ),
  },
  {
    path: 'sdr-groupings',
    loadChildren: () =>
      import('./employee-groupings/employee-groupings.module').then(
        (m) => m.EmployeeGroupingsModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmployeeRoutingModule {}
