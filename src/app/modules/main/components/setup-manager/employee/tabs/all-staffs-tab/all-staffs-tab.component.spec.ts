import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllStaffsTabComponent } from './all-staffs-tab.component';

describe('AllStaffsTabComponent', () => {
  let component: AllStaffsTabComponent;
  let fixture: ComponentFixture<AllStaffsTabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllStaffsTabComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllStaffsTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
