import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import cloneDeep from 'lodash-es/cloneDeep';
import isNull from 'lodash-es/isNull';
import { GridOptions } from 'ag-grid-community';
import { AgGridAngular } from 'ag-grid-angular';

import {
  IEmployees,
  IEmployeesResponse,
} from 'src/app/modules/main/models/employee.model';
import { IGenericParams } from 'src/app/modules/main/models/generic.model';
import { MainState } from 'src/app/modules/main/store/main.reducers';
import {
  mainSelector,
  sdrmGroupingSelector,
} from 'src/app/modules/main/store/main.selectors';
import {
  ADMIN_EMPLOYEES,
  CURRENT_USER,
  CURRENT_USER_PROFILE,
  FORMSTATE,
  GLOBAL_AG_GRID_OPTIONS,
  MAX_ITEMS_PER_PAGE,
  PERMISSION_ACTION_DESC,
  USER_ROLE,
} from 'src/app/shared/constants/utils';
import { DataLoaderService } from 'src/app/shared/services/data-loader.service';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import {
  IUserAccount,
  IUserProfile,
} from 'src/app/modules/auth/models/auth.model';
import { AuthService } from 'src/app/modules/auth/services/auth.service';
import { MENU_ITEM_NAME } from 'src/app/shared/constants/page.menu';
import { EMPLOYEES_COLUMNS } from '../../employee.constants';

import { EmployeeDetailsComponent } from '../../components/employee-details/employee-details.component';
import { EmployeeAssignDepartmentComponent } from '../../components/employee-assign-department/employee-assign-department.component';
import { EmployeeActionButtonRendererComponent } from '../../components/employee-action-button-renderer/employee-action-button-renderer.component';
import { AvatarCellRendererComponent } from 'src/app/shared/components/avatar-cell-renderer/avatar-cell-renderer.component';
import { StatusCellRendererComponent } from 'src/app/shared/components/status-cell-renderer/status-cell-renderer.component';
import { UsersService } from 'src/app/modules/main/services/users.service';
import { EmployeeGroupingsEntryComponent } from '../../employee-groupings/employee-groupings-entry/employee-groupings-entry.component';
import { RoleCellRendererComponent } from 'src/app/shared/components/role-cell-renderer/role-cell-renderer.component';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-employee-tab',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './employee-tab.component.html',
  styleUrls: ['./employee-tab.component.scss'],
})
export class EmployeeTabComponent implements OnInit {
  user: IUserAccount;
  profile: IUserProfile;
  employeeArr: IEmployees[] = [];
  fakeArr: any[] = [];

  showContent = false;
  itemsPerPage = MAX_ITEMS_PER_PAGE;
  totalItems: number;
  currentPage = 1;

  private unsubscribe$ = new Subject<any>();
  private gridApi;
  readonly EMPLOYEES_COLUMNS = EMPLOYEES_COLUMNS;
  readonly PERMISSION_ACTION_DESC = PERMISSION_ACTION_DESC;
  readonly USER_ROLE = USER_ROLE;

  columnDefs: any[] = [];
  frameworkComponents = {
    actionButtonRenderer: EmployeeActionButtonRendererComponent,
    avatarCellRenderer: AvatarCellRendererComponent,
    statusCellRenderer: StatusCellRendererComponent,
    roleStatusCellRenderer: RoleCellRendererComponent,
  };
  gridOptions: GridOptions = GLOBAL_AG_GRID_OPTIONS;
  @ViewChild('agGrid') agGrid: AgGridAngular;

  bsModalRef: BsModalRef;

  constructor(
    private store: Store<MainState>,
    private router: Router,
    private dataLoader: DataLoaderService,
    private modalService: BsModalService,
    private storage: LocalStorageService,
    private authService: AuthService,
    private usersService: UsersService,
    private loadingController: LoadingController
  ) {
    this.fakeArr = Array.from({
      length: 15,
    });

    for (const [key, value] of Object.entries(this.EMPLOYEES_COLUMNS)) {
      this.columnDefs.push({
        field: String(value.FIELD),
        headerName: String(value.LABEL),
        sortable: false,
        pinned: value.PINNED,
        headerClass: value.HEADERCLASS,
        cellStyle: {
          textAlign: value.TEXTALIGN,
        },
        editable: false,
        type: 'nonEditableColumn',
        width: value.WIDTH,
        cellRenderer: value.CELL_RENDERER,
        hide: value.HIDE,
        wrapText: value.WRAPTEXT,
        cellRendererParams: () => {
          if (value.LABEL === 'Action') {
            return {
              clicked: (field: any) => {
                const { action, data } = field;
                this.onGridActionButton(action, data);
              },
              data: {
                ...{
                  tab: 'employee',
                },
              },
            };
          }
        },
      });
    }
  }

  ngOnInit(): void {
    this.user = this.storage.getItem(CURRENT_USER);
    this.profile = this.storage.getItem(CURRENT_USER_PROFILE);

    if (this.user.role === USER_ROLE.ADMIN) {
      this.initEmployees();
    } else if (this.user.role === USER_ROLE.MANAGER) {
      this.initSDRMGroupings();
    }
  }

  onGridReady(params) {
    this.gridApi = params.api;
    // if (this.agGrid && this.agGrid.api) {
    //   this.agGrid.api.showLoadingOverlay();
    // }
  }

  onGridActionButton(action: string, data: any) {
    if (action === 'view_employee') {
      this.viewEmployee(data);
    } else if (action === 'view_campaign') {
      this.viewCampaign(data);
    } else if (action === 'tag_employee') {
      this.tagEmployeeDeparment(data);
    }
  }

  pageChanged(event: PageChangedEvent) {
    const { page } = event;
    this.refreshEmployees(page);
  }

  viewEmployee(employee: IEmployees) {
    const initialState = {
      employee,
    };
    this.modalService.show(EmployeeDetailsComponent, {
      initialState,
      class: 'modal-lg',
    });
  }

  viewAccount() {}

  viewCampaign(employee: IEmployees) {
    this.router.navigate(['/main/employee/campaigns'], {
      queryParams: {
        id: employee.id,
      },
    });
  }

  search(event: any) {
    const val = event.target.value;
    this.refreshEmployees(1, val);
  }

  clearSearch() {
    this.refreshEmployees();
  }

  tagEmployeeDeparment(employee: IEmployees) {
    const initialState = {
      employee,
      departmentId: employee.departmentId,
      formState: !!employee.departmentId ? FORMSTATE.READ : FORMSTATE.ADD,
    };
    this.modalService.show(EmployeeAssignDepartmentComponent, {
      initialState,
    });
  }

  hasFeatureAccess(action): boolean {
    this.user = this.storage.getItem(CURRENT_USER);
    return this.authService.hasPermission(
      this.user.role,
      MENU_ITEM_NAME.EMPLOYEE.TAG,
      action
    );
  }

  toggleAddSDR() {
    const initialState = {
      managerId: Number(this.profile.id),
    };
    this.bsModalRef = this.modalService.show(EmployeeGroupingsEntryComponent, {
      initialState,
      class: 'modal-lg',
    });
    this.bsModalRef.content.onRefresh.subscribe(() => {
      this.refreshSDRMGroupings();
    });
  }

  private async initEmployees() {
    const loading = await this.loadingController.create();
    loading.present();

    // const CACHE_DATA = this.storage.getItem(ADMIN_EMPLOYEES);
    const handleResponse = (employeeArr) => {
      this.employeeArr = [...employeeArr];
      setTimeout(() => {
        this.showContent = true;
        loading.dismiss();
      }, 300);
    };

    this.store
      .pipe(
        select(mainSelector),
        takeUntil(this.unsubscribe$),
        map((res) => res.employeeSDR)
      )
      .subscribe(
        (response: IEmployeesResponse) => {
          if (response && response.data) {
            const employees = cloneDeep(response.data);
            this.totalItems = response.count;
            handleResponse(employees);
          }
        },
        () => {
          setTimeout(() => {
            this.showContent = true;
            loading.dismiss();
          }, 300);
        }
      );
  }

  private initSDRMGroupings() {
    const handleResponse = (employeeArr) => {
      this.employeeArr = [...employeeArr];
      setTimeout(() => {
        this.showContent = true;
      }, 300);
    };

    this.store
      .pipe(
        select(sdrmGroupingSelector),
        takeUntil(this.unsubscribe$)
        // map((res) => res.employeeSDR)
      )
      .subscribe(
        (response: any) => {
          console.log(response);
          if (response && response.data) {
            const employees = cloneDeep(response.data);
            this.totalItems = response.count;
            handleResponse(employees);
          } else {
            this.refreshSDRMGroupings();
          }
        },
        () => {
          setTimeout(() => {
            this.showContent = true;
          }, 300);
        }
      );
  }

  private refreshEmployees(page: number = 1, Search?: string) {
    const params: IGenericParams = {
      PageIndex: page,
      PageSize: this.itemsPerPage,
    };

    if (Search) {
      params.Search = Search;
    }

    this.showContent = false;
    this.dataLoader.getAllEmployees(params);
  }

  private refreshSDRMGroupings(page: number = 1, Search?: string) {
    const params: IGenericParams = {
      PageIndex: page,
      PageSize: this.itemsPerPage,
    };

    if (Search) {
      params.Search = Search;
    }

    this.usersService
      .getAllSDRGroupings(Number(this.profile.id), params)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (response) => {
          // if (response && response.sdrs) {
          //   this.sdrArr = [...response.sdrs.data];
          // }

          // if (response && response.manager) {
          //   this.selectedSDRM = response.manager;
          //   this.pubsub.$pub('selected-sdrm', this.selectedSDRM);
          // }
          this.showContent = true;
        },
        () => (this.showContent = true),
        () => (this.showContent = true)
      );
  }
}
