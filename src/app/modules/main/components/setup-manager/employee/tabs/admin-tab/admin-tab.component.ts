import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { BsModalService } from 'ngx-bootstrap/modal';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

import cloneDeep from 'lodash-es/cloneDeep';
import { GridOptions } from 'ag-grid-community';
import { AgGridAngular } from 'ag-grid-angular';

import {
  IEmployees,
  IEmployeesResponse,
} from 'src/app/modules/main/models/employee.model';
import { IGenericParams } from 'src/app/modules/main/models/generic.model';
import { MainState } from 'src/app/modules/main/store/main.reducers';
import { mainSelector } from 'src/app/modules/main/store/main.selectors';
import {
  ADMIN_ADMINS,
  CURRENT_USER,
  GLOBAL_AG_GRID_OPTIONS,
  MAX_ITEMS_PER_PAGE,
  PERMISSION_ACTION_DESC,
} from 'src/app/shared/constants/utils';
import { DataLoaderService } from 'src/app/shared/services/data-loader.service';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { EmployeeDetailsComponent } from '../../components/employee-details/employee-details.component';
import { MENU_ITEM_NAME } from 'src/app/shared/constants/page.menu';
import { AuthService } from 'src/app/modules/auth/services/auth.service';
import { IUserAccount } from 'src/app/modules/auth/models/auth.model';
import { EMPLOYEES_COLUMNS } from '../../employee.constants';
import { EmployeeActionButtonRendererComponent } from '../../components/employee-action-button-renderer/employee-action-button-renderer.component';
import { AvatarCellRendererComponent } from 'src/app/shared/components/avatar-cell-renderer/avatar-cell-renderer.component';
import { StatusCellRendererComponent } from 'src/app/shared/components/status-cell-renderer/status-cell-renderer.component';
import { RoleCellRendererComponent } from 'src/app/shared/components/role-cell-renderer/role-cell-renderer.component';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-admin-tab',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './admin-tab.component.html',
  styleUrls: ['./admin-tab.component.scss'],
})
export class AdminTabComponent implements OnInit {
  user: IUserAccount;
  employeeArr: IEmployees[] = [];
  fakeArr: any[] = [];

  showContent = false;
  itemsPerPage = MAX_ITEMS_PER_PAGE;
  totalItems: number;
  currentPage = 1;

  private unsubscribe$ = new Subject<any>();
  private gridApi;
  readonly EMPLOYEES_COLUMNS = EMPLOYEES_COLUMNS;
  readonly PERMISSION_ACTION_DESC = PERMISSION_ACTION_DESC;

  columnDefs: any[] = [];
  frameworkComponents = {
    actionButtonRenderer: EmployeeActionButtonRendererComponent,
    avatarCellRenderer: AvatarCellRendererComponent,
    statusCellRenderer: StatusCellRendererComponent,
    roleStatusCellRenderer: RoleCellRendererComponent,
  };
  gridOptions: GridOptions = GLOBAL_AG_GRID_OPTIONS;
  @ViewChild('agGrid') agGrid: AgGridAngular;

  constructor(
    private store: Store<MainState>,
    private dataLoader: DataLoaderService,
    private modalService: BsModalService,
    private storage: LocalStorageService,
    private authService: AuthService,
    private loadingController: LoadingController
  ) {
    this.fakeArr = Array.from({
      length: 15,
    });

    for (const [key, value] of Object.entries(this.EMPLOYEES_COLUMNS)) {
      this.columnDefs.push({
        field: String(value.FIELD),
        headerName: String(value.LABEL),
        sortable: false,
        pinned: value.PINNED,
        headerClass: value.HEADERCLASS,
        cellStyle: {
          textAlign: value.TEXTALIGN,
        },
        editable: false,
        type: 'nonEditableColumn',
        width: value.WIDTH,
        cellRenderer: value.CELL_RENDERER,
        hide: value.HIDE,
        wrapText: value.WRAPTEXT,
        cellRendererParams: () => {
          if (value.LABEL === 'Action') {
            return {
              clicked: (field: any) => {
                const { action, data } = field;
                this.onGridActionButton(action, data);
              },
              data: {
                ...{
                  tab: 'admin',
                },
              },
            };
          }
        },
      });
    }
  }

  ngOnInit(): void {
    this.initEmployees();
  }

  onGridReady(params) {
    this.gridApi = params.api;
    // if (this.agGrid && this.agGrid.api) {
    //   this.agGrid.api.showLoadingOverlay();
    // }
  }

  onGridActionButton(action: string, data: any) {
    if (action === 'view_employee') {
      this.viewEmployee(data);
    }
  }

  pageChanged(event: PageChangedEvent) {
    console.log('event: ', event);
    const { page } = event;
    this.refreshEmployees(page);
  }

  viewEmployee(employee: IEmployees) {
    const initialState = {
      employee,
    };
    this.modalService.show(EmployeeDetailsComponent, {
      initialState,
      class: 'modal-lg',
    });
  }

  search(event: any) {
    const val = event.target.value;
    this.refreshEmployees(1, val);
  }

  clearSearch() {
    this.refreshEmployees();
  }

  hasFeatureAccess(action): boolean {
    this.user = this.storage.getItem(CURRENT_USER);
    if (this.user) {
      return this.authService.hasPermission(
        this.user.role,
        MENU_ITEM_NAME.EMPLOYEE.TAG,
        action
      );
    } else {
      return false;
    }
  }

  private async initEmployees() {
    // const CACHE_DATA = this.storage.getItem(ADMIN_ADMINS);
    const loading = await this.loadingController.create();
    loading.present();

    const handleResponse = (employeeArr) => {
      this.employeeArr = [...employeeArr];
      setTimeout(() => {
        this.showContent = true;
        loading.dismiss();
      }, 300);
    };

    this.store
      .pipe(
        select(mainSelector),
        takeUntil(this.unsubscribe$),
        map((res) => res.employeeAdmin)
      )
      .subscribe(
        (response: IEmployeesResponse) => {
          if (response && response.data) {
            const employees = cloneDeep(response.data);
            this.totalItems = response.count;
            handleResponse(employees);
          }
        },
        () => {
          setTimeout(() => {
            this.showContent = true;
            loading.dismiss();
          }, 300);
        }
      );
  }

  private refreshEmployees(page: number = 1, Search?: string) {
    const params: IGenericParams = {
      PageIndex: page,
      PageSize: this.itemsPerPage,
    };

    if (Search) {
      params.Search = Search;
    }

    this.showContent = false;
    this.dataLoader.getAllEmployees(params);
  }
}
