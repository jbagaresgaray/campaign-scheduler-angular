import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { AgGridAngular } from 'ag-grid-angular';
import { GridOptions } from 'ag-grid-community';
import cloneDeep from 'lodash-es/cloneDeep';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import {
  IEmployees,
  IEmployeesResponse,
} from 'src/app/modules/main/models/employee.model';
import {
  ADMIN_STAFFS,
  CURRENT_USER,
  FORMSTATE,
  GLOBAL_AG_GRID_OPTIONS,
  MAX_ITEMS_PER_PAGE,
  PERMISSION_ACTION_DESC,
} from 'src/app/shared/constants/utils';
import { MainState } from 'src/app/modules/main/store/main.reducers';
import { DataLoaderService } from 'src/app/shared/services/data-loader.service';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { IGenericParams } from 'src/app/modules/main/models/generic.model';
import { mainSelector } from 'src/app/modules/main/store/main.selectors';
import { IUserAccount } from 'src/app/modules/auth/models/auth.model';
import { EMPLOYEES_COLUMNS } from '../../employee.constants';

import { EmployeeDetailsComponent } from '../../components/employee-details/employee-details.component';
import { EmployeeActionButtonRendererComponent } from '../../components/employee-action-button-renderer/employee-action-button-renderer.component';
import { AvatarCellRendererComponent } from 'src/app/shared/components/avatar-cell-renderer/avatar-cell-renderer.component';
import { StatusCellRendererComponent } from 'src/app/shared/components/status-cell-renderer/status-cell-renderer.component';

import { EmployeeAssignDepartmentComponent } from '../../components/employee-assign-department/employee-assign-department.component';
import { RoleCellRendererComponent } from 'src/app/shared/components/role-cell-renderer/role-cell-renderer.component';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-all-staffs-tab',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './all-staffs-tab.component.html',
  styleUrls: ['./all-staffs-tab.component.scss'],
})
export class AllStaffsTabComponent implements OnInit {
  user: IUserAccount;
  employeeArr: IEmployees[] = [];
  fakeArr: any[] = [];

  showContent = false;
  itemsPerPage = MAX_ITEMS_PER_PAGE;
  totalItems: number;
  currentPage = 1;

  private unsubscribe$ = new Subject<any>();
  private gridApi;
  readonly EMPLOYEES_COLUMNS = EMPLOYEES_COLUMNS;
  readonly PERMISSION_ACTION_DESC = PERMISSION_ACTION_DESC;

  columnDefs: any[] = [];
  frameworkComponents = {
    actionButtonRenderer: EmployeeActionButtonRendererComponent,
    avatarCellRenderer: AvatarCellRendererComponent,
    statusCellRenderer: StatusCellRendererComponent,
    roleStatusCellRenderer: RoleCellRendererComponent,
  };
  gridOptions: GridOptions = GLOBAL_AG_GRID_OPTIONS;
  @ViewChild('agGrid') agGrid: AgGridAngular;

  constructor(
    private router: Router,
    private store: Store<MainState>,
    private dataLoader: DataLoaderService,
    private storage: LocalStorageService,
    private modalService: BsModalService,
    private loadingController: LoadingController
  ) {
    this.fakeArr = Array.from({
      length: 15,
    });

    for (const [key, value] of Object.entries(this.EMPLOYEES_COLUMNS)) {
      this.columnDefs.push({
        field: String(value.FIELD),
        headerName: String(value.LABEL),
        sortable: false,
        pinned: value.PINNED,
        headerClass: value.HEADERCLASS,
        cellStyle: {
          textAlign: value.TEXTALIGN,
        },
        editable: false,
        type: 'nonEditableColumn',
        width: value.WIDTH,
        cellRenderer: value.CELL_RENDERER,
        hide: value.HIDE,
        wrapText: value.WRAPTEXT,
        cellRendererParams: () => {
          if (value.LABEL === 'Action') {
            return {
              clicked: (field: any) => {
                const { action, data } = field;
                this.onGridActionButton(action, data);
              },
              data: {
                ...{
                  tab: 'staffs',
                },
              },
            };
          }
        },
      });
    }
  }

  ngOnInit(): void {
    this.initEmployees();
  }

  onGridReady(params) {
    this.gridApi = params.api;
    // if (this.agGrid && this.agGrid.api) {
    //   this.agGrid.api.showLoadingOverlay();
    // }
  }

  onGridActionButton(action: string, data: any) {
    if (action === 'view_employee') {
      this.viewEmployee(data);
    } else if (action === 'view_campaign') {
      this.viewCampaign(data);
    } else if (action === 'tag_employee') {
      this.tagEmployeeDeparment(data);
    } else if (action === 'view_groupings') {
      this.viewGroupings(data);
    }
  }

  pageChanged(event: PageChangedEvent) {
    console.log('event: ', event);
    const { page } = event;
    this.refreshEmployees(page);
  }

  tagEmployeeDeparment(employee: IEmployees) {
    const initialState = {
      employee,
      departmentId: employee.departmentId,
      formState: !!employee.departmentId ? FORMSTATE.READ : FORMSTATE.ADD,
    };
    this.modalService.show(EmployeeAssignDepartmentComponent, {
      initialState,
    });
  }

  viewGroupings(employee: IEmployees) {
    this.router.navigate(['/main/employee/sdr-groupings'], {
      queryParams: {
        managerId: employee.id,
      },
    });
  }

  viewCampaign(employee: IEmployees) {
    this.router.navigate(['/main/employee/campaigns'], {
      queryParams: {
        id: employee.id,
      },
    });
  }

  viewEmployee(employee: IEmployees) {
    const initialState = {
      employee,
    };
    this.modalService.show(EmployeeDetailsComponent, {
      initialState,
      class: 'modal-lg',
    });
  }

  search(event: any) {
    const val = event.target.value;
    this.refreshEmployees(1, val);
  }

  clearSearch() {
    this.refreshEmployees();
  }

  private async initEmployees() {
    // const CACHE_DATA = this.storage.getItem(ADMIN_STAFFS);
    const loading = await this.loadingController.create();
    loading.present();

    const handleResponse = (employeeArr) => {
      this.employeeArr = [...employeeArr];
      setTimeout(() => {
        this.showContent = true;
        loading.dismiss();
      }, 300);
    };

    this.store
      .pipe(
        select(mainSelector),
        takeUntil(this.unsubscribe$),
        map((res) => res.employeeAllStaffs)
      )
      .subscribe(
        (response: IEmployeesResponse) => {
          if (response && response.data) {
            const employees = cloneDeep(response.data);
            this.totalItems = response.count;
            handleResponse(employees);
          }
        },
        () => {
          setTimeout(() => {
            this.showContent = true;
            loading.dismiss();
          }, 300);
        }
      );
  }

  private refreshEmployees(page: number = 1, Search?: string) {
    const params: IGenericParams = {
      PageIndex: page,
      PageSize: this.itemsPerPage,
    };

    if (Search) {
      params.Search = Search;
    }

    this.showContent = false;
    this.dataLoader.getAllStaffs(params);
  }
}
