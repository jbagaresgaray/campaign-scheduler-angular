import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmployeeCampaignComponent } from './employee-campaign.component';

const routes: Routes = [{ path: '', component: EmployeeCampaignComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmployeeCampaignRoutingModule {}
