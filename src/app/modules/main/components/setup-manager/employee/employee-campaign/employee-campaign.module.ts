import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  NbButtonModule,
  NbCardModule,
  NbIconModule,
  NbInputModule,
  NbListModule,
  NbSpinnerModule,
  NbTooltipModule,
  NbWindowModule,
} from '@nebular/theme';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { EmployeeCampaignRoutingModule } from './employee-campaign-routing.module';
import { ComponentsModule } from 'src/app/shared/components/components.module';

import { EmployeeCampaignComponent } from './employee-campaign.component';

@NgModule({
  declarations: [EmployeeCampaignComponent],
  imports: [
    CommonModule,
    EmployeeCampaignRoutingModule,
    ComponentsModule,
    NbButtonModule,
    NbCardModule,
    NbIconModule,
    NbInputModule,
    NbTooltipModule,
    NbListModule,
    NbSpinnerModule,
    NbWindowModule,
    PaginationModule,
    FormsModule,
    IonicModule,
  ],
})
export class EmployeeCampaignModule {}
