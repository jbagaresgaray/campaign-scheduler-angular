import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { NbWindowService } from '@nebular/theme';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ICampaigns } from 'src/app/modules/main/models/campaign.model';
import { IEmployee } from 'src/app/modules/main/models/employee.model';
import { MainService } from 'src/app/modules/main/services/main.service';
import { UsersService } from 'src/app/modules/main/services/users.service';
import { ECampaignStatus } from 'src/app/shared/constants/enums';
import { CAMPAIGN_STATUS_ITEM } from 'src/app/shared/constants/utils';
import { CampaignSchedulerComponent } from '../../../campaign/components/campaign-scheduler/campaign-scheduler.component';

@Component({
  selector: 'app-employee-campaign',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './employee-campaign.component.html',
  styleUrls: ['./employee-campaign.component.scss'],
})
export class EmployeeCampaignComponent implements OnInit {
  campaignArr: ICampaigns[] = [];
  employee: IEmployee;
  fakeArr: any[] = [];
  title: string;
  employeeId: number;

  showContent: boolean;

  private unsubscribe = new Subject<any>();

  constructor(
    private route: ActivatedRoute,
    private mainService: MainService,
    private usersService: UsersService,
    private loadingController: LoadingController,
    private windowService: NbWindowService
  ) {
    this.fakeArr = Array.from({
      length: 20,
    });

    this.route.queryParams.subscribe((params) => {
      if (params && params.id) {
        this.employeeId = params.id;
        this.getEmployeeCampaigns();
        this.getEmployeeDetails();
      }
    });
  }

  ngOnInit(): void {}

  viewCampaignScheduler(campaign: ICampaigns) {
    this.windowService
      .open(CampaignSchedulerComponent, {
        title: `${this.employee.firstName} ${this.employee.lastName}`,
        context: {
          employee: this.employee,
          campaign,
          campaignId: campaign.id,
        },
        windowClass: 'window-fullscreen',
      })
      .fullScreen();
  }

  getStatus(statusId: ECampaignStatus) {
    switch (statusId) {
      case ECampaignStatus.ACTIVE:
        return {
          class: 'success',
          label: CAMPAIGN_STATUS_ITEM.ACTIVE,
        };
      case ECampaignStatus.INACTIVE:
        return {
          class: 'warning',
          label: CAMPAIGN_STATUS_ITEM.INACTIVE,
        };
      case ECampaignStatus.TERMINATED:
        return {
          class: 'danger',
          label: CAMPAIGN_STATUS_ITEM.TERMINATED,
        };
    }
  }

  private getEmployeeCampaigns() {
    this.showContent = false;
    this.mainService
      .getAllSDRCampaign(this.employeeId)
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(
        (response) => {
          console.log('getAllSDRCampaign: ', response);
          this.campaignArr = [...response];
        },
        () => (this.showContent = false),
        () => (this.showContent = false)
      );
  }

  private async getEmployeeDetails() {
    const loading = await this.loadingController.create();
    loading.present();
    this.usersService
      .getStaffDetails(this.employeeId)
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(
        (response: IEmployee) => {
          console.log('getStaffDetails: ', response);
          if (response) {
            this.employee = response;
          }
        },
        () => loading.dismiss(),
        () => loading.dismiss()
      );
  }
}
