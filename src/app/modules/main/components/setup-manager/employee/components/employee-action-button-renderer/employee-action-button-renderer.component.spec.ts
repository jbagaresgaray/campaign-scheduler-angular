import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeActionButtonRendererComponent } from './employee-action-button-renderer.component';

describe('EmployeeActionButtonRendererComponent', () => {
  let component: EmployeeActionButtonRendererComponent;
  let fixture: ComponentFixture<EmployeeActionButtonRendererComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmployeeActionButtonRendererComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeActionButtonRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
