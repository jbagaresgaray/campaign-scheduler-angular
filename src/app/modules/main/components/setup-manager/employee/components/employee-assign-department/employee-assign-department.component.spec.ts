import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeAssignDepartmentComponent } from './employee-assign-department.component';

describe('EmployeeAssignDepartmentComponent', () => {
  let component: EmployeeAssignDepartmentComponent;
  let fixture: ComponentFixture<EmployeeAssignDepartmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmployeeAssignDepartmentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeAssignDepartmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
