import { Component, ViewEncapsulation } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { IUserAccount } from 'src/app/modules/auth/models/auth.model';
import { AuthService } from 'src/app/modules/auth/services/auth.service';
import { MENU_ITEM_NAME } from 'src/app/shared/constants/page.menu';
import {
  CURRENT_USER,
  PERMISSION_ACTION_DESC,
  USER_ACCOUNT_STATUS,
  USER_ROLE,
} from 'src/app/shared/constants/utils';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';

@Component({
  selector: 'app-employee-action-button-renderer',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './employee-action-button-renderer.component.html',
  styleUrls: ['./employee-action-button-renderer.component.scss'],
})
export class EmployeeActionButtonRendererComponent
  implements ICellRendererAngularComp {
  params: any;
  user: IUserAccount;

  VIEW_EMPLOYEE = 'View';
  VIEW_CAMPAIGN = 'View Campaign';
  VIEW_GROUPINGS = 'View Groupings';
  ASSIGN_DEPARTMENTS = 'Assign Department';
  VIEW_DEPARTMENTS = 'View Department';

  departmentId: number;
  roleId: number;

  ACTIVE_TAB: string;

  readonly PERMISSION_ACTION_DESC = PERMISSION_ACTION_DESC;
  readonly USER_ACCOUNT_STATUS = USER_ACCOUNT_STATUS;
  readonly USER_ROLE = USER_ROLE;

  constructor(
    private storage: LocalStorageService,
    private authService: AuthService
  ) {}

  hasFeatureAccess(action): boolean {
    this.user = this.storage.getItem(CURRENT_USER);
    return this.authService.hasPermission(
      this.user.role,
      MENU_ITEM_NAME.EMPLOYEE.TAG,
      action
    );
  }

  agInit(params: any): void {
    this.params = params;
    if (params.data && params.data.tab) {
      this.ACTIVE_TAB = params.data.tab;
    }

    if (params.data && params.data.departmentId) {
      this.departmentId = params.data.departmentId;
    }

    if (params.data && params.data.roleId) {
      this.roleId = params.data.roleId;
    }
  }

  refresh(): boolean {
    return false;
  }

  viewEmployee() {
    this.params.clicked({
      action: 'view_employee',
      data: this.params.data,
    });
  }

  viewCampaign() {
    this.params.clicked({
      action: 'view_campaign',
      data: this.params.data,
    });
  }

  viewGroupings() {
    this.params.clicked({
      action: 'view_groupings',
      data: this.params.data,
    });
  }

  tagEmployeeDeparment() {
    this.params.clicked({
      action: 'tag_employee',
      data: this.params.data,
    });
  }
}
