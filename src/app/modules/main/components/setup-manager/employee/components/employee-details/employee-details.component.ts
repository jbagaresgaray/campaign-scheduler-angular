import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { LoadingController } from '@ionic/angular';
import { select, Store } from '@ngrx/store';

import {
  IEmployee,
  IEmployees,
} from 'src/app/modules/main/models/employee.model';
import { UsersService } from 'src/app/modules/main/services/users.service';
import { COUNTRIES } from 'src/app/shared/constants/countries';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { ADMIN_DEPARTMENTS, SDR_TYPE } from 'src/app/shared/constants/utils';
import {
  IDepartment,
  IDepartmentResponse,
} from 'src/app/modules/main/models/department.model';
import { mainSelector } from 'src/app/modules/main/store/main.selectors';
import { MainState } from 'src/app/modules/main/store/main.reducers';

@Component({
  selector: 'app-employee-details',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.scss'],
})
export class EmployeeDetailsComponent implements OnInit, OnDestroy {
  employee: IEmployees;
  departmentsArr: IDepartment[] = [];
  employeeForm: FormGroup;

  sdrTypeArr: { id: number; name: string }[] = [];

  private unsubscribe$ = new Subject<any>();
  readonly COUNTRIES = COUNTRIES;

  constructor(
    private store: Store<MainState>,
    public bsModalRef: BsModalRef,
    private formBuilder: FormBuilder,
    private usersService: UsersService,
    private loadingController: LoadingController,
    private storage: LocalStorageService
  ) {
    this.sdrTypeArr = [...SDR_TYPE];
  }

  ngOnInit(): void {
    console.log('employee: ', this.employee);
    this.createForm();
    this.getEmployeeDetails();
    this.initDepartments();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  get f(): any {
    return this.employeeForm.controls;
  }

  private createForm() {
    this.employeeForm = this.formBuilder.group({
      id: [''],
      firstName: [''],
      lastName: [''],
      middleName: [''],
      email: [''],
      linkedInName: [''],
      linkedInUrl: [''],
      dateHired: [''],
      departmentId: [''],
      address: [''],
      state: [''],
      postCode: [''],
      country: [''],
      sdrType: [''],
    });
    this.employeeForm.reset();

    if (this.employee) {
      this.employeeForm.patchValue(this.employee);
    }
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  private async getEmployeeDetails() {
    const loading = await this.loadingController.create();
    loading.present();
    this.usersService
      .getStaffDetails(Number(this.employee.id))
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (response: IEmployee) => {
          console.log('getStaffDetails: ', response);
          if (response) {
            this.employee = response;
          }
        },
        () => loading.dismiss(),
        () => loading.dismiss()
      );
  }

  private initDepartments() {
    const CACHE_DATA = this.storage.getItem(ADMIN_DEPARTMENTS);
    this.store
      .pipe(
        select(mainSelector),
        takeUntil(this.unsubscribe$),
        map((res) => res.departments)
      )
      .subscribe((response: IDepartmentResponse) => {
        if (response && response.data) {
          this.departmentsArr = [...response.data];
        } else {
          const data = CACHE_DATA.data;
          this.departmentsArr = [...data];
        }
      });
  }
}
