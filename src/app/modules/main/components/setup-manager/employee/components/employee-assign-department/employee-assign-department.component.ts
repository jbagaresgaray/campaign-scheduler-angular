import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { map, takeUntil } from 'rxjs/operators';

import cloneDeep from 'lodash-es/cloneDeep';

import {
  IDepartment,
  IDepartmentResponse,
} from 'src/app/modules/main/models/department.model';
import { IEmployees } from 'src/app/modules/main/models/employee.model';
import { MainState } from 'src/app/modules/main/store/main.reducers';
import { mainSelector } from 'src/app/modules/main/store/main.selectors';
import { ADMIN_DEPARTMENTS, FORMSTATE } from 'src/app/shared/constants/utils';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';

import Swal from 'sweetalert2';
import { Subject, Subscription } from 'rxjs';
import { UsersService } from 'src/app/modules/main/services/users.service';

@Component({
  selector: 'app-employee-assign-department',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './employee-assign-department.component.html',
  styleUrls: ['./employee-assign-department.component.scss'],
})
export class EmployeeAssignDepartmentComponent implements OnInit, OnDestroy {
  departmentsArr: IDepartment[] = [];
  formState: string;
  headerTitle: string;
  fakeArr: any[] = [];
  departmentId: number;
  employee: IEmployees;
  unsubscribe$ = new Subject<any>();
  private subscription: Subscription;
  readonly FORMSTATE = FORMSTATE;

  isSubmitting = false;

  constructor(
    private store: Store<MainState>,
    public bsModalRef: BsModalRef,
    private storage: LocalStorageService,
    private userService: UsersService
  ) {
    this.fakeArr = Array.from({
      length: 30,
    });
  }

  ngOnInit(): void {
    this.headerTitle =
      this.formState === FORMSTATE.ADD
        ? `Assign ${this.employee.firstName} ${this.employee.lastName} to a Department`
        : `View ${this.employee.firstName} ${this.employee.lastName} assigned Department`;
    this.initDepartments();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  assign() {
    this.isSubmitting = true;
    this.subscription = this.userService
      .assignEmployeeToDepartment({
        userProfileId: this.employee.id,
        departmentId: this.departmentId,
      })
      .subscribe(
        (response) => {
          if (response && response.statusCode === 200) {
            this.isSubmitting = false;
            Swal.fire('Saved!', '', 'success').then(() =>
              this.bsModalRef.hide()
            );
          }
        },
        () => {
          this.isSubmitting = false;
        },
        () => this.subscription.unsubscribe()
      );
  }

  private initDepartments() {
    const CACHE_DATA = this.storage.getItem(ADMIN_DEPARTMENTS);
    const handleResponse = (departmentsArr) => {
      this.departmentsArr = [...departmentsArr];
    };

    this.store
      .pipe(
        select(mainSelector),
        takeUntil(this.unsubscribe$),
        map((res) => res.departments)
      )
      .subscribe((response: IDepartmentResponse) => {
        if (response && response.data) {
          const userAccounts = cloneDeep(response.data);
          handleResponse(userAccounts);
        } else {
          const data = CACHE_DATA.data;
          handleResponse(data);
        }
      });
  }
}
