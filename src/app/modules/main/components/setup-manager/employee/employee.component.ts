import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IUserAccount } from 'src/app/modules/auth/models/auth.model';
import { CURRENT_USER, USER_ROLE } from 'src/app/shared/constants/utils';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss'],
})
export class EmployeeComponent implements OnInit {
  user: IUserAccount;
  readonly USER_ROLE = USER_ROLE;

  tabAll = 'All';
  tabSDR = 'SDRs';
  tabSDRM = 'Managers';
  tabAdmins = 'Admins';

  activeTab: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private storage: LocalStorageService
  ) {}

  ngOnInit(): void {
    this.user = this.storage.getItem(CURRENT_USER);

    this.activatedRoute.queryParams.subscribe((params) => {
      this.activeTab = params['action'];
    });
  }

  onChangeTab(event: any) {
    this.activeTab = event.tabId;
    this.router.navigate([], {
      relativeTo: this.activatedRoute,
      queryParams: {
        action: this.activeTab,
      },
      queryParamsHandling: 'merge', // remove to replace all query params by provided
    });
  }
}
