import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeGroupingsComponent } from './employee-groupings.component';

describe('EmployeeGroupingsComponent', () => {
  let component: EmployeeGroupingsComponent;
  let fixture: ComponentFixture<EmployeeGroupingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmployeeGroupingsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeGroupingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
