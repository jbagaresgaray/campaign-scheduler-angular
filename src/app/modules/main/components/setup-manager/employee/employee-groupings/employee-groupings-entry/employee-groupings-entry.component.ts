import {
  Component,
  EventEmitter,
  NgZone,
  OnDestroy,
  OnInit,
  ViewEncapsulation,
} from '@angular/core';
import { select, Store } from '@ngrx/store';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Subject, Subscription } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import Swal from 'sweetalert2';
import cloneDeep from 'lodash-es/cloneDeep';

import {
  IEmployees,
  IEmployeesResponse,
} from 'src/app/modules/main/models/employee.model';
import { MainState } from 'src/app/modules/main/store/main.reducers';
import { mainSelector } from 'src/app/modules/main/store/main.selectors';
import { ADMIN_EMPLOYEES } from 'src/app/shared/constants/utils';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { IGenericParams } from 'src/app/modules/main/models/generic.model';
import { DataLoaderService } from 'src/app/shared/services/data-loader.service';
import { ToastAlertService } from 'src/app/shared/services/toast-alert.service';
import { LoadingController } from '@ionic/angular';
import { UsersService } from 'src/app/modules/main/services/users.service';

@Component({
  selector: 'app-employee-groupings-entry',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './employee-groupings-entry.component.html',
  styleUrls: ['./employee-groupings-entry.component.scss'],
})
export class EmployeeGroupingsEntryComponent implements OnInit, OnDestroy {
  fakeArr: any[] = [];
  employeesArr: IEmployees[] = [];
  showContent = false;
  unsubscribe$ = new Subject<any>();

  managerId: number;

  onRefresh: EventEmitter<any> = new EventEmitter();

  isSubmitting: boolean;

  private assignManagerSub: Subscription = new Subscription();

  constructor(
    private store: Store<MainState>,
    public bsModalRef: BsModalRef,
    private storage: LocalStorageService,
    private dataLoader: DataLoaderService,
    private userService: UsersService,
    private toastService: ToastAlertService,
    private loadingController: LoadingController
  ) {
    this.fakeArr = Array.from({
      length: 20,
    });
    this.isSubmitting = false;
  }

  ngOnInit(): void {
    this.initEmployees();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  addSDR(employee: IEmployees) {
    const addSDR = async () => {
      this.isSubmitting = true;
      const loading = await this.loadingController.create();
      loading.present();

      this.assignManagerSub = this.userService
        .createSDRGroupings({
          managerEmployeeId: this.managerId,
          sdrEmployeeId: Number(employee.id),
        })
        .subscribe(
          (response) => {
            if (response && response.statusCode === 200) {
              this.assignManagerSub.unsubscribe();
              this.onRefresh.emit();

              this.isSubmitting = false;
              loading.dismiss();
              Swal.fire('Added!', '', 'success').then(() => {
                this.bsModalRef.hide();
              });
            }
          },
          (err) => {
            console.log('err: ', err);
            this.isSubmitting = false;
            loading.dismiss();
            if (err) {
              const error = err.error;

              if (error && error.message) {
                return this.toastService.showErrorToast(
                  'Warning!',
                  err.message
                );
              }
            }
          }
        );
    };

    Swal.fire({
      title: 'Do you want to add this SDR?',
      showCancelButton: true,
      allowOutsideClick: false,
      confirmButtonText: `Confirm`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        addSDR();
      }
    });
  }

  search(event: any) {
    const val = event.target.value;
    this.refreshEmployees(val);
  }

  clearSearch() {
    this.refreshEmployees();
  }

  private refreshEmployees(Search?: string) {
    const params: IGenericParams = {
      Search,
    };

    this.dataLoader.getAllEmployees(params);
  }

  private initEmployees() {
    const CACHE_DATA = this.storage.getItem(ADMIN_EMPLOYEES);
    const handleResponse = (employeesArr) => {
      this.employeesArr = [...employeesArr];
      console.log('this.employeesArr: ', this.employeesArr);
    };

    this.store
      .pipe(
        select(mainSelector),
        takeUntil(this.unsubscribe$),
        map((res) => res.employeeSDR)
      )
      .subscribe((response: IEmployeesResponse) => {
        if (response && response.data) {
          const employees = cloneDeep(response.data);
          handleResponse(employees);
        } else {
          const data = CACHE_DATA.data;
          handleResponse(data);
        }
      });
  }
}
