import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeGroupingsEntryComponent } from './employee-groupings-entry.component';

describe('EmployeeGroupingsEntryComponent', () => {
  let component: EmployeeGroupingsEntryComponent;
  let fixture: ComponentFixture<EmployeeGroupingsEntryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmployeeGroupingsEntryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeGroupingsEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
