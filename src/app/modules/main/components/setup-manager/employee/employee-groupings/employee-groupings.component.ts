import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import Swal from 'sweetalert2';

import { IUserAccount } from 'src/app/modules/auth/models/auth.model';
import { IEmployees } from 'src/app/modules/main/models/employee.model';
import {
  CAMPAIGN_SDR_STATUS,
  CAMPAIGN_SDR_STATUS_ENUM,
  CURRENT_USER,
  MAX_ITEMS_PER_PAGE,
  PERMISSION_ACTION_DESC,
  USER_ROLE,
} from 'src/app/shared/constants/utils';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { AuthService } from 'src/app/modules/auth/services/auth.service';
import { MENU_ITEM_NAME } from 'src/app/shared/constants/page.menu';
import { ActivatedRoute } from '@angular/router';
import { IGenericParams } from 'src/app/modules/main/models/generic.model';
import { UsersService } from 'src/app/modules/main/services/users.service';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { takeUntil } from 'rxjs/operators';
import { NbSidebarService } from '@nebular/theme';
import { PubsubService } from 'src/app/shared/services/pubsub.service';
import { EmployeeGroupingsEntryComponent } from './employee-groupings-entry/employee-groupings-entry.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-employee-groupings',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './employee-groupings.component.html',
  styleUrls: ['./employee-groupings.component.scss'],
})
export class EmployeeGroupingsComponent implements OnInit, OnDestroy {
  sdrArr: IEmployees[] = [];
  selectedSDRM: IEmployees;
  user: IUserAccount;
  fakeArr: any[] = [];

  bsModalRef: BsModalRef;

  managerId: number;
  showContent = false;
  isRemoving = false;

  itemsPerPage = MAX_ITEMS_PER_PAGE;
  totalItems: number;
  currentPage = 1;

  unsubscribe$ = new Subject<any>();
  readonly CAMPAIGN_SDR_STATUS_ENUM = CAMPAIGN_SDR_STATUS_ENUM;
  readonly PERMISSION_ACTION_DESC = PERMISSION_ACTION_DESC;
  readonly USER_ROLE = USER_ROLE;

  constructor(
    private route: ActivatedRoute,
    private storage: LocalStorageService,
    private authService: AuthService,
    private usersService: UsersService,
    private sidebarService: NbSidebarService,
    private pubsub: PubsubService,
    private modalService: BsModalService
  ) {
    this.fakeArr = Array.from({
      length: 15,
    });
    this.route.queryParams.subscribe((params) => {
      if (params && params.managerId) {
        this.managerId = params.managerId;

        this.refreshEmployees();
      }
    });
  }

  ngOnInit(): void {
    this.user = this.storage.getItem(CURRENT_USER);
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  toggleSidebar() {
    this.pubsub.$pub('settings-sidebar', 'manager');
    this.sidebarService.toggle(false, 'settings-sidebar');
    return false;
  }

  toggleAddSDR() {
    const initialState = {
      managerId: this.managerId,
    };
    this.bsModalRef = this.modalService.show(EmployeeGroupingsEntryComponent, {
      initialState,
      class: 'modal-lg',
    });
    this.bsModalRef.content.onRefresh.subscribe(() => {
      this.refreshEmployees();
    });
  }

  hasFeatureAccess(action): boolean {
    this.user = this.storage.getItem(CURRENT_USER);
    if (this.user) {
      return this.authService.hasPermission(
        this.user.role,
        MENU_ITEM_NAME.CAMPAIGN.TAG,
        action
      );
    } else {
      return false;
    }
  }

  search(event: any) {
    const val = event.target.value;
    this.refreshEmployees(1, val);
  }

  clearSearch() {
    this.refreshEmployees();
  }

  pageChanged(event: PageChangedEvent) {
    console.log('event: ', event);
    const { page } = event;
    this.refreshEmployees(page);
  }

  removeSDR() {
    Swal.fire({
      title: 'Do you want to remove this SDR on the groupings?',
      showCancelButton: true,
      confirmButtonText: `Confirm`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        Swal.fire('Deleted!', '', 'success');
      }
    });
  }

  getSDRStatus(statusId: any) {
    switch (statusId) {
      case CAMPAIGN_SDR_STATUS_ENUM.ACTIVE:
        return {
          class: 'success',
          label: CAMPAIGN_SDR_STATUS.ACTIVE,
        };
      case CAMPAIGN_SDR_STATUS_ENUM.TERMINATED:
        return {
          class: 'danger',
          label: CAMPAIGN_SDR_STATUS.TERMINATED,
        };
      case CAMPAIGN_SDR_STATUS_ENUM.AVAILABLE:
        return {
          class: 'info',
          label: CAMPAIGN_SDR_STATUS.AVAILABLE,
        };

      default:
        return {
          class: 'warning',
          label: CAMPAIGN_SDR_STATUS.INACTIVE,
        };
    }
  }

  private refreshEmployees(page: number = 1, Search?: string) {
    const params: IGenericParams = {
      PageIndex: page,
      PageSize: this.itemsPerPage,
    };

    if (Search) {
      params.Search = Search;
    }

    this.usersService
      .getAllSDRGroupings(this.managerId, params)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (response) => {
          if (response && response.sdrs) {
            this.sdrArr = [...response.sdrs.data];
          }

          if (response && response.manager) {
            this.selectedSDRM = response.manager;
            this.pubsub.$pub('selected-sdrm', this.selectedSDRM);
          }
          this.showContent = true;
        },
        () => (this.showContent = true),
        () => (this.showContent = true)
      );
  }
}
