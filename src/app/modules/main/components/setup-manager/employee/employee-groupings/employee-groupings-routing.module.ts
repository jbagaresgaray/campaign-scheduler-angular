import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmployeeGroupingsComponent } from './employee-groupings.component';

const routes: Routes = [{ path: '', component: EmployeeGroupingsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmployeeGroupingsRoutingModule {}
