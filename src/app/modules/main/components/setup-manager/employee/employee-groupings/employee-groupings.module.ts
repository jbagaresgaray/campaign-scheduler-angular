import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  NbButtonModule,
  NbCardModule,
  NbIconModule,
  NbInputModule,
  NbListModule,
  NbSpinnerModule,
  NbTooltipModule,
  NbUserModule,
  NbWindowModule,
} from '@nebular/theme';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { EmployeeGroupingsRoutingModule } from './employee-groupings-routing.module';
import { ComponentsModule } from 'src/app/shared/components/components.module';

import { EmployeeGroupingsComponent } from './employee-groupings.component';
import { EmployeeGroupingsEntryComponent } from './employee-groupings-entry/employee-groupings-entry.component';

@NgModule({
  declarations: [EmployeeGroupingsComponent, EmployeeGroupingsEntryComponent],
  imports: [
    CommonModule,
    EmployeeGroupingsRoutingModule,
    ComponentsModule,
    NbButtonModule,
    NbCardModule,
    NbIconModule,
    NbInputModule,
    NbTooltipModule,
    NbListModule,
    NbSpinnerModule,
    NbUserModule,
    NbWindowModule,
    PaginationModule,
    FormsModule,
    IonicModule,
  ],
})
export class EmployeeGroupingsModule {}
