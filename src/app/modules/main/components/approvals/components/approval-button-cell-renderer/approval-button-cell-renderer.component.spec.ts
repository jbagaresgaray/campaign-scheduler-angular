import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprovalButtonCellRendererComponent } from './approval-button-cell-renderer.component';

describe('ApprovalButtonCellRendererComponent', () => {
  let component: ApprovalButtonCellRendererComponent;
  let fixture: ComponentFixture<ApprovalButtonCellRendererComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApprovalButtonCellRendererComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprovalButtonCellRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
