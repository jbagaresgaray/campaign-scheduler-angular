import { Component, OnInit } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
  selector: 'app-approval-button-cell-renderer',
  templateUrl: './approval-button-cell-renderer.component.html',
  styleUrls: ['./approval-button-cell-renderer.component.scss'],
})
export class ApprovalButtonCellRendererComponent
  implements ICellRendererAngularComp {
  params: any;

  constructor() {}

  agInit(params: any): void {
    this.params = params;
  }

  refresh(): boolean {
    return false;
  }

  approve() {
    this.params.clicked({
      action: 'approve',
      data: this.params.data,
    });
  }

  deny() {
    this.params.clicked({
      action: 'deny',
      data: this.params.data,
    });
  }
}
