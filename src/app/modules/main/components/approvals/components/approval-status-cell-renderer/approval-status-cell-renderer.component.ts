import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { PENDING_APPROVAL_STATUS_ENUM } from 'src/app/shared/constants/utils';

@Component({
  selector: 'app-approval-status-cell-renderer',
  templateUrl: './approval-status-cell-renderer.component.html',
  styleUrls: ['./approval-status-cell-renderer.component.scss'],
})
export class ApprovalStatusCellRendererComponent
  implements ICellRendererAngularComp {
  params: any;
  statusId: any;

  constructor() {}

  agInit(params: any): void {
    this.params = params;
    if (params && params.data) {
      this.statusId = params.data.statusId;
    }
  }

  refresh(): boolean {
    return false;
  }

  getStatus(statusId: number) {
    switch (statusId) {
      case PENDING_APPROVAL_STATUS_ENUM.APPROVED:
        return {
          class: 'success',
          label: 'Approved',
        };
      case PENDING_APPROVAL_STATUS_ENUM.DENIED:
        return {
          class: 'warning',
          label: 'Denied',
        };
      case PENDING_APPROVAL_STATUS_ENUM.PENDING:
        return {
          class: 'info',
          label: 'Pending',
        };
    }
  }
}
