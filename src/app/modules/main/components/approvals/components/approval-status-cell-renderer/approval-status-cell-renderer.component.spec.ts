import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprovalStatusCellRendererComponent } from './approval-status-cell-renderer.component';

describe('ApprovalStatusCellRendererComponent', () => {
  let component: ApprovalStatusCellRendererComponent;
  let fixture: ComponentFixture<ApprovalStatusCellRendererComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApprovalStatusCellRendererComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprovalStatusCellRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
