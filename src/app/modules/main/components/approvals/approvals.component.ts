import { Component, OnInit, ViewChild } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { NbMenuService } from '@nebular/theme';
import { Store } from '@ngrx/store';
import { AgGridAngular } from 'ag-grid-angular';
import { GridOptions } from 'ag-grid-community';
import isEmpty from 'lodash-es/isEmpty';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';

import { IUserAccount } from 'src/app/modules/auth/models/auth.model';
import { RoleCellRendererComponent } from 'src/app/shared/components/role-cell-renderer/role-cell-renderer.component';
import {
  GLOBAL_AG_GRID_OPTIONS,
  MAX_ITEMS_PER_PAGE,
} from 'src/app/shared/constants/utils';
import { UserService } from 'src/app/shared/mocks/services/employee.mock';
import { DataLoaderService } from 'src/app/shared/services/data-loader.service';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import Swal from 'sweetalert2';
import { IUserAccounts } from '../../models/users.model';
import { UsersService } from '../../services/users.service';
import { MainState } from '../../store/main.reducers';
import { PENDING_COLUMNS } from './approval.constant';
import { ApprovalButtonCellRendererComponent } from './components/approval-button-cell-renderer/approval-button-cell-renderer.component';
import { ApprovalStatusCellRendererComponent } from './components/approval-status-cell-renderer/approval-status-cell-renderer.component';

@Component({
  selector: 'app-approvals',
  templateUrl: './approvals.component.html',
  styleUrls: ['./approvals.component.scss'],
})
export class ApprovalsComponent implements OnInit {
  user: IUserAccount;
  pendingArr: IUserAccounts[] = [];
  selectedUser: IUserAccounts = null;

  fakeArr: any[] = [];

  itemsPerPage = MAX_ITEMS_PER_PAGE;
  totalItems: number;
  currentPage = 1;
  numOfPages = 0;
  showContent = false;

  columnDefs: any[] = [];
  private gridApi;
  frameworkComponents = {
    actionButtonRenderer: ApprovalButtonCellRendererComponent,
    statusCellRenderer: ApprovalStatusCellRendererComponent,
    roleStatusCellRenderer: RoleCellRendererComponent,
  };
  gridOptions: GridOptions = GLOBAL_AG_GRID_OPTIONS;
  @ViewChild('agGrid') agGrid: AgGridAngular;

  constructor(
    private store: Store<MainState>,
    private menuService: NbMenuService,
    private userService: UsersService,
    private dataLoader: DataLoaderService,
    private storage: LocalStorageService,
    private loadingController: LoadingController,
    private mockUserService: UserService
  ) {
    this.fakeArr = Array.from({
      length: 15,
    });

    for (const [key, value] of Object.entries(PENDING_COLUMNS)) {
      this.columnDefs.push({
        field: String(value.FIELD),
        headerName: String(value.LABEL),
        sortable: false,
        pinned: value.PINNED,
        headerClass: value.HEADERCLASS,
        cellStyle: {
          textAlign: value.TEXTALIGN,
        },
        editable: false,
        type: 'nonEditableColumn',
        width: value.WIDTH,
        cellRenderer: value.CELL_RENDERER,
        hide: value.HIDE,
        wrapText: value.WRAPTEXT,
        cellRendererParams: () => {
          if (value.LABEL === 'Action') {
            return {
              clicked: (field: any) => {
                const { action, data } = field;
                this.onGridActionButton(action, data);
              },
            };
          }
        },
      });
    }
  }

  ngOnInit(): void {
    this.refreshPending();
  }

  onGridReady(params: any) {
    this.gridApi = params.api;
    if (this.agGrid && this.agGrid.api) {
      if (isEmpty(this.pendingArr)) {
        this.agGrid.api.hideOverlay();
      }
    }
  }

  onGridActionButton(action: string, data: any) {
    if (action === 'update_status') {
      this.toggleUpdateStatus(data);
    } else if (action === 'approve') {
      this.approve(data);
    } else if (action === 'deny') {
      this.deny(data);
    }
  }

  search(event: any) {
    const val = event.target.value;
    this.refreshPending();
  }

  clearSearch() {
    this.refreshPending();
  }

  pageChanged(event: PageChangedEvent) {
    const { page } = event;
    this.refreshPending();
  }

  private approve(user: IUserAccounts) {
    const approveUser = () => {
      Swal.fire('Approved!', 'Account successfully approved!', 'success');
    };

    Swal.fire({
      title: 'Approve user account?',
      showCancelButton: true,
      confirmButtonText: `Confirm`,
      icon: 'info',
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        approveUser();
      }
    });
  }

  private deny(user: IUserAccounts) {
    const denyUser = () => {
      Swal.fire('Denied!', 'Account denied!', 'error');
    };

    Swal.fire({
      title: 'Deny user account?',
      showCancelButton: true,
      confirmButtonText: `Confirm`,
      icon: 'error',
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        denyUser();
      }
    });
  }

  private toggleUpdateStatus(user: IUserAccounts) {
    console.log('toggleUpdateStatus: ', user);
    this.selectedUser = user;
  }

  private refreshPending() {
    this.showContent = false;
    this.mockUserService.getPendingApprovals().subscribe((response) => {
      this.pendingArr = [...response];
    });
  }
}
