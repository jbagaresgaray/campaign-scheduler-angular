import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  NbButtonModule,
  NbCardModule,
  NbContextMenuModule,
  NbDatepickerModule,
  NbIconModule,
  NbInputModule,
  NbListModule,
  NbProgressBarModule,
  NbSelectModule,
  NbTooltipModule,
  NbUserModule,
} from '@nebular/theme';
import { AgGridModule } from 'ag-grid-angular';

import { FormsModule } from '@angular/forms';


import { ApprovalsRoutingModule } from './approvals-routing.module';
import { ApprovalsComponent } from './approvals.component';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { ApprovalStatusCellRendererComponent } from './components/approval-status-cell-renderer/approval-status-cell-renderer.component';
import { ApprovalButtonCellRendererComponent } from './components/approval-button-cell-renderer/approval-button-cell-renderer.component';


@NgModule({
  declarations: [ApprovalsComponent, ApprovalStatusCellRendererComponent, ApprovalButtonCellRendererComponent],
  imports: [
    CommonModule,
    ApprovalsRoutingModule,
    NbButtonModule,
    NbCardModule,
    NbIconModule,
    NbInputModule,
    NbTooltipModule,
    NbProgressBarModule,
    NbListModule,
    NbUserModule,
    NbDatepickerModule,
    NbSelectModule,
    NbContextMenuModule,
    FormsModule,
    ComponentsModule,
    AgGridModule,
  ],
})
export class ApprovalsModule { }
