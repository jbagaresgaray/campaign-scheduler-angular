import { Component, OnDestroy, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { map, take, takeUntil } from 'rxjs/operators';
import { Subject, Subscription } from 'rxjs';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import Swal from 'sweetalert2';
import cloneDeep from 'lodash-es/cloneDeep';
import isEmpty from 'lodash-es/isEmpty';
import { LoadingController } from '@ionic/angular';

import { profileSelector } from './../../../auth/store/auth.selectors';
import { AuthState } from './../../../auth/store/auth.reducers';
import { COUNTRIES } from './../../../../shared/constants/countries';
import { IUserProfile } from 'src/app/modules/auth/models/auth.model';
import {
  IDepartment,
  IDepartmentResponse,
} from '../../models/department.model';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { MainState } from '../../store/main.reducers';
import { ADMIN_DEPARTMENTS } from 'src/app/shared/constants/utils';
import { mainSelector } from '../../store/main.selectors';

import { UsersService } from '../../services/users.service';
import { IEmployee } from '../../models/employee.model';
import { DataLoaderService } from 'src/app/shared/services/data-loader.service';
import {
  IBlobImageResponse,
  UploadService,
} from '../../services/upload.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss'],
})
export class UserProfileComponent implements OnInit, OnDestroy {
  isSubmitting: boolean;
  countryArr: {
    name: string;
    code: string;
  }[];
  profile: IUserProfile;
  employee: IEmployee;
  departmentsArr: IDepartment[] = [];
  departmentId: number;

  filesToUpload: File[] = [];
  profileImage: any;
  base64String: string;
  imageFileName: string;

  userForm: FormGroup;
  isFormValid = false;

  $unsubscribe = new Subject<any>();
  unsubscribe$ = new Subject<any>();
  private updateSub: Subscription = new Subscription();

  constructor(
    private authstore: Store<AuthState>,
    private store: Store<MainState>,
    private formBuilder: FormBuilder,
    private storage: LocalStorageService,
    private loadingController: LoadingController,
    private usersService: UsersService,
    private dataLoader: DataLoaderService,
    private uploadService: UploadService
  ) {
    this.createForm();
  }

  ngOnInit(): void {
    this.isSubmitting = false;
    this.populateCountries();
    this.initProfile();
    this.initDepartments();
  }

  ngOnDestroy(): void {
    this.$unsubscribe.next();
    this.$unsubscribe.complete();
  }

  get f(): any {
    return this.userForm.controls;
  }

  uploadImage() {
    const input = document.createElement('input');
    input.type = 'file';
    input.name = 'image';
    input.accept = 'image/x-png,image/gif,image/jpeg';
    input.click();
    input.onchange = () => {
      const blob: File = input.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(blob);
      reader.onload = () => {
        const base64result = String(reader.result).split(',');
        this.profileImage = reader.result;
        this.base64String = base64result[1];
        this.imageFileName = blob.name;
      };
    };
  }

  async updateProfile() {
    if (this.isSubmitting) {
      return;
    }

    this.isSubmitting = true;

    if (this.userForm.invalid) {
      this.isSubmitting = false;
      this.validateAllFormFields(this.userForm);
      return;
    }

    const loading = await this.loadingController.create();
    await loading.present();

    const updateUserProfile = (imageUrl?: string) => {
      this.updateSub = this.usersService
        .updateUserProfile({
          ...this.userForm.getRawValue(),
          imageUrl,
        })
        .subscribe(
          (response) => {
            if (response) {
              this.dataLoader.refreshProfileData();
              Swal.fire('Updated successfully!', '', 'success');
            }
          },
          (err) => {
            console.log('err: ', err);
            this.isSubmitting = false;
            loading.dismiss();
            this.updateSub.unsubscribe();
          },
          () => {
            this.isSubmitting = false;
            loading.dismiss();
            this.updateSub.unsubscribe();
          }
        );
    };

    if (!isEmpty(this.base64String)) {
      this.uploadService
        .uploadBlob({
          base64String: this.base64String,
          imageFileName: this.imageFileName,
        })
        .pipe(take(1))
        .toPromise()
        .then(
          (resp: IBlobImageResponse) => {
            if (resp) {
              updateUserProfile(resp.url);
            }
          },
          (err) => {
            console.log('err: ', err);
            this.isSubmitting = false;
            loading.dismiss();
          }
        );
    } else {
      updateUserProfile(null);
    }
  }

  private populateCountries() {
    this.countryArr = COUNTRIES;
  }

  private initProfile() {
    this.authstore
      .select(profileSelector)
      .pipe(takeUntil(this.$unsubscribe))
      .subscribe((response) => {
        if (response && response.profile) {
          this.profile = response.profile;

          if (this.profile.image) {
            this.profileImage = this.profile.image;
          }
          console.log('this.profile: ', this.profile);
          this.getEmployeeDetails();
        }
      });
  }

  private initDepartments() {
    const CACHE_DATA = this.storage.getItem(ADMIN_DEPARTMENTS);
    const handleResponse = (departmentsArr) => {
      this.departmentsArr = [...departmentsArr];
    };

    this.store
      .pipe(
        select(mainSelector),
        takeUntil(this.unsubscribe$),
        map((res) => res.departments)
      )
      .subscribe((response: IDepartmentResponse) => {
        if (response && response.data) {
          const userAccounts = cloneDeep(response.data);
          handleResponse(userAccounts);
        } else {
          const data = CACHE_DATA.data;
          handleResponse(data);
        }
      });
  }

  private async getEmployeeDetails() {
    const loading = await this.loadingController.create();
    loading.present();
    this.usersService
      .getStaffDetails(Number(this.profile.id))
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (response: IEmployee) => {
          if (response) {
            this.employee = response;
            Object.keys(this.employee).forEach((name) => {
              if (this.employee[name] && this.userForm.controls[name]) {
                this.userForm.controls[name].setValue(this.employee[name]);
              }
            });
            console.log('this.employee: ', this.employee);
          }
        },
        () => loading.dismiss(),
        () => loading.dismiss()
      );
  }

  private createForm() {
    this.userForm = this.formBuilder.group({
      id: [''],
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email: [{ value: '', disabled: true }],
      linkedInUrl: [''],
      linkedInName: [''],
      departmentId: [''],
      dateHired: [''],
      address: [''],
      state: [''],
      postalCode: [''],
      country: [''],
    });
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
}
