import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';
import { LoadingBarModule } from '@ngx-loading-bar/core';
import { NgxSpinnerModule } from 'ngx-spinner';
import {
  NbThemeModule,
  NbLayoutModule,
  NbMenuModule,
  NbSidebarModule,
  NbTabsetModule,
  NbButtonModule,
} from '@nebular/theme';
import { ModalModule } from 'ngx-bootstrap/modal';

import { MiscellaneousModule } from '../../miscellaneous/miscellaneous.module';
import { ComponentsModule } from 'src/app/shared/components/components.module';

import { ThemeModule } from 'src/app/shared/@theme/@theme.module';
import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [MainComponent],
  imports: [
    CommonModule,
    MainRoutingModule,
    MiscellaneousModule,
    ComponentsModule,
    NbThemeModule,
    NbLayoutModule,
    NbMenuModule,
    NbSidebarModule,
    NbButtonModule,
    ThemeModule,
    LoadingBarModule,
    NgxSpinnerModule,
    NbTabsetModule,
    IonicModule,
    ModalModule,
  ],
})
export class MainModule {}
