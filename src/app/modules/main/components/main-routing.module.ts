import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainGuard } from 'src/app/shared/guards/main.guard';
import { NotFoundComponent } from '../../miscellaneous/not-found/not-found.component';

import { MainComponent } from './main.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    // canActivate: [MainGuard],
    children: [
      {
        path: 'dashboard',
        loadChildren: () =>
          import('./dashboard/dashboard.module').then((m) => m.DashboardModule),
      },
      {
        path: 'campaign',
        loadChildren: () =>
          import('./campaign/campaign.module').then((m) => m.CampaignModule),
      },
      {
        path: 'scheduler',
        loadChildren: () =>
          import('./scheduler/scheduler.module').then((m) => m.SchedulerModule),
      },
      {
        path: 'payroll',
        loadChildren: () =>
          import('./payroll/payroll.module').then((m) => m.PayrollModule),
      },
      {
        path: 'employee',
        loadChildren: () =>
          import('./setup-manager/employee/employee.module').then(
            (m) => m.EmployeeModule
          ),
      },
      {
        path: 'users',
        loadChildren: () =>
          import('./setup-manager/users/users.module').then(
            (m) => m.UsersModule
          ),
      },
      {
        path: 'departments',
        loadChildren: () =>
          import('./setup-manager/departments/departments.module').then(
            (m) => m.DepartmentsModule
          ),
      },
      {
        path: 'pricing',
        loadChildren: () =>
          import(
            './setup-manager/campaign-pricing/campaign-pricing.module'
          ).then((m) => m.CampaignPricingModule),
      },
      {
        path: 'user-profile',
        loadChildren: () =>
          import('./user-profile/user-profile.module').then(
            (m) => m.UserProfileModule
          ),
      },
      {
        path: 'payroll',
        loadChildren: () =>
          import('./payroll/payroll.module').then((m) => m.PayrollModule),
      },
      {
        path: 'kpi',
        loadChildren: () => import('./kpi/kpi.module').then((m) => m.KpiModule),
      },
      {
        path: 'approvals',
        loadChildren: () =>
          import('./approvals/approvals.module').then(
            (m) => m.ApprovalsModule
          ),
      },
      {
        path: 'change-password',
        loadChildren: () =>
          import('./change-password/change-password.module').then(
            (m) => m.ChangePasswordModule
          ),
      },
    ],
  },

  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '**',
    component: NotFoundComponent,
  },
  {
    path: '',
    component: MainComponent,
    // canActivate: [MainGuard]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MainRoutingModule {
  constructor() {
    console.log('MainRoutingModule');
  }
}
