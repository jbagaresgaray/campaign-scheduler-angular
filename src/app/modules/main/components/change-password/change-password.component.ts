import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { LoadingController } from '@ionic/angular';
import {
  ISetPassword,
  IUserAccount,
} from 'src/app/modules/auth/models/auth.model';
import { AuthService } from 'src/app/modules/auth/services/auth.service';
import {
  passwordValidationRegex,
  CURRENT_USER,
} from 'src/app/shared/constants/utils';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { ToastAlertService } from 'src/app/shared/services/toast-alert.service';

@Component({
  selector: 'app-change-password',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent implements OnInit {
  user: IUserAccount;
  resetForm: FormGroup;
  isFormValid = false;
  isSubmitting = false;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private toastService: ToastAlertService,
    private storage: LocalStorageService,
    private loadingController: LoadingController
  ) {}

  ngOnInit(): void {
    this.createForm();
  }

  get f(): any {
    return this.resetForm.controls;
  }

  showPassword(event: any): void {
    if (event.type === 'text') {
      event.type = 'password';
    } else {
      event.type = 'text';
    }
  }

  async resetPass() {
    this.isSubmitting = true;
    this.resetForm.disable();
    this.user = this.storage.getItem(CURRENT_USER);

    const resetFields: ISetPassword = {
      userId: this.user.id,
      ...this.resetForm.getRawValue(),
    };

    const loading = await this.loadingController.create();
    loading.present();

    if (this.resetForm.invalid) {
      this.isSubmitting = false;
      loading.dismiss();
      this.validateAllFormFields(this.resetForm);
      return;
    }

    this.authService
      .setPassword(resetFields)
      .toPromise()
      .then(
        (response) => {
          console.log('response: ', response);
          if (response.statusCode === 200) {
            this.isSubmitting = false;
            this.resetForm.enable();
            loading.dismiss();
            this.toastService.showSuccessAlert(response.message).then(() => {
              window.location.reload();
            });
          } else if (response.statusCode === 400) {
            this.isSubmitting = false;
            loading.dismiss();
            this.resetForm.enable();
            const message = response.message;
            this.toastService.showErrorToast('Warning!', message);
            return;
          }
        },
        (error) => {
          console.log('resetPassword error: ', error);
          if (error) {
            this.isSubmitting = false;
            this.resetForm.enable();
            loading.dismiss();

            const err = error.error;
            console.log('resetPassword err: ', err);
            if (err && err.errors) {
              const errors = err.errors;
              if (errors) {
                this.toastService.showErrorToast('Warning!', errors[0]);
                return;
              }
            } else if (err && err.message) {
              this.toastService.showErrorToast('Warning!', err.message);
              return;
            } else if (error && error.message) {
              this.toastService.showErrorToast('Warning!', error.message);
              return;
            }
          }
        }
      );
  }

  private createForm() {
    this.resetForm = this.formBuilder.group({
      currentPassword: ['', [Validators.required, Validators.minLength(6)]],
      newPassword: [
        '',
        [
          Validators.required,
          Validators.pattern(passwordValidationRegex),
          Validators.minLength(6),
        ],
      ],
      confirmPassword: [
        '',
        [
          Validators.required,
          Validators.pattern(passwordValidationRegex),
          Validators.minLength(6),
        ],
      ],
    });
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  private checkPasswords(control: FormControl): { [s: string]: boolean } {
    if (this.resetForm) {
      if (control.value !== this.resetForm.controls.newPassword.value) {
        return { passwordNotMatch: true };
      }
    }
    return null;
  }
}
