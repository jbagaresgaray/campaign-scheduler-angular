import { Component, OnDestroy, ViewEncapsulation } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators';
import { UserActive } from 'src/app/shared/mocks/data/user-activity';
import { UserActivtyService } from 'src/app/shared/mocks/services/user-activty.mock';

@Component({
  selector: 'app-scheduler-sdr-activity',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './scheduler-sdr-activity.component.html',
  styleUrls: ['./scheduler-sdr-activity.component.scss'],
})
export class SchedulerSdrActivityComponent implements OnDestroy {
  private alive = true;

  userActivity: UserActive[] = [];
  type = 'month';
  types = ['week', 'month', 'year'];
  currentTheme: string;

  constructor(
    private themeService: NbThemeService,
    private userActivityService: UserActivtyService
  ) {
    this.themeService
      .getJsTheme()
      .pipe(takeWhile(() => this.alive))
      .subscribe((theme) => {
        this.currentTheme = theme.name;
      });

    this.getUserActivity(this.type);
  }

  getUserActivity(period: string) {
    this.userActivityService
      .getUserActivityData(period)
      .pipe(takeWhile(() => this.alive))
      .subscribe((userActivityData) => {
        this.userActivity = userActivityData;
      });
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
