import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SchedulerSdrActivityComponent } from './scheduler-sdr-activity.component';

describe('SchedulerSdrActivityComponent', () => {
  let component: SchedulerSdrActivityComponent;
  let fixture: ComponentFixture<SchedulerSdrActivityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SchedulerSdrActivityComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedulerSdrActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
