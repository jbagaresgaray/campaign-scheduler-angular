import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SchedulerProgressCardComponent } from './scheduler-progress-card.component';

describe('SchedulerProgressCardComponent', () => {
  let component: SchedulerProgressCardComponent;
  let fixture: ComponentFixture<SchedulerProgressCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SchedulerProgressCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedulerProgressCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
