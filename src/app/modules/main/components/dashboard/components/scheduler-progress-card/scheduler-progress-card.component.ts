import { Component, OnDestroy, ViewEncapsulation } from '@angular/core';
import { takeWhile } from 'rxjs/operators';
import { ProgressInfo, StatsProgressBarData } from 'src/app/shared/mocks/data/stats-bar';
import { StatsProgressBarService } from 'src/app/shared/mocks/services/stats-progress.mock';

@Component({
  selector: 'app-scheduler-progress-card',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './scheduler-progress-card.component.html',
  styleUrls: ['./scheduler-progress-card.component.scss'],
})
export class SchedulerProgressCardComponent implements OnDestroy {
  private alive = true;

  progressInfoData: ProgressInfo[];

  constructor(private statsProgressBarService: StatsProgressBarService) {
    this.statsProgressBarService
      .getProgressInfoData()
      .pipe(takeWhile(() => this.alive))
      .subscribe((data) => {
        this.progressInfoData = data;
      });
  }

  ngOnDestroy() {
    this.alive = true;
  }
}
