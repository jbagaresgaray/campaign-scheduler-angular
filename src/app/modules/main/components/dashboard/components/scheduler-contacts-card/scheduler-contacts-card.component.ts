import { Component, OnDestroy, ViewEncapsulation } from '@angular/core';
import { takeWhile } from 'rxjs/operators';
import { forkJoin } from 'rxjs';
import { Contacts, RecentUsers } from 'src/app/shared/mocks/data/users';
import { UserService } from 'src/app/shared/mocks/services/employee.mock';

@Component({
  selector: 'app-scheduler-contacts-card',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './scheduler-contacts-card.component.html',
  styleUrls: ['./scheduler-contacts-card.component.scss'],
})
export class SchedulerContactsCardComponent implements OnDestroy {
  private alive = true;

  contacts: any[];
  recent: any[];

  constructor(private userService: UserService) {
    forkJoin(this.userService.getContacts(), this.userService.getRecentUsers())
      .pipe(takeWhile(() => this.alive))
      .subscribe(([contacts, recent]: [Contacts[], RecentUsers[]]) => {
        this.contacts = contacts;
        this.recent = recent;
      });
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
