import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SchedulerContactsCardComponent } from './scheduler-contacts-card.component';

describe('SchedulerContactsCardComponent', () => {
  let component: SchedulerContactsCardComponent;
  let fixture: ComponentFixture<SchedulerContactsCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SchedulerContactsCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedulerContactsCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
