import { Component, Input, OnDestroy, ViewEncapsulation } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators';

@Component({
  selector: 'app-scheduler-card-front',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './scheduler-card-front.component.html',
  styleUrls: ['./scheduler-card-front.component.scss'],
})
export class SchedulerCardFrontComponent implements OnDestroy {
  private alive = true;

  @Input() frontCardData: any;

  currentTheme: string;

  constructor(private themeService: NbThemeService) {
    this.themeService
      .getJsTheme()
      .pipe(takeWhile(() => this.alive))
      .subscribe((theme) => {
        this.currentTheme = theme.name;
      });
  }

  trackByDate(_, item) {
    return item.date;
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
