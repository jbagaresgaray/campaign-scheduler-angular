import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SchedulerCardBackComponent } from './scheduler-card-back.component';

describe('SchedulerCardBackComponent', () => {
  let component: SchedulerCardBackComponent;
  let fixture: ComponentFixture<SchedulerCardBackComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SchedulerCardBackComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedulerCardBackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
