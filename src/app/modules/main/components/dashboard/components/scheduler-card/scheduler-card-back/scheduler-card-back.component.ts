import { Component, Input, OnDestroy, ViewEncapsulation } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators';

@Component({
  selector: 'app-scheduler-card-back',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './scheduler-card-back.component.html',
  styleUrls: ['./scheduler-card-back.component.scss'],
})
export class SchedulerCardBackComponent implements OnDestroy {
  private alive = true;

  @Input() trafficBarData: any;

  currentTheme: string;

  constructor(private themeService: NbThemeService) {
    this.themeService
      .getJsTheme()
      .pipe(takeWhile(() => this.alive))
      .subscribe((theme) => {
        this.currentTheme = theme.name;
      });
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
