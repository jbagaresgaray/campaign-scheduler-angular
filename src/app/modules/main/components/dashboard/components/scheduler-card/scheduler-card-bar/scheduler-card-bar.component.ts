import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-scheduler-card-bar',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './scheduler-card-bar.component.html',
  styleUrls: ['./scheduler-card-bar.component.scss'],
})
export class SchedulerCardBarComponent {
  @Input() barData: {
    prevDate: string;
    prevValue: number;
    nextDate: string;
    nextValue: number;
  };
  @Input() successDelta: boolean;

  constructor() {}
}
