import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SchedulerCardFrontComponent } from './scheduler-card-front.component';

describe('SchedulerCardFrontComponent', () => {
  let component: SchedulerCardFrontComponent;
  let fixture: ComponentFixture<SchedulerCardFrontComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SchedulerCardFrontComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedulerCardFrontComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
