import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SchedulerCardHeaderComponent } from './scheduler-card-header.component';

describe('SchedulerCardHeaderComponent', () => {
  let component: SchedulerCardHeaderComponent;
  let fixture: ComponentFixture<SchedulerCardHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SchedulerCardHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedulerCardHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
