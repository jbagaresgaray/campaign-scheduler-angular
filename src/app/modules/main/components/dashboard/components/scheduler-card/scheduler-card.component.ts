import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { takeWhile } from 'rxjs/operators';
import { TrafficBar, TrafficList } from 'src/app/shared/mocks/data/traffic-bar';
import {
  TrafficBarService,
  TrafficListService,
} from 'src/app/shared/mocks/services/traffic-bar.mock';

@Component({
  selector: 'app-scheduler-card',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './scheduler-card.component.html',
  styleUrls: ['./scheduler-card.component.scss'],
})
export class SchedulerCardComponent implements OnInit {
  private alive = true;

  trafficBarData: TrafficBar;
  trafficListData: TrafficList;
  revealed = false;
  period: string = 'week';

  constructor(
    private trafficListService: TrafficListService,
    private trafficBarService: TrafficBarService
  ) {
    this.getTrafficFrontCardData(this.period);
    this.getTrafficBackCardData(this.period);
  }

  ngOnInit(): void {}

  toggleView() {
    this.revealed = !this.revealed;
  }

  setPeriodAngGetData(value: string): void {
    this.period = value;

    this.getTrafficFrontCardData(value);
    this.getTrafficBackCardData(value);
  }

  getTrafficBackCardData(period: string) {
    this.trafficBarService
      .getTrafficBarData(period)
      .pipe(takeWhile(() => this.alive))
      .subscribe((trafficBarData) => {
        this.trafficBarData = trafficBarData;
      });
  }

  getTrafficFrontCardData(period: string) {
    this.trafficListService
      .getTrafficListData(period)
      .pipe(takeWhile(() => this.alive))
      .subscribe((trafficListData) => {
        this.trafficListData = trafficListData;
      });
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
