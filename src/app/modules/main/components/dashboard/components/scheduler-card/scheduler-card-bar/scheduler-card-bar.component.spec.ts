import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SchedulerCardBarComponent } from './scheduler-card-bar.component';

describe('SchedulerCardBarComponent', () => {
  let component: SchedulerCardBarComponent;
  let fixture: ComponentFixture<SchedulerCardBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SchedulerCardBarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedulerCardBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
