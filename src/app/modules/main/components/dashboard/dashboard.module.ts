import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { SchedulerCardComponent } from './components/scheduler-card/scheduler-card.component';
import {
  NbCardModule,
  NbIconModule,
  NbListModule,
  NbProgressBarModule,
  NbSelectModule,
  NbTabsetModule,
  NbUserModule,
} from '@nebular/theme';

import { PipesModule } from 'src/app/shared/pipes/pipes.module';
import { MocksModule } from 'src/app/shared/mocks/mocks.module';

import { SchedulerCardHeaderComponent } from './components/scheduler-card/scheduler-card-header/scheduler-card-header.component';
import { SchedulerCardFrontComponent } from './components/scheduler-card/scheduler-card-front/scheduler-card-front.component';
import { SchedulerCardBackComponent } from './components/scheduler-card/scheduler-card-back/scheduler-card-back.component';
import { SchedulerCardBarComponent } from './components/scheduler-card/scheduler-card-bar/scheduler-card-bar.component';
import { SchedulerProgressCardComponent } from './components/scheduler-progress-card/scheduler-progress-card.component';
import { SchedulerSdrActivityComponent } from './components/scheduler-sdr-activity/scheduler-sdr-activity.component';
import { SchedulerContactsCardComponent } from './components/scheduler-contacts-card/scheduler-contacts-card.component';

import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { ManagerDashboardComponent } from './manager-dashboard/manager-dashboard.component';
import { SdrDashboardComponent } from './sdr-dashboard/sdr-dashboard.component';

@NgModule({
  declarations: [
    DashboardComponent,
    SchedulerCardComponent,
    SchedulerCardHeaderComponent,
    SchedulerCardFrontComponent,
    SchedulerCardBackComponent,
    SchedulerCardBarComponent,
    SchedulerProgressCardComponent,
    SchedulerSdrActivityComponent,
    AdminDashboardComponent,
    ManagerDashboardComponent,
    SchedulerContactsCardComponent,
    SdrDashboardComponent,
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    ComponentsModule,
    NbCardModule,
    NbSelectModule,
    NbIconModule,
    NbListModule,
    NbProgressBarModule,
    NbTabsetModule,
    NbUserModule,
    PipesModule,
    MocksModule,
  ],
})
export class DashboardModule {}
