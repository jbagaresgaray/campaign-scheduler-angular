import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-sdr-dashboard',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './sdr-dashboard.component.html',
  styleUrls: ['./sdr-dashboard.component.scss'],
})
export class SdrDashboardComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
