import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-manager-dashboard',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './manager-dashboard.component.html',
  styleUrls: ['./manager-dashboard.component.scss'],
})
export class ManagerDashboardComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
