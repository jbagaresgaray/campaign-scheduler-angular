import { Component, OnInit } from '@angular/core';
import { IUserAccount } from 'src/app/modules/auth/models/auth.model';
import { CURRENT_USER, USER_ROLE } from 'src/app/shared/constants/utils';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  user: IUserAccount;
  readonly USER_ROLE = USER_ROLE;

  constructor(private storage: LocalStorageService) { }

  ngOnInit(): void {
    this.user = this.storage.getItem(CURRENT_USER);
  }

}
