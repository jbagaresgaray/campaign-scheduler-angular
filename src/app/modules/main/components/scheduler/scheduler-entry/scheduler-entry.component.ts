import { Component, OnInit, ViewEncapsulation } from '@angular/core';
// import { BsModalRef } from 'ngx-bootstrap/modal';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { BsModalService } from 'ngx-bootstrap/modal';

import { CAMPAIGN_CONTENT } from 'src/app/shared/constants/utils';
import { IScheduler } from 'src/app/modules/main/models/scheduler.model';
import { ICampaign } from '../../../models/campaign.model';
import { ContentModalComponent } from 'src/app/shared/components/content-modal/content-modal.component';

@Component({
  selector: 'app-scheduler-entry',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './scheduler-entry.component.html',
  styleUrls: ['./scheduler-entry.component.scss'],
})
export class SchedulerEntryComponent implements OnInit {
  scheduler: IScheduler;
  campaign: ICampaign;
  schedulerForm: FormGroup;

  readonly CAMPAIGN_CONTENT = CAMPAIGN_CONTENT;

  constructor(
    private formBuilder: FormBuilder,
    private modalService: BsModalService
  ) {}

  ngOnInit(): void {
    this.createForm();
  }

  viewContent(key: string, title: string) {
    const initialState = {
      content: this.campaign.content[key],
      title,
    };
    this.modalService.show(ContentModalComponent, { initialState });
  }

  private createForm() {
    this.schedulerForm = this.formBuilder.group({
      id: ['', [Validators.required]],
      sdrId: ['', [Validators.required]],
      campaignId: ['', [Validators.required]],
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      middleName: [''],
      company: [''],
      jobTitle: [''],
      industry: [''],
      city: [''],
      state: [''],
      linkedInUrl: [''],
      connectionRequest: [{ value: false }],
      connectionRequestedDate: [''],
      requestAccepted: [{ value: false }],
      isMessage1: [{ value: false }],
      dateMessage1: [''],
      isMessage2: [{ value: false }],
      dateMessage2: [''],
      isMessage3: [{ value: false }],
      dateMessage3: [''],
      isMessage4: [{ value: false }],
      dateMessage4: [''],
      isMessage5: [{ value: false }],
      dateMessage5: [''],
      neutral: [''],
      negative: [''],
    });

    if (this.scheduler) {
      this.schedulerForm.patchValue(this.scheduler);
    }
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
}
