import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { ICampaign } from 'src/app/modules/main/models/campaign.model';
import { IEmployees } from 'src/app/modules/main/models/employee.model';

@Component({
  selector: 'app-scheduler-header',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './scheduler-header.component.html',
  styleUrls: ['./scheduler-header.component.scss'],
})
export class SchedulerHeaderComponent implements OnInit {
  @Input() campaign: ICampaign;
  @Input() employee: IEmployees;
  @Input() campaignId: number;

  constructor() {}

  ngOnInit(): void {}
}
