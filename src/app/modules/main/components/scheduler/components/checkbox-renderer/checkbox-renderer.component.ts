import { Component, ViewEncapsulation } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
  selector: 'app-scheduler-checkbox-renderer',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './checkbox-renderer.component.html',
  styleUrls: ['./checkbox-renderer.component.scss'],
})
export class CheckboxRendererComponent implements ICellRendererAngularComp {
  constructor() {}

  params: any;

  agInit(params: any): void {
    this.params = params;
  }

  // checkedHandler(event) {
  //   let checked = event.target.checked;
  //   let colId = this.params.column.colId;
  //   this.params.node.setDataValue(colId, checked);
  // }

  refresh(): boolean {
    return true;
  }
}
