import { Component, ViewEncapsulation } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { IUserAccount } from 'src/app/modules/auth/models/auth.model';
import { CURRENT_USER, USER_ROLE } from 'src/app/shared/constants/utils';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';

@Component({
  selector: 'app-action-button-renderer',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './action-button-renderer.component.html',
  styleUrls: ['./action-button-renderer.component.scss'],
})
export class ActionButtonRendererComponent implements ICellRendererAngularComp {
  params: any;
  user: IUserAccount;

  readonly DELETE_TOOLTIP = 'Delete';
  readonly EDIT_TOOLTIP = 'View';
  readonly USER_ROLE = USER_ROLE;

  constructor(private storage: LocalStorageService) {}

  agInit(params: any): void {
    this.params = params;
    this.user = this.storage.getItem(CURRENT_USER);
  }

  refresh(): boolean {
    return false;
  }

  updateHandler() {
    this.params.clicked({
      action: 'edit',
      data: this.params.data,
    });
  }

  deleteHandler() {
    this.params.clicked({
      action: 'delete',
      data: this.params.data,
    });
  }
}
