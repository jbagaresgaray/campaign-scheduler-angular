import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-action-button-popover',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './action-button-popover.component.html',
  styleUrls: ['./action-button-popover.component.scss'],
})
export class ActionButtonPopoverComponent implements OnInit {
  @Input() actionArr: any[];

  constructor() {}

  ngOnInit(): void {}
}
