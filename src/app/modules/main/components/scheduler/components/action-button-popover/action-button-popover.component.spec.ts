import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionButtonPopoverComponent } from './action-button-popover.component';

describe('ActionButtonPopoverComponent', () => {
  let component: ActionButtonPopoverComponent;
  let fixture: ComponentFixture<ActionButtonPopoverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActionButtonPopoverComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionButtonPopoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
