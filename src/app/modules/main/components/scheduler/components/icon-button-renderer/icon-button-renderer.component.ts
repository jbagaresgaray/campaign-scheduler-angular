import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

import { SCHEDULER_MORE_ACTION } from 'src/app/modules/main/components/scheduler/scheduler';
import {
  CAMPAIGN_CONTENT,
  CURRENT_USER,
  PERMISSION_ACTION_DESC,
} from 'src/app/shared/constants/utils';
import { ICampaign } from '../../../../models/campaign.model';
import { NbMenuItem } from '@nebular/theme';
import { IScheduler } from 'src/app/modules/main/models/scheduler.model';
import { AuthService } from 'src/app/modules/auth/services/auth.service';
import { MENU_ITEM_NAME } from 'src/app/shared/constants/page.menu';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { IUserAccount } from 'src/app/modules/auth/models/auth.model';

@Component({
  selector: 'app-icon-button-renderer',
  templateUrl: './icon-button-renderer.component.html',
  styleUrls: ['./icon-button-renderer.component.scss'],
})
export class IconButtonRendererComponent implements ICellRendererAngularComp {
  params: any;
  campaign: ICampaign;
  scheduler: IScheduler;
  user: IUserAccount;
  readonly PERMISSION_ACTION_DESC = PERMISSION_ACTION_DESC;

  connectionRequestArr: NbMenuItem[] = [];
  message1RequestArr: NbMenuItem[] = [];
  message2RequestArr: NbMenuItem[] = [];
  message3RequestArr: NbMenuItem[] = [];
  message4RequestArr: NbMenuItem[] = [];
  message5RequestArr: NbMenuItem[] = [];
  requestAcceptedArr: NbMenuItem[] = [
    {
      title: 'Confirmed',
      data: 'request_confirmed',
    },
    {
      title: 'Rejected',
      data: 'request_rejected',
    },
  ];

  isConnectionRequest = false;
  isRequestAccepted = false;
  isMessage1 = false;
  isMessage2 = false;
  isMessage3 = false;
  isMessage4 = false;
  isMessage5 = false;
  isPositiveResponse = false;
  isNegativeResponse = false;

  readonly MORE_ACTION = SCHEDULER_MORE_ACTION;
  readonly CAMPAIGN_CONTENT = CAMPAIGN_CONTENT;

  constructor(
    private storage: LocalStorageService,
    private authService: AuthService
  ) {
    for (const [key, value] of Object.entries(
      this.MORE_ACTION.CONNECTION_REQUEST.CONTEXT_MENU
    )) {
      this.connectionRequestArr.push({
        title: String(value.title),
        data: String(value.data),
      });
    }

    for (const [key, value] of Object.entries(
      this.MORE_ACTION.MESSAGE_1.CONTEXT_MENU
    )) {
      this.message1RequestArr.push({
        title: String(value.title),
        data: String(value.data),
      });
    }

    for (const [key, value] of Object.entries(
      this.MORE_ACTION.MESSAGE_2.CONTEXT_MENU
    )) {
      this.message2RequestArr.push({
        title: String(value.title),
        data: String(value.data),
      });
    }

    for (const [key, value] of Object.entries(
      this.MORE_ACTION.MESSAGE_3.CONTEXT_MENU
    )) {
      this.message3RequestArr.push({
        title: String(value.title),
        data: String(value.data),
      });
    }

    for (const [key, value] of Object.entries(
      this.MORE_ACTION.MESSAGE_4.CONTEXT_MENU
    )) {
      this.message4RequestArr.push({
        title: String(value.title),
        data: String(value.data),
      });
    }

    for (const [key, value] of Object.entries(
      this.MORE_ACTION.MESSAGE_5.CONTEXT_MENU
    )) {
      this.message5RequestArr.push({
        title: String(value.title),
        data: String(value.data),
      });
    }
  }

  agInit(params: any): void {
    this.params = params;
    const data = params.data;
    if (params && data && data.campaign) {
      this.campaign = params.campaign;
    }

    this.scheduler = data;

    this.isConnectionRequest = data.connectionRequest;
    this.isRequestAccepted = data.requestAccepted;
    this.isMessage1 = data.isMessage1;
    this.isMessage2 = data.isMessage2;
    this.isMessage3 = data.isMessage3;
    this.isMessage4 = data.isMessage4;
    this.isMessage5 = data.isMessage5;
    this.isPositiveResponse = !!data.neutral;
    this.isNegativeResponse = !!data.negative;
  }

  refresh(): boolean {
    return false;
  }

  hasFeatureAccess(action): boolean {
    this.user = this.storage.getItem(CURRENT_USER);
    return this.authService.hasPermission(
      this.user.role,
      MENU_ITEM_NAME.SCHEDULER.TAG,
      action
    );
  }

  requestConnectedStatus() {
    if (this.isRequestAccepted && this.scheduler.requestAccepted !== null) {
      return 'success';
    } else if (
      !this.isRequestAccepted &&
      this.scheduler.requestAccepted !== null
    ) {
      return 'danger';
    } else {
      return 'basic';
    }
  }

  onConnectionRequest() {
    this.params.clicked({
      data: this.params.data,
    });
  }

  onRequestAccepted() {
    this.params.clicked({
      action: this.MORE_ACTION.CONNECTION_REQUEST_ACCEPTED.FIELD,
      data: this.params.data,
    });
  }

  onMessageSent() {
    this.params.clicked({
      data: this.params.data,
    });
  }

  onNeutralPositive() {
    this.params.clicked({
      action: this.MORE_ACTION.NEUTRAL_POSITIVE_RESPONSE.FIELD,
      data: this.params.data,
    });
  }

  onNegativeNoFollowUp() {
    this.params.clicked({
      action: this.MORE_ACTION.NEGATIVE_NO_FOLLOW_UP.FIELD,
      data: this.params.data,
    });
  }
}
