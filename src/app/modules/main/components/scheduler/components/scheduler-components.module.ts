import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgGridModule } from 'ag-grid-angular';
import {
  NbButtonModule,
  NbContextMenuModule,
  NbIconModule,
  NbTooltipModule,
} from '@nebular/theme';
import { IonicModule } from '@ionic/angular';

import { CheckboxRendererComponent } from './checkbox-renderer/checkbox-renderer.component';
import { ActionButtonRendererComponent } from './action-button-renderer/action-button-renderer.component';
import { SchedulerHeaderComponent } from './scheduler-header/scheduler-header.component';
import { SchedulerMainListingComponent } from './scheduler-main-listing/scheduler-main-listing.component';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { IconButtonRendererComponent } from './icon-button-renderer/icon-button-renderer.component';
import { ActionButtonPopoverComponent } from './action-button-popover/action-button-popover.component';

const COMPONENTS = [
  CheckboxRendererComponent,
  ActionButtonRendererComponent,
  IconButtonRendererComponent,
  SchedulerHeaderComponent,
  SchedulerMainListingComponent,
  ActionButtonPopoverComponent,
];
@NgModule({
  declarations: [...COMPONENTS],
  exports: [...COMPONENTS],
  imports: [
    CommonModule,
    AgGridModule,
    NbButtonModule,
    NbTooltipModule,
    NbIconModule,
    NbContextMenuModule,
    IonicModule,
    ComponentsModule,
  ],
})
export class SchedulerComponentsModule {}
