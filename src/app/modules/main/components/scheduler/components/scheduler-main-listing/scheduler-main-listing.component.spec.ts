import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SchedulerMainListingComponent } from './scheduler-main-listing.component';

describe('SchedulerMainListingComponent', () => {
  let component: SchedulerMainListingComponent;
  let fixture: ComponentFixture<SchedulerMainListingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SchedulerMainListingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedulerMainListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
