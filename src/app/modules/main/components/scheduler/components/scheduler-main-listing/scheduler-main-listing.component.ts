import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { AgGridAngular } from 'ag-grid-angular';
import { GridOptions } from 'ag-grid-community';
import { BsModalService } from 'ngx-bootstrap/modal';
import Swal from 'sweetalert2';
import * as momentTZ from 'moment-timezone';
import isEmpty from 'lodash-es/isEmpty';
import { LoadingController } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { NbMenuService, NbWindowService } from '@nebular/theme';
import { Subscription } from 'rxjs';

import {
  IScheduler,
  ISchedulerData,
} from 'src/app/modules/main/models/scheduler.model';
import {
  SCHEDULER_COLUMNS,
  SCHEDULER_MORE_ACTION,
} from 'src/app/modules/main/components/scheduler/scheduler';
import { ActionButtonRendererComponent } from '../action-button-renderer/action-button-renderer.component';
import { CheckboxRendererComponent } from '../checkbox-renderer/checkbox-renderer.component';
import { IconButtonRendererComponent } from '../icon-button-renderer/icon-button-renderer.component';
import {
  CAMPAIGN_CONTENT,
  FORMSTATE,
  GLOBAL_AG_GRID_OPTIONS,
} from 'src/app/shared/constants/utils';
import { SchedulerEntryComponent } from '../../scheduler-entry/scheduler-entry.component';
import { ICampaign } from '../../../../models/campaign.model';
import { SchedulerService } from 'src/app/modules/main/services/scheduler.service';
import { ToastAlertService } from 'src/app/shared/services/toast-alert.service';
import { ContentModalComponent } from 'src/app/shared/components/content-modal/content-modal.component';

@Component({
  selector: 'app-scheduler-main-listing',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './scheduler-main-listing.component.html',
  styleUrls: ['./scheduler-main-listing.component.scss'],
})
export class SchedulerMainListingComponent implements OnInit {
  @Input() schedulerData: any[] = [];
  @Input() showLoading: boolean;
  @Input() showNoRows: boolean;
  @Input() campaign: ICampaign;

  @Output() onRefresh = new EventEmitter<any>();

  selectedScheduler: ISchedulerData;

  schedulerColumnDefs: any[] = [];

  frameworkComponents = {
    checkboxRenderer: CheckboxRendererComponent,
    actionButtonRenderer: ActionButtonRendererComponent,
    iconButtonRenderer: IconButtonRendererComponent,
  };

  private connectionReqSub: Subscription = new Subscription();
  private readonly SCHEDULER_COLUMNS = SCHEDULER_COLUMNS;
  private readonly MORE_ACTION = SCHEDULER_MORE_ACTION;
  private gridApi;
  @ViewChild('agGrid') agGrid: AgGridAngular;

  headerComponentParams = {
    headerComponentParams: {
      template:
        '<div class="ag-cell-label-container" role="presentation">' +
        '  <span ref="eMenu" class="ag-header-icon ag-header-cell-menu-button"></span>' +
        '  <div ref="eLabel" class="ag-header-cell-label" role="presentation">' +
        '    <span ref="eSortOrder" class="ag-header-icon ag-sort-order"></span>' +
        '    <span ref="eSortAsc" class="ag-header-icon ag-sort-ascending-icon"></span>' +
        '    <span ref="eSortDesc" class="ag-header-icon ag-sort-descending-icon"></span>' +
        '    <span ref="eSortNone" class="ag-header-icon ag-sort-none-icon"></span>' +
        '    <span ref="eText" class="ag-header-cell-text" role="columnheader" style="white-space: normal;"></span>' +
        '    <span ref="eFilter" class="ag-header-icon ag-filter-icon"></span>' +
        '  </div>' +
        '</div>',
    },
  };

  gridOptions: GridOptions = {};

  constructor(
    private modalService: BsModalService,
    private windowService: NbWindowService,
    private schedulerService: SchedulerService,
    private loadingController: LoadingController,
    private toaster: ToastAlertService,
    private menuService: NbMenuService
  ) {
    this.gridOptions = {
      ...GLOBAL_AG_GRID_OPTIONS,
      defaultColDef: {
        ...GLOBAL_AG_GRID_OPTIONS.defaultColDef,
        ...this.headerComponentParams,
      },
    };

    for (const [key, value] of Object.entries(this.SCHEDULER_COLUMNS)) {
      this.schedulerColumnDefs.push({
        field: String(value.FIELD),
        headerName: String(value.LABEL),
        sortable: false,
        pinned: value.PINNED,
        headerClass: value.HEADERCLASS,
        cellStyle: {
          textAlign: value.TEXTALIGN,
        },
        editable: false,
        type: 'nonEditableColumn',
        width: value.WIDTH,
        cellRenderer: value.CELL_RENDERER,
        hide: value.HIDE,
        wrapText: value.WRAPTEXT,
        resizable: value.RESIZABLE,
        cellRendererParams: () => {
          if (
            value.LABEL === 'Action' ||
            value.CELL_RENDERER === 'iconButtonRenderer'
          ) {
            return {
              clicked: (field: any) => {
                const { action, data } = field;
                this.onGridActionButton(action, data);
              },
            };
          }
        },
      });
    }

    this.menuService
      .onItemClick()
      .pipe(
        filter(({ tag }) => tag === 'connection-request-context-menu'),
        map(({ item: { data } }) => data)
      )
      .subscribe((action) => {
        console.log('action: ', action);
        if (action === CAMPAIGN_CONTENT.INVITE_MESSAGE.KEY) {
          this.viewContent(
            CAMPAIGN_CONTENT.INVITE_MESSAGE.KEY,
            CAMPAIGN_CONTENT.INVITE_MESSAGE.LABEL
          );
        } else if (
          action === `${CAMPAIGN_CONTENT.INVITE_MESSAGE.KEY}_complete`
        ) {
          this.onConnectionRequest(true);
        }
      });

    this.menuService
      .onItemClick()
      .pipe(
        filter(({ tag }) => tag === 'request-accepted-context-menu'),
        map(({ item: { data } }) => data)
      )
      .subscribe((action) => {
        if (action === 'request_confirmed') {
          this.onRequestAccepted(true);
        } else if (action === 'request_rejected') {
          this.onRequestAccepted(false);
        }
      });

    this.menuService
      .onItemClick()
      .pipe(
        filter(({ tag }) => tag === 'message-1-context-menu'),
        map(({ item: { data } }) => data)
      )
      .subscribe((action) => {
        console.log('action: ', action);
        if (action === CAMPAIGN_CONTENT.MESSAGE_1.KEY) {
          this.viewContent(
            CAMPAIGN_CONTENT.MESSAGE_1.KEY,
            CAMPAIGN_CONTENT.MESSAGE_1.LABEL
          );
        } else if (action === `${CAMPAIGN_CONTENT.MESSAGE_1.KEY}_complete`) {
          this.onMessageSent(this.MORE_ACTION.MESSAGE_1.FIELD, true);
        }
      });

    this.menuService
      .onItemClick()
      .pipe(
        filter(({ tag }) => tag === 'message-2-context-menu'),
        map(({ item: { data } }) => data)
      )
      .subscribe((action) => {
        console.log('action: ', action);
        if (action === CAMPAIGN_CONTENT.MESSAGE_2.KEY) {
          this.viewContent(
            CAMPAIGN_CONTENT.MESSAGE_2.KEY,
            CAMPAIGN_CONTENT.MESSAGE_2.LABEL
          );
        } else if (action === `${CAMPAIGN_CONTENT.MESSAGE_2.KEY}_complete`) {
          this.onMessageSent(this.MORE_ACTION.MESSAGE_2.FIELD, true);
        }
      });

    this.menuService
      .onItemClick()
      .pipe(
        filter(({ tag }) => tag === 'message-3-context-menu'),
        map(({ item: { data } }) => data)
      )
      .subscribe((action) => {
        console.log('action: ', action);
        if (action === CAMPAIGN_CONTENT.MESSAGE_3.KEY) {
          this.viewContent(
            CAMPAIGN_CONTENT.MESSAGE_3.KEY,
            CAMPAIGN_CONTENT.MESSAGE_3.LABEL
          );
        } else if (action === `${CAMPAIGN_CONTENT.MESSAGE_3.KEY}_complete`) {
          this.onMessageSent(this.MORE_ACTION.MESSAGE_3.FIELD, true);
        }
      });

    this.menuService
      .onItemClick()
      .pipe(
        filter(({ tag }) => tag === 'message-4-context-menu'),
        map(({ item: { data } }) => data)
      )
      .subscribe((action) => {
        console.log('action: ', action);
        if (action === CAMPAIGN_CONTENT.MESSAGE_4.KEY) {
          this.viewContent(
            CAMPAIGN_CONTENT.MESSAGE_4.KEY,
            CAMPAIGN_CONTENT.MESSAGE_4.LABEL
          );
        } else if (action === `${CAMPAIGN_CONTENT.MESSAGE_4.KEY}_complete`) {
          this.onMessageSent(this.MORE_ACTION.MESSAGE_4.FIELD, true);
        }
      });

    this.menuService
      .onItemClick()
      .pipe(
        filter(({ tag }) => tag === 'message-5-context-menu'),
        map(({ item: { data } }) => data)
      )
      .subscribe((action) => {
        console.log('action: ', action);
        if (action === CAMPAIGN_CONTENT.MESSAGE_5.KEY) {
          this.viewContent(
            CAMPAIGN_CONTENT.MESSAGE_5.KEY,
            CAMPAIGN_CONTENT.MESSAGE_5.LABEL
          );
        } else if (action === `${CAMPAIGN_CONTENT.MESSAGE_5.KEY}_complete`) {
          this.onMessageSent(this.MORE_ACTION.MESSAGE_5.FIELD, true);
        }
      });
  }

  ngOnInit(): void {}

  onGridReady(params) {
    this.gridApi = params.api;
    if (this.agGrid && this.agGrid.api) {
      if (isEmpty(this.schedulerData)) {
        this.agGrid.api.hideOverlay();
      }
    }
  }

  headerHeightSetter() {
    const padding = 40;
    const allColumnIds = [];

    const height = this.headerHeightGetter() + padding;
    this.gridApi.setHeaderHeight(height);
    this.gridApi.resetRowHeights();

    this.agGrid.columnApi.getAllColumns().forEach((column: any) => {
      allColumnIds.push(column.colId);
    });
    this.agGrid.columnApi.autoSizeColumns(allColumnIds, false);
  }

  onGridActionButton(action: string, data: any) {
    this.selectedScheduler = this.formatSchedulerData(data);
    if (action === FORMSTATE.DELETE) {
      this.deleteSchedule(data);
    } else if (action === FORMSTATE.EDIT) {
      this.updateSchedule(data);
    } else if (
      action === SCHEDULER_MORE_ACTION.NEUTRAL_POSITIVE_RESPONSE.FIELD
    ) {
      this.onNeutralPositive(
        momentTZ(new Date()).format('YYYY-MM-DD HH:mm:ss')
      );
    } else if (action === SCHEDULER_MORE_ACTION.NEGATIVE_NO_FOLLOW_UP.FIELD) {
      this.onNegativeNoFollowUp(
        momentTZ(new Date()).format('YYYY-MM-DD HH:mm:ss')
      );
    }
  }

  private headerHeightGetter() {
    const columnHeaderTexts = document.querySelectorAll('.ag-header-cell-text');
    const columnHeaderTextsArray = [];
    columnHeaderTexts.forEach((node) => columnHeaderTextsArray.push(node));
    const clientHeights = columnHeaderTextsArray.map(
      (headerText) => headerText.clientHeight
    );
    const tallestHeaderTextHeight = Math.max(...clientHeights);
    return tallestHeaderTextHeight;
  }

  private updateSchedule(scheduler: IScheduler) {
    // const initialState = {
    //   scheduler,
    // };
    // this.modalService.show(SchedulerEntryComponent, {
    //   initialState,
    //   class: 'modal-lg window-modal',
    // });

    this.windowService
      .open(SchedulerEntryComponent, {
        title: `${scheduler.firstName} ${scheduler.lastName}`,
        context: {
          scheduler,
          campaign: this.campaign,
        },
        windowClass: 'window-fullscreen',
      })
      .maximize();
  }

  private deleteSchedule(item: IScheduler) {
    Swal.fire({
      title: 'Do you want to delete this item?',
      showCancelButton: true,
      confirmButtonText: `Confirm`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        Swal.fire('Deleted!', '', 'success');
      }
    });
  }

  private formatSchedulerData(scheduler: IScheduler): ISchedulerData {
    return {
      id: Number(scheduler.id),
      firstName: scheduler.firstName,
      lastName: scheduler.lastName,
      middleName: scheduler.middleName,
      jobTitle: scheduler.jobTitle,
      company: scheduler.company,
      industry: scheduler.industry,
      city: scheduler.city,
      state: scheduler.state,
      linkedInUrl: scheduler.linkedInUrl,
      connectionRequest: scheduler.connectionRequest,
      date: scheduler.date,
      requestAccepted: scheduler.requestAccepted,
      isMessage1: scheduler.isMessage1,
      dateMessage1: scheduler.dateMessage1,
      isMessage2: scheduler.isMessage2,
      dateMessage2: scheduler.dateMessage2,
      isMessage3: scheduler.isMessage3,
      dateMessage3: scheduler.dateMessage3,
      isMessage4: scheduler.isMessage4,
      dateMessage4: scheduler.dateMessage4,
      isMessage5: scheduler.isMessage5,
      dateMessage5: scheduler.dateMessage5,
      neutral: scheduler.neutral,
      negative: scheduler.negative,
    };
  }

  private async onConnectionRequest(connectionRequest) {
    if (this.selectedScheduler.connectionRequest) {
      Swal.fire({
        title: 'Oops...',
        text: this.MORE_ACTION.CONNECTION_REQUEST.LABEL + ' already done!',
        icon: 'error',
      });
      return;
    }

    const loading = await this.loadingController.create();
    loading.present();
    this.connectionReqSub = this.schedulerService
      .updateScheduler({
        ...this.selectedScheduler,
        ...{
          connectionRequest,
          date: momentTZ(new Date()).format('YYYY-MM-DD HH:mm:ss'),
        },
      })
      .subscribe(
        () => {
          loading.dismiss();
          this.toaster.showSuccessToast('Success', 'Saved!');
          this.onRefresh.emit();
          this.connectionReqSub.unsubscribe();
        },
        (error) => {
          loading.dismiss();
          this.connectionReqSub.unsubscribe();

          if (error) {
            const err = error.error;
            if (err && err.message) {
              this.toaster.showErrorToast('WARNING!', err.message);
            }
          }
        }
      );
  }

  private async onRequestAccepted(requestAccepted: boolean) {
    if (!this.selectedScheduler.connectionRequest) {
      Swal.fire({
        title: 'Oops...',
        text:
          this.MORE_ACTION.CONNECTION_REQUEST.LABEL +
          ' must be completed first!',
        icon: 'error',
      });
      return;
    }

    if (this.selectedScheduler.requestAccepted !== null) {
      Swal.fire({
        title: 'Oops...',
        text:
          this.MORE_ACTION.CONNECTION_REQUEST_ACCEPTED.LABEL +
          ' already completed!',
        icon: 'error',
      });
      return;
    }

    const loading = await this.loadingController.create();
    loading.present();

    this.connectionReqSub = this.schedulerService
      .updateScheduler({
        ...this.selectedScheduler,
        ...{
          requestAccepted,
        },
      })
      .subscribe(
        () => {
          loading.dismiss();
          this.toaster.showSuccessToast('Success', 'Saved!');
          this.onRefresh.emit();
          this.connectionReqSub.unsubscribe();
        },
        (error) => {
          loading.dismiss();
          this.connectionReqSub.unsubscribe();

          if (error) {
            const err = error.error;
            if (err && err.message) {
              this.toaster.showErrorToast('WARNING!', err.message);
            }
          }
        }
      );
  }

  private async onMessageSent(key: string, isSent: boolean) {
    if (!this.selectedScheduler.connectionRequest) {
      Swal.fire({
        title: 'Oops...',
        text:
          this.MORE_ACTION.CONNECTION_REQUEST.LABEL +
          ' must be completed first!',
        icon: 'error',
      });
      return;
    }

    if (
      key === this.MORE_ACTION.MESSAGE_1.FIELD &&
      !this.selectedScheduler.connectionRequest
    ) {
      Swal.fire({
        title: 'Oops...',
        text:
          this.MORE_ACTION.CONNECTION_REQUEST.LABEL +
          ' must be completed first!',
        icon: 'error',
      });
      return;
    }

    if (
      key === this.MORE_ACTION.MESSAGE_2.FIELD &&
      !this.selectedScheduler.isMessage1
    ) {
      Swal.fire({
        title: 'Oops...',
        text: this.MORE_ACTION.MESSAGE_1.LABEL + ' must be completed first!',
        icon: 'error',
      });
      return;
    }

    if (
      key === this.MORE_ACTION.MESSAGE_3.FIELD &&
      !this.selectedScheduler.isMessage2
    ) {
      Swal.fire({
        title: 'Oops...',
        text: this.MORE_ACTION.MESSAGE_2.LABEL + ' must be completed first!',
        icon: 'error',
      });
      return;
    }

    if (
      key === this.MORE_ACTION.MESSAGE_4.FIELD &&
      !this.selectedScheduler.isMessage3
    ) {
      Swal.fire({
        title: 'Oops...',
        text: this.MORE_ACTION.MESSAGE_3.LABEL + ' must be completed first!',
        icon: 'error',
      });
      return;
    }

    if (
      key === this.MORE_ACTION.MESSAGE_5.FIELD &&
      !this.selectedScheduler.isMessage4
    ) {
      Swal.fire({
        title: 'Oops...',
        text: this.MORE_ACTION.MESSAGE_4.LABEL + ' must be completed first!',
        icon: 'error',
      });
      return;
    }

    if (
      key === this.MORE_ACTION.MESSAGE_1.FIELD &&
      this.selectedScheduler.isMessage1
    ) {
      Swal.fire({
        title: 'Oops...',
        text: 'Message 1 already completed',
        icon: 'error',
      });
      return;
    } else if (
      key === this.MORE_ACTION.MESSAGE_2.FIELD &&
      this.selectedScheduler.isMessage2
    ) {
      Swal.fire({
        title: 'Oops...',
        text: 'Message 2 already completed',
        icon: 'error',
      });
      return;
    } else if (
      key === this.MORE_ACTION.MESSAGE_3.FIELD &&
      this.selectedScheduler.isMessage3
    ) {
      Swal.fire({
        title: 'Oops...',
        text: 'Message 3 already completed',
        icon: 'error',
      });
      return;
    } else if (
      key === this.MORE_ACTION.MESSAGE_4.FIELD &&
      this.selectedScheduler.isMessage4
    ) {
      Swal.fire({
        title: 'Oops...',
        text: 'Message 4 already completed',
        icon: 'error',
      });
      return;
    } else if (
      key === this.MORE_ACTION.MESSAGE_5.FIELD &&
      this.selectedScheduler.isMessage5
    ) {
      Swal.fire({
        title: 'Oops...',
        text: 'Message 5 already completed',
        icon: 'error',
      });
      return;
    }

    const loading = await this.loadingController.create();
    loading.present();

    this.connectionReqSub = this.schedulerService
      .updateScheduler({
        ...this.selectedScheduler,
        ...{
          [key]: isSent,
          date: momentTZ(new Date()).format('YYYY-MM-DD HH:mm:ss'),
        },
      })
      .subscribe(
        () => {
          loading.dismiss();
          this.toaster.showSuccessToast('Success', 'Saved!');
          this.onRefresh.emit();
          this.connectionReqSub.unsubscribe();
        },
        (error) => {
          loading.dismiss();
          this.connectionReqSub.unsubscribe();

          if (error) {
            const err = error.error;
            if (err && err.message) {
              this.toaster.showErrorToast('WARNING!', err.message);
            }
          }
        }
      );
  }

  private async onNeutralPositive(date: string) {
    const loading = await this.loadingController.create();
    loading.present();

    this.connectionReqSub = this.schedulerService
      .updateScheduler({
        ...this.selectedScheduler,
        ...{
          neutral: date,
        },
      })
      .subscribe(
        () => {
          loading.dismiss();
          this.toaster.showSuccessToast('Success', 'Saved!');
          this.onRefresh.emit();
          this.connectionReqSub.unsubscribe();
        },
        (error) => {
          loading.dismiss();
          this.connectionReqSub.unsubscribe();

          if (error) {
            const err = error.error;
            if (err && err.message) {
              this.toaster.showErrorToast('WARNING!', err.message);
            }
          }
        }
      );
  }

  private async onNegativeNoFollowUp(date: string) {
    const loading = await this.loadingController.create();
    loading.present();

    this.connectionReqSub = this.schedulerService
      .updateScheduler({
        ...this.selectedScheduler,
        ...{
          negative: date,
        },
      })
      .subscribe(
        () => {
          loading.dismiss();
          this.toaster.showSuccessToast('Success', 'Saved!');
          this.onRefresh.emit();
          this.connectionReqSub.unsubscribe();
        },
        (error) => {
          loading.dismiss();
          this.connectionReqSub.unsubscribe();

          if (error) {
            const err = error.error;
            if (err && err.message) {
              this.toaster.showErrorToast('WARNING!', err.message);
            }
          }
        }
      );
  }

  private viewContent(key: string, title: string) {
    if (this.campaign && this.campaign.content) {
      const initialState = {
        content: this.campaign.content[key],
        title,
      };
      this.modalService.show(ContentModalComponent, { initialState });
    } else {
      return this.toaster.showWarningToast(
        'WARNING',
        'No campaign content! Please contact the administration.'
      );
    }
  }
}
