import { ICampaignScheduler } from './../../models/scheduler.model';
import { mainSelector } from './../../store/main.selectors';
import {
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { NbSidebarService } from '@nebular/theme';
import { select, Store } from '@ngrx/store';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { Subject } from 'rxjs';
import { map, take, takeUntil } from 'rxjs/operators';
import { IUserProfile } from 'src/app/modules/auth/models/auth.model';
import { AuthState } from 'src/app/modules/auth/store/auth.reducers';
import { profileSelector } from 'src/app/modules/auth/store/auth.selectors';
import {
  ACTIVE_CAMPAIGN,
  MAX_ITEMS_PER_PAGE,
  USER_ROLE,
} from 'src/app/shared/constants/utils';
import { DataLoaderService } from 'src/app/shared/services/data-loader.service';
import { PubsubService } from 'src/app/shared/services/pubsub.service';

import { ICampaign } from '../../models/campaign.model';
import { IGenericParams } from '../../models/generic.model';

import { IScheduler } from '../../models/scheduler.model';
import { SchedulerService } from '../../services/scheduler.service';
import { MainState } from '../../store/main.reducers';

@Component({
  selector: 'app-scheduler',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './scheduler.component.html',
  styleUrls: ['./scheduler.component.scss'],
})
export class SchedulerComponent implements OnInit, OnDestroy {
  profile: IUserProfile;
  campaign: ICampaign;

  schedulerArr: any[] = [];
  unsubscribe$ = new Subject<any>();
  unsubscribe2$ = new Subject<any>();
  unsubscribe3$ = new Subject<any>();

  itemsPerPage = MAX_ITEMS_PER_PAGE;
  totalItems: number;
  currentPage = 1;
  searchText: string;

  showContent = false;
  isSearching = false;

  curDate: Date = new Date();

  constructor(
    private authstore: Store<AuthState>,
    private mainstore: Store<MainState>,
    private schedulerService: SchedulerService,
    private dataLoader: DataLoaderService,
    private sidebarService: NbSidebarService,
    private pubsub: PubsubService,
    private loadingController: LoadingController
  ) {}

  ngOnInit(): void {
    this.initProfile();
    this.initData();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();

    this.unsubscribe3$.next();
    this.unsubscribe3$.complete();
  }

  toggleSidebar(): boolean {
    this.pubsub.$pub('settings-sidebar', 'campaign');
    this.sidebarService.toggle(false, 'settings-sidebar');
    return false;
  }

  async pageChanged(event: PageChangedEvent) {
    const { page } = event;
    this.showContent = false;

    const loading = await this.loadingController.create();
    loading.present();

    if (this.searchText) {
      this.initSchedulerData(page, this.searchText, loading);
    } else {
      this.initSchedulerData(page, null, loading);
    }
  }

  async search(event: any) {
    const val = event.target.value;

    const loading = await this.loadingController.create();
    loading.present();

    this.initSchedulerData(1, val, loading);
  }

  clearSearch() {
    this.isSearching = false;
    this.searchText = '';
    this.initSchedulerData();
  }

  private initProfile() {
    this.authstore
      .select(profileSelector)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((response) => {
        if (response && response.profile) {
          this.profile = response.profile;
        }
      });
  }

  private async initData() {
    const loading = await this.loadingController.create();
    loading.present();

    this.mainstore
      .pipe(
        select(mainSelector),
        map((state) => state.scheduler),
        takeUntil(this.unsubscribe3$)
      )
      .subscribe(
        (response: ICampaignScheduler) => {
          if (response && response.schedulers) {
            const resp = response.schedulers;
            this.schedulerArr = resp.data.map((item) => {
              return {
                ...item,
                ...{
                  role: USER_ROLE.SDR,
                  campaign: this.campaign,
                },
              };
            });
            this.totalItems = resp.count;
            this.schedulerService.activeScheduler = resp.data;
            this.showContent = true;
            loading.dismiss();
          }

          if (response && response.campaign) {
            this.campaign = response.campaign;
            this.schedulerService.activeCampaign = response.campaign;
            this.pubsub.$pub(ACTIVE_CAMPAIGN, this.campaign);
          }
        },
        () => {
          loading.dismiss();
        }
      );
  }

  initSchedulerData(page: number = 1, Search?: string, event?: any) {
    const params: IGenericParams = {
      PageIndex: page,
      PageSize: this.itemsPerPage,
    };

    if (Search) {
      params.Search = Search;
      this.searchText = Search;
      this.isSearching = true;
      this.currentPage = 1;
    }

    this.dataLoader.getActiveScheduler(params).then(
      (response) => {
        if (response) {
          this.showContent = true;
          if (event) {
            event.dismiss();
          }
        }
      },
      () => {
        this.showContent = true;
        if (event) {
          event.dismiss();
        }
      }
    );
  }
}
