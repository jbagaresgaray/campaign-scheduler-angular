import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbContextMenuModule,
  NbDatepickerModule,
  NbIconModule,
  NbInputModule,
  NbSelectModule,
  NbTabsetModule,
  NbTooltipModule,
} from '@nebular/theme';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { ComponentsModule } from 'src/app/shared/components/components.module';
import { PipesModule } from 'src/app/shared/pipes/pipes.module';

import { SchedulerRoutingModule } from './scheduler-routing.module';
import { SchedulerComponent } from './scheduler.component';
import { SchedulerEntryComponent } from './scheduler-entry/scheduler-entry.component';

import { MocksModule } from 'src/app/shared/mocks/mocks.module';
import { SchedulerComponentsModule } from './components/scheduler-components.module';

@NgModule({
  declarations: [SchedulerComponent, SchedulerEntryComponent],
  exports: [SchedulerComponent],
  imports: [
    CommonModule,
    SchedulerRoutingModule,
    SchedulerComponentsModule,
    ComponentsModule,
    MocksModule,
    ReactiveFormsModule,
    FormsModule,
    NbIconModule,
    NbCardModule,
    NbTooltipModule,
    NbButtonModule,
    NbCheckboxModule,
    NbInputModule,
    NbDatepickerModule,
    NbSelectModule,
    NbTabsetModule,
    NbContextMenuModule,
    IonicModule,
    PipesModule,
  ],
})
export class SchedulerModule {}
