import { MAX_ITEMS_PER_PAGE } from './../../../shared/constants/utils';
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import {
  tap,
  switchMap,
  withLatestFrom,
  catchError,
  map,
} from 'rxjs/operators';
import { Store } from '@ngrx/store';

import { MainState } from './main.reducers';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import {
  MainActionTypes,
  UserAccountsAction,
  DepartmentsAction,
  EmployeeSDRAction,
  EmployeeManagerAction,
  EmployeeAdminAction,
  CampaignsAction,
  EmployeeAllStaffsAction,
  SchedulerAction,
} from './main.actions';
import {
  ADMIN_USER_ACCOUNTS,
  ADMIN_DEPARTMENTS,
  ADMIN_EMPLOYEES,
  ADMIN_MANAGERS,
  ADMIN_ADMINS,
  ADMIN_CAMPAIGNS,
  ADMIN_STAFFS,
} from 'src/app/shared/constants/utils';
import { mainSelector } from './main.selectors';
import { DataLoaderService } from 'src/app/shared/services/data-loader.service';
import { ICampaignScheduler } from '../models/scheduler.model';
import { SchedulerService } from '../services/scheduler.service';

@Injectable()
export class MainEffects {
  constructor(
    private store: Store<MainState>,
    private actions$: Actions,
    private localStorage: LocalStorageService,
    private schedulerService: SchedulerService,
    private dataLoader: DataLoaderService
  ) {}

  @Effect({ dispatch: false })
  public userAccountsEffects$ = this.actions$.pipe(
    ofType<UserAccountsAction>(MainActionTypes.userAccountsAction),
    tap((action) => {
      this.localStorage.setItem(
        ADMIN_USER_ACCOUNTS,
        action.payload.userAccounts
      );
    })
  );

  @Effect({ dispatch: false })
  public departmentsEffects$ = this.actions$.pipe(
    ofType<DepartmentsAction>(MainActionTypes.departmentsAction),
    tap((action) => {
      this.localStorage.setItem(ADMIN_DEPARTMENTS, action.payload.departments);
    })
  );

  @Effect({ dispatch: false })
  public employeeSDREffects$ = this.actions$.pipe(
    ofType<EmployeeSDRAction>(MainActionTypes.employeeSDRAction),
    tap((action) => {
      this.localStorage.setItem(ADMIN_EMPLOYEES, action.payload.employeeSDR);
    })
  );

  @Effect({ dispatch: false })
  public employeeManagerEffects$ = this.actions$.pipe(
    ofType<EmployeeManagerAction>(MainActionTypes.employeeManagerAction),
    tap((action) => {
      this.localStorage.setItem(ADMIN_MANAGERS, action.payload.employeeMgr);
    })
  );

  @Effect({ dispatch: false })
  public employeeAdminEffects$ = this.actions$.pipe(
    ofType<EmployeeAdminAction>(MainActionTypes.employeeAdminsAction),
    tap((action) => {
      this.localStorage.setItem(ADMIN_ADMINS, action.payload.employeeAdmin);
    })
  );

  @Effect({ dispatch: false })
  public employeeAllStaffsEffects$ = this.actions$.pipe(
    ofType<EmployeeAllStaffsAction>(MainActionTypes.employeeStaffsAction),
    tap((action) => {
      this.localStorage.setItem(ADMIN_STAFFS, action.payload.employeeAllStaffs);
    })
  );

  @Effect({ dispatch: false })
  public campaignsEffect$ = this.actions$.pipe(
    ofType<CampaignsAction>(MainActionTypes.campaignsAction),
    tap((action) => {
      this.localStorage.setItem(ADMIN_CAMPAIGNS, action.payload.campaigns);
    })
  );
}
