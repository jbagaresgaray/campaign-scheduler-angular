import {
  ISDRMGroupingResponse,
  IUserAccountsResponse,
} from './../models/users.model';
import { Action } from '@ngrx/store';
import { IDepartmentResponse } from '../models/department.model';
import { IEmployeesResponse } from '../models/employee.model';
import { ICampaignResponse } from '../models/campaign.model';
import { ICampaignScheduler, IScheduler } from '../models/scheduler.model';

export enum MainActionTypes {
  userAccountsAction = '[main] userAccounts',
  departmentsAction = '[main] departments',
  employeeSDRAction = '[users] employee-sdr',
  employeeManagerAction = '[users] employee-manager',
  employeeAdminsAction = '[users] employee-admins',
  employeeStaffsAction = '[users] employee-all-staff',
  campaignsAction = '[main] campaigns',
  schedulerAction = '[sdr] scheduler',
  sdrmGroupingsAction = '[sdrm] groupings',
}

export class UserAccountsAction implements Action {
  readonly type = MainActionTypes.userAccountsAction;

  constructor(public payload: { userAccounts: IUserAccountsResponse }) {}
}

export class DepartmentsAction implements Action {
  readonly type = MainActionTypes.departmentsAction;

  constructor(public payload: { departments: IDepartmentResponse }) {}
}

export class EmployeeSDRAction implements Action {
  readonly type = MainActionTypes.employeeSDRAction;

  constructor(public payload: { employeeSDR: IEmployeesResponse }) {}
}

export class EmployeeManagerAction implements Action {
  readonly type = MainActionTypes.employeeManagerAction;

  constructor(public payload: { employeeMgr: IEmployeesResponse }) {}
}

export class EmployeeAdminAction implements Action {
  readonly type = MainActionTypes.employeeAdminsAction;

  constructor(public payload: { employeeAdmin: IEmployeesResponse }) {}
}

export class EmployeeAllStaffsAction implements Action {
  readonly type = MainActionTypes.employeeStaffsAction;

  constructor(public payload: { employeeAllStaffs: IEmployeesResponse }) {}
}

export class CampaignsAction implements Action {
  readonly type = MainActionTypes.campaignsAction;

  constructor(public payload: { campaigns: ICampaignResponse }) {}
}

export class SchedulerAction implements Action {
  readonly type = MainActionTypes.schedulerAction;

  constructor(public payload: { scheduler: ICampaignScheduler }) {}
}

export class SDRMGroupingsAction implements Action {
  readonly type = MainActionTypes.sdrmGroupingsAction;

  constructor(public payload: { sdrmGroupings: ISDRMGroupingResponse }) {}
}

export type MainActions =
  | UserAccountsAction
  | DepartmentsAction
  | EmployeeSDRAction
  | EmployeeManagerAction
  | EmployeeAdminAction
  | EmployeeAllStaffsAction
  | CampaignsAction
  | SchedulerAction
  | SDRMGroupingsAction;
