import { MainActionTypes, MainActions } from './main.actions';
import {
  ISDRMGroupingResponse,
  IUserAccountsResponse,
} from './../models/users.model';
import { IDepartmentResponse } from '../models/department.model';
import { IEmployeesResponse } from '../models/employee.model';
import { ICampaignResponse } from '../models/campaign.model';
import { ICampaignScheduler } from '../models/scheduler.model';

export interface MainState {
  userAccounts?: IUserAccountsResponse;
  departments?: IDepartmentResponse;
  employeeSDR?: IEmployeesResponse;
  employeeMgr?: IEmployeesResponse;
  employeeAdmin?: IEmployeesResponse;
  employeeAllStaffs?: IEmployeesResponse;
  campaigns?: ICampaignResponse;
  scheduler?: ICampaignScheduler;
  sdrmGroupings?: ISDRMGroupingResponse;
}

export const initialMainState: MainState = {
  userAccounts: undefined,
  departments: undefined,
  employeeSDR: undefined,
  employeeMgr: undefined,
  employeeAdmin: undefined,
  employeeAllStaffs: undefined,
  campaigns: undefined,
  scheduler: undefined,
  sdrmGroupings: undefined,
};

export function mainReducer(
  // tslint:disable-next-line:no-shadowed-variable
  MainState = initialMainState,
  action: MainActions
): MainState {
  switch (action.type) {
    case MainActionTypes.userAccountsAction:
      return Object.assign({}, MainState, {
        userAccounts: action.payload.userAccounts,
      });

    case MainActionTypes.departmentsAction:
      return Object.assign({}, MainState, {
        departments: action.payload.departments,
      });

    case MainActionTypes.employeeSDRAction:
      return Object.assign({}, MainState, {
        employeeSDR: action.payload.employeeSDR,
      });

    case MainActionTypes.employeeManagerAction:
      return Object.assign({}, MainState, {
        employeeMgr: action.payload.employeeMgr,
      });

    case MainActionTypes.employeeAdminsAction:
      return Object.assign({}, MainState, {
        employeeAdmin: action.payload.employeeAdmin,
      });

    case MainActionTypes.employeeStaffsAction:
      return Object.assign({}, MainState, {
        employeeAllStaffs: action.payload.employeeAllStaffs,
      });

    case MainActionTypes.campaignsAction:
      return Object.assign({}, MainState, {
        campaigns: action.payload.campaigns,
      });

    case MainActionTypes.schedulerAction:
      return Object.assign({}, MainState, {
        scheduler: action.payload.scheduler,
      });

    case MainActionTypes.sdrmGroupingsAction:
      return Object.assign({}, MainState, {
        sdrmGroupings: action.payload.sdrmGroupings,
      });

    default:
      return MainState;
  }
}
