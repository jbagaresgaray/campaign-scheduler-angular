import { createSelector } from '@ngrx/store';

export const mainSelectorState = (state) => state.main;

export const mainSelector = createSelector(
  mainSelectorState,
  (userAccounts) => userAccounts,
  (departments) => departments,
  (employeeSDR) => employeeSDR,
  (employeeMgr) => employeeMgr,
  (employeeAdmin) => employeeAdmin,
  (employeeAllStaffs) => employeeAllStaffs,
  (campaigns) => campaigns,
  (scheduler) => scheduler
);

export const sdrmGroupingSelector = createSelector(
  mainSelectorState,
  (state) => state.sdrmGroupings
);
