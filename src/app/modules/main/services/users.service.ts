import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseService } from 'src/app/shared/services/base.service';
import { EUserAccountStatus } from 'src/app/shared/constants/enums';
import {
  ISDRMGrouping,
  ISDRMGroupingResponse,
  IUserAccountsResponse,
} from '../models/users.model';
import { IGenericParams } from '../models/generic.model';
import { IEmployee, IEmployeesResponse } from '../models/employee.model';
import { APIS } from 'src/app/shared/constants/utils';

interface IUpdateUserStatus {
  statusId: EUserAccountStatus;
  userId: string;
}

interface IUserAssignDepartment {
  userProfileId: string;
  departmentId: number;
}

@Injectable({
  providedIn: 'root',
})
export class UsersService extends BaseService {
  constructor(http: HttpClient) {
    super(http);
  }

  getAllUsers(params?: IGenericParams): Observable<IUserAccountsResponse> {
    return this.getParams(APIS.USERS, '/useraccounts', params);
  }

  updateUserStatus({ userId, statusId }: IUpdateUserStatus): Observable<any> {
    return this.put(APIS.USERS, '/useraccounts/status/update', {
      userId,
      statusId,
    });
  }

  getStaffDetails(id: number): Observable<IEmployee> {
    return this.get(APIS.USERS, `/userprofiles/${id}`);
  }

  updateUserProfile(data: IEmployee): Observable<any> {
    return this.put(APIS.USERS, `/userprofiles`, data);
  }

  createSDRGroupings(data: ISDRMGrouping): Observable<any> {
    return this.post(APIS.USERS, '/sdrgroupings', data);
  }

  getAllSDRGroupings(
    id: number,
    params?: IGenericParams
  ): Observable<ISDRMGroupingResponse> {
    return this.getParams(APIS.USERS, `/sdrgroupings/managerid=${id}`, params);
  }

  // ===========================================================
  //                      ALL STAFFS
  // ===========================================================

  getAllStaffs(params?: IGenericParams): Observable<IEmployeesResponse> {
    return this.getParams(APIS.USERS, '/userprofiles', params);
  }

  // ===========================================================
  //                      EMPLOYEES
  // ===========================================================

  getAllEmployees(params?: IGenericParams): Observable<IEmployeesResponse> {
    return this.getParams(APIS.USERS, '/userprofiles/sdr', params);
  }

  getAllEmployeesByDepartment(
    departmentId: string,
    params?: IGenericParams
  ): Observable<any> {
    return this.getParams(
      APIS.USERS,
      '/userprofiles/departmentid=' + departmentId,
      params
    );
  }

  assignEmployeeToDepartment({
    userProfileId,
    departmentId,
  }: IUserAssignDepartment): Observable<any> {
    return this.put(APIS.USERS, '/userprofiles/assign/department', {
      userProfileId,
      departmentId,
    });
  }

  // ===========================================================
  //                      MANAGERS
  // ===========================================================

  getAllManagers(params?: IGenericParams): Observable<IEmployeesResponse> {
    return this.getParams(APIS.USERS, '/userprofiles/manager', params);
  }

  // ===========================================================
  //                      ADMINS
  // ===========================================================

  getAllAdmins(params?: IGenericParams): Observable<IEmployeesResponse> {
    return this.getParams(APIS.USERS, '/userprofiles/admin', params);
  }
}
