import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseService } from 'src/app/shared/services/base.service';
import { IGenericParams } from '../models/generic.model';
import { IDepartment } from '../models/department.model';
import { APIS } from 'src/app/shared/constants/utils';
import {
  IAssignCampaign,
  ICampaign,
  ICampaignContent,
  ICampaignSDRStatus,
  ICampaignStatus,
} from '../models/campaign.model';

@Injectable({
  providedIn: 'root',
})
export class MainService extends BaseService {
  constructor(http: HttpClient) {
    super(http);
  }

  private queryString(params) {
    return Object.keys(params)
      .map((key) => key + '=' + params[key])
      .join('&');
  }

  // ===========================================================
  //                      DEPARTMENT
  // ===========================================================

  getAllDepartments(params?: IGenericParams): Observable<any> {
    return this.getParams(APIS.CORE, '/departments', params);
  }

  createDepartment(data: IDepartment): Observable<any> {
    return this.post(APIS.CORE, '/departments', data);
  }

  getDepartment(id: string): Observable<any> {
    return this.get(APIS.CORE, '/departments/' + id);
  }

  deleteDepartment(id: string): Observable<any> {
    return this.delete(APIS.CORE, '/departments/' + id);
  }

  updateDepartment(data: IDepartment): Observable<any> {
    return this.put(APIS.CORE, '/departments', data);
  }

  // ===========================================================
  //                      CAMPAIGNS
  // ===========================================================

  createCampaign(data: ICampaign): Observable<any> {
    return this.post(APIS.CORE, '/campaigns', data);
  }

  deleteCampaign(id: number): Observable<any> {
    return this.delete(APIS.CORE, '/campaigns/' + id);
  }

  getCampaign(id: number): Observable<ICampaign> {
    return this.get(APIS.CORE, '/campaigns/' + id);
  }

  updateCampaign(data: ICampaign): Observable<any> {
    return this.put(APIS.CORE, '/campaigns', data);
  }

  assignManagerToCampaign(data: IAssignCampaign): Observable<any> {
    return this.put(APIS.CORE, '/campaigns/assignmanager', data);
  }

  assignSDRToCampaign(data: IAssignCampaign): Observable<any> {
    return this.put(APIS.CORE, '/campaigns/assignsdr', data);
  }

  unAssignSDRToCampaign(data: IAssignCampaign): Observable<any> {
    return this.delete(
      APIS.CORE,
      `/campaigns/unassignsdr/${data.campaignId}/${data.userProfileId}`
    );
  }

  changeCampaignStatus(data: ICampaignStatus): Observable<any> {
    return this.put(APIS.CORE, '/campaigns/statuschange', data);
  }

  changeSDRCampaignStatus(data: ICampaignSDRStatus): Observable<any> {
    return this.put(APIS.CORE, '/campaigns/sdr/statuschange', data);
  }

  getAllCampaignSDR(id: number, params?: IGenericParams): Observable<any> {
    return this.getParams(
      APIS.CORE,
      `/campaigns/assignedsdr/campaignid=${id}`,
      params
    );
  }

  // ===========================================================
  //                      CAMPAIGNS CONTENT
  // ===========================================================

  getCampaignContent(id: number): Observable<any> {
    return this.get(APIS.CORE, `/content/campaignid=${id}`);
  }

  createCampaignContent(data: ICampaignContent): Observable<any> {
    return this.post(APIS.CORE, `/content`, data);
  }

  // ===========================================================
  //                      CAMPAIGNS ADMIN
  // ===========================================================

  getAllCampaigns(params?: IGenericParams): Observable<any> {
    return this.getParams(APIS.CORE, '/campaigns', params);
  }

  // ===========================================================
  //                      CAMPAIGNS SDR
  // ===========================================================

  getAllSDRCampaign(id: number): Observable<any> {
    return this.get(APIS.CORE, `/campaigns/employeeid=${id}`);
  }

  // ===========================================================
  //                      CAMPAIGNS MANAGER
  // ===========================================================

  getAllSDRMCampaign(id: number): Observable<any> {
    return this.get(APIS.CORE, `/campaigns/managerid=${id}`);
  }
}
