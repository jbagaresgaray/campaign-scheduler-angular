import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BaseService } from 'src/app/shared/services/base.service';
import { APIS } from 'src/app/shared/constants/utils';
import { Observable } from 'rxjs';
import { IGenericParams } from '../models/generic.model';
import { ICampaign } from '../models/campaign.model';
import { IScheduler, ISchedulerData } from '../models/scheduler.model';

interface IUploadScheduler {
  UserProfileId: string;
  CampaignId: string;
  CsvData: any;
}

@Injectable({
  providedIn: 'root',
})
export class SchedulerService extends BaseService {
  activeCampaign: ICampaign;
  activeScheduler: IScheduler[] = [];

  constructor(http: HttpClient) {
    super(http);
  }

  uploadCampaignScheduler(data: IUploadScheduler): Observable<any> {
    const formData: FormData = new FormData();
    for (const [key, value] of Object.entries(data)) {
      console.log(`${key}: ${value}`);
      formData.append(key, value);
    }

    return this.upload(APIS.MAIN, '/scheduler/upload', formData);
  }

  getCampaignScheduler(
    campaignId: number,
    userId: number,
    params?: IGenericParams
  ): Observable<any> {
    return this.getParams(
      APIS.MAIN,
      `/scheduler/${campaignId}/${userId}`,
      params
    );
  }

  getActiveSchedulers(params?: IGenericParams): Observable<any> {
    return this.getParams(APIS.MAIN, `/scheduler/currentuser`, params);
  }

  getActiveCampaigns(): Observable<any> {
    return this.get(APIS.CORE, `/campaigns/currentuser`);
  }

  updateScheduler(data: ISchedulerData): Observable<any> {
    return this.put(APIS.MAIN, `/scheduler`, data);
  }
}
