import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { APIS } from 'src/app/shared/constants/utils';
import { BaseService } from 'src/app/shared/services/base.service';

export interface IBlobImage {
  base64String: string;
  imageFileName: string;
}

export interface IBlobImageResponse {
  url: string;
}

@Injectable({
  providedIn: 'root',
})
export class UploadService extends BaseService {
  constructor(http: HttpClient) {
    super(http);
  }

  uploadBlob(data: IBlobImage): Observable<IBlobImageResponse> {
    return this.post(APIS.BLOB, '/azureblob/image', data);
  }

  deleteBlob(data: IBlobImage): Observable<any> {
    return this.delete(APIS.BLOB, '/azureblob/image', data);
  }
}
