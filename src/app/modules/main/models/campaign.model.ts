import { IEmployees } from './employee.model';

export interface ICampaign {
  id: string;
  campaignName: string;
  plan: string;
  client: number;
  started: string;
  statusId?: number;
  clientEmail: string;
  accountsExecutiveId?: string;
  trelloEditorId?: string;
  userProfileId?: number;
  content?: ICampaignContent;
  manager?: IEmployees;
}

export interface ICampaignSDR {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  employeeId: string;
  image?: string;
}

export interface ICampaigns {
  campaignName?: string;
  client?: string;
  id?: number;
  managerId?: number;
  plan?: string;
  sdrCount?: number;
  started?: string;
  statusId?: number;
}

export interface ICampaignResponse {
  count: number;
  data: ICampaigns[];
  pageIndex: number;
  pageSize: number;
}

export interface IAssignCampaign {
  campaignId: number;
  userProfileId: number;
}

export interface ICampaignStatus {
  campaignId: number;
  statusId: number;
}

export interface ICampaignSDRStatus {
  campaignId: number;
  userProfileId: number;
  statusId: number;
}

export interface ICampaignContent {
  message1?: string;
  message2?: string;
  message3?: string;
  message4?: string;
  message5?: string;
  inviteMessage?: string;
  campaignId?: number;
}

export interface ICampaignPricing {
  id?: number;
  name?: string;
  durationId?: number;
  pricing?: number;
}
