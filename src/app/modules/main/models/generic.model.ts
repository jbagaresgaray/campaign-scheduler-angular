export interface IGenericParams {
  PageIndex?: number;
  PageSize?: number;
  Sort?: string;
  Search?: string;
}
