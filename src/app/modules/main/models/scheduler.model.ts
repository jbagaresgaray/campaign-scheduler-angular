import { ICampaign } from './campaign.model';
export interface IScheduler {
  id: string;
  sdrId: string;
  campaignId: string;
  firstName: string;
  lastName: string;
  middleName?: string;
  company?: string;
  jobTitle?: string;
  industry?: string;
  city?: string;
  state?: string;
  linkedInUrl: string;
  connectionRequest: boolean;
  date: string;
  requestAccepted: boolean;
  isMessage1?: boolean;
  dateMessage1?: string;
  isMessage2?: boolean;
  dateMessage2?: string;
  isMessage3?: boolean;
  dateMessage3?: string;
  isMessage4?: boolean;
  dateMessage4?: string;
  isMessage5?: boolean;
  dateMessage5?: string;
  neutral?: string;
  negative?: string;
}

export interface ISchedulerResponse {
  count: number;
  data: IScheduler[];
  pageIndex: number;
  pageSize: number;
}

export interface ICampaignScheduler {
  campaign: ICampaign;
  schedulers: ISchedulerResponse;
}

export interface ISchedulerData {
  id: number;
  firstName?: string;
  lastName?: string;
  middleName?: string;
  jobTitle?: string;
  company?: string;
  industry?: string;
  city?: string;
  state?: string;
  linkedInUrl?: string;
  connectionRequest?: boolean;
  date?: string;
  requestAccepted?: boolean;
  isMessage1?: boolean;
  dateMessage1?: string;
  isMessage2?: boolean;
  dateMessage2?: string;
  isMessage3?: boolean;
  dateMessage3?: string;
  isMessage4?: boolean;
  dateMessage4?: string;
  isMessage5?: boolean;
  dateMessage5?: string;
  neutral?: string;
  negative?: string;
}
