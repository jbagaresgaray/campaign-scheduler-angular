export interface IDepartment {
  id: string;
  name: string;
  shortName: string;
  description?: string;
  statusId?: number;
}


export interface IDepartmentResponse {
  count: number;
  data: IDepartment[];
  pageIndex: number;
  pageSize: number;
}


