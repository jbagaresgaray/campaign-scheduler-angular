export interface IEmployee {
  id: string;
  firstName: string;
  lastName: string;
  middleName?: string;
  email: string;
  linkedInName: string;
  linkedInUrl: string;
  dateHired: string;
  departmentId?: number;
  address?: string;
  state?: string;
  postCode?: string;
  country?: string;
  dateCreated: string;
  userId?: string;
  imageUrl?: string;
}

export interface IEmployees {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  imageUrl?: string;
  statusId?: string;
  departmentId?: number;
  roledId?: number;

  selected?: boolean;
  loading?: boolean;
}

export interface IEmployeesResponse {
  count: number;
  data: IEmployees[];
  pageIndex: number;
  pageSize: number;
}
