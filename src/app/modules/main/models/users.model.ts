import { EUserAccountStatus } from 'src/app/shared/constants/enums';
import { IEmployees, IEmployeesResponse } from './employee.model';
export interface IUserAccounts {
  id?: string;
  email?: string;
  firstName?: string;
  lastName?: string;
  statusId?: EUserAccountStatus;
}

export interface IUserAccountsResponse {
  count: number;
  data: IUserAccounts[];
  pageIndex: number;
  pageSize: number;
}

export interface ISDRMGrouping {
  managerEmployeeId: number;
  sdrEmployeeId: number;
}

export interface ISDRMGroupingResponse {
  manager: IEmployees;
  sdrs: IEmployeesResponse;
}
