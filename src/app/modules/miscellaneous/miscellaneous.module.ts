import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbButtonModule, NbCardModule } from '@nebular/theme';

import { MiscellaneousRoutingModule } from './miscellaneous-routing.module';
import { MiscellaneousComponent } from './miscellaneous.component';
import { NotFoundComponent } from './not-found/not-found.component';

@NgModule({
  declarations: [MiscellaneousComponent, NotFoundComponent],
  imports: [
    CommonModule,
    NbCardModule,
    NbButtonModule,
    MiscellaneousRoutingModule,
  ],
})
export class MiscellaneousModule {}
