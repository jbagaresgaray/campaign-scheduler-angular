import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

import { AuthComponent } from './modules/auth/components/auth.component';
import { NotFoundComponent } from './modules/miscellaneous/not-found/not-found.component';
import { AuthGuard } from './shared/guards/auth.guard';

const routes: Routes = [
  {
    path: 'main',
    loadChildren: () =>
      import('./modules/main/components/main.module').then((m) => m.MainModule),
  },
  {
    path: '',
    component: AuthComponent,
    // canActivate: [AuthGuard],
    children: [
      {
        path: 'login',
        loadChildren: () =>
          import('./modules/auth/components/login/login.module').then(
            (m) => m.LoginModule
          ),
      },
      {
        path: 'forgot-password',
        loadChildren: () =>
          import(
            './modules/auth/components/forgot-password/forgot-password.module'
          ).then((m) => m.ForgotPasswordModule),
      },
      {
        path: 'register',
        loadChildren: () =>
          import('./modules/auth/components/register/register.module').then(
            (m) => m.RegisterModule
          ),
      },
      {
        path: 'reset-password',
        loadChildren: () =>
          import(
            './modules/auth/components/reset-password/reset-password.module'
          ).then((m) => m.ResetPasswordModule),
      },
      {
        path: 'thank-you',
        loadChildren: () =>
          import(
            './modules/auth/components/register-thank-you/register-thank-you.module'
          ).then((m) => m.RegisterThankYouModule),
      },
      {
        path: '',
        redirectTo: '/login',
        pathMatch: 'full',
      },
      {
        path: '**',
        component: NotFoundComponent,
      },
    ],
  },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: '**', redirectTo: '/login' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
