import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { tap } from 'rxjs/operators';

import { AuthState } from '../../modules/auth/store/auth.reducers';
import { isLoggedIn } from '../../modules/auth/store/auth.selectors';

@Injectable({
  providedIn: 'root',
})
export class MainGuard implements CanActivate {
  constructor(private store: Store<AuthState>) {
    console.log('MainGuard: ');
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return this.store.pipe(
      select(isLoggedIn),
      // tslint:disable-next-line: no-shadowed-variable
      tap((isLoggedIn) => {
        if (!isLoggedIn) {
          location.replace('/login');
        }
      })
    );
  }
}
