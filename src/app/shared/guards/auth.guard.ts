import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { AuthState } from '../../modules/auth/store/auth.reducers';
import {
  isLoggedIn,
  isLoggedOut,
} from '../../modules/auth/store/auth.selectors';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(private store: Store<AuthState>, private router: Router) {
    console.log('AuthGuard: ');
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return this.store.pipe(
      select(isLoggedOut),
      // tslint:disable-next-line: no-shadowed-variable
      tap((isLoggedOut) => {
        if (isLoggedOut) {
          return true;
        } else {
          this.router.navigateByUrl('/login');
        }
      })
    );
  }
}
