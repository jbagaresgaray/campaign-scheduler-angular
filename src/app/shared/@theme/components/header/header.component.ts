import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  NbMediaBreakpointsService,
  NbMenuItem,
  NbMenuService,
  NbSidebarService,
  NbThemeService,
} from '@nebular/theme';

import { filter, map, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';

import upperCase from 'lodash-es/upperCase';
import uniqBy from 'lodash-es/uniqBy';

import { AuthState } from 'src/app/modules/auth/store/auth.reducers';
import { Logout } from 'src/app/modules/auth/store/auth.actions';
import { IUserProfile } from 'src/app/modules/auth/models/auth.model';
import { profileSelector } from 'src/app/modules/auth/store/auth.selectors';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  private destroy$: Subject<void> = new Subject<void>();
  userPictureOnly = false;
  user: IUserProfile;

  themes = [
    {
      value: 'default',
      name: 'Light',
    },
    {
      value: 'corporate',
      name: 'Corporate',
    },
  ];

  currentTheme = 'corporate';

  userMenu: NbMenuItem[] = [
    { title: 'Change Password', data: 'change-password' },
    { title: 'Log out', data: 'logout' },
  ];

  constructor(
    private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    private themeService: NbThemeService,
    private breakpointService: NbMediaBreakpointsService,
    private router: Router,
    private store: Store<AuthState>
  ) {}

  ngOnInit() {
    this.currentTheme = this.themeService.currentTheme;
    let userMenu: any[] = [...this.userMenu];

    this.store
      .select(profileSelector)
      .pipe(takeUntil(this.destroy$))
      .subscribe((user: any) => {
        if (user && user.profile) {
          this.user = user.profile;
          userMenu = [
            { title: upperCase(this.user.role) + ' Profile', data: 'profile' },
            ...this.userMenu,
          ];
          this.userMenu = uniqBy(userMenu, 'data');
        }
      });

    const { xl } = this.breakpointService.getBreakpointsMap();

    this.themeService
      .onMediaQueryChange()
      .pipe(
        map(([, currentBreakpoint]) => currentBreakpoint.width < xl),
        takeUntil(this.destroy$)
      )
      .subscribe(
        (isLessThanXl: boolean) => (this.userPictureOnly = isLessThanXl)
      );

    this.menuService
      .onItemClick()
      .pipe(
        filter(({ tag }) => tag === 'header-context-menu'),
        map(({ item: { data } }) => data)
      )
      .subscribe((action) => {
        console.log('action: ', action);
        switch (action) {
          case 'profile':
            return this.openProfile();
          case 'logout':
            return this.logout();
          case 'change-password':
            this.router.navigate(['/main/change-password']);
        }
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  changeTheme(themeName: string) {
    this.themeService.changeTheme(themeName);
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    return false;
  }

  navigateHome() {
    this.menuService.navigateHome();
    return false;
  }

  private logout() {
    this.store.dispatch(new Logout());
  }

  private openProfile() {
    this.router.navigateByUrl('/main/user-profile');
  }
}
