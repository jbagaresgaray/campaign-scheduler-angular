import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../../environments/environment';
import { CURRENT_USER } from '../constants/utils';

export abstract class BaseService {
  constructor(private http: HttpClient) {}

  protected getAPIBase(apiUrl: string, route: string = ''): string {
    return environment[apiUrl] + route;
  }

  private getToken(): string {
    const user = localStorage.getItem(CURRENT_USER);
    if (user) {
      if (user !== 'undefined') {
        const parseUser = JSON.parse(user);
        if (parseUser) {
          const token = parseUser.token || parseUser.Token;
          return token;
        }
        return '';
      } else {
        return '';
      }
    } else {
      return '';
    }
  }

  protected commonStateChangeHeaders(): HttpHeaders {
    return new HttpHeaders({
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `Bearer ${this.getToken()}`,
    });
  }

  protected get(API: string, route: string): Observable<any> {
    const url = this.getAPIBase(API, route);
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.getToken()}`,
      Accept: 'application/json',
    });
    const options = { headers };
    return this.http.get(url, options);
  }

  protected getParams(
    API: string,
    route: string,
    params: any
  ): Observable<any> {
    const url = this.getAPIBase(API, route);
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.getToken()}`,
      Accept: 'application/json',
    });
    const options = { headers, params };
    return this.http.get(url, options);
  }

  protected post(API: string, route: string, object?: any): Observable<any> {
    return this.http.post(this.getAPIBase(API, route), object, {
      headers: this.commonStateChangeHeaders(),
    });
  }

  protected postToParams(
    API: string,
    route: string,
    params: any
  ): Observable<any> {
    return this.http.post(
      this.getAPIBase(API, route),
      {},
      { headers: this.commonStateChangeHeaders(), params }
    );
  }

  protected upload(API: string, route: string, object: any): Observable<any> {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.getToken()}`,
      Accept: 'application/json',
    });
    headers.set('Content-Type', 'multipart/form-data');

    return this.http.post(this.getAPIBase(API, route), object, {
      headers,
      reportProgress: true,
      observe: 'events',
    });
  }

  protected delete(API: string, route: string, object?: any): Observable<any> {
    return this.http.delete(this.getAPIBase(API, route), {
      headers: this.commonStateChangeHeaders(),
      ...object,
    });
  }

  protected put(API: string, route: string, object: any): Observable<any> {
    return this.http.put(this.getAPIBase(API, route), object, {
      headers: this.commonStateChangeHeaders(),
    });
  }

  protected patch(API: string, route: string, object: any): Observable<any> {
    return this.http.patch(this.getAPIBase(API, route), object, {
      headers: this.commonStateChangeHeaders(),
    });
  }

  public handleError = (error: any) => {};
}
