import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { BaseService } from './base.service';
import { IRefreshToken } from 'src/app/modules/auth/models/auth.model';
import { APIS } from './../constants/utils';

@Injectable({
  providedIn: 'root',
})
export class RefreshService extends BaseService {
  constructor(http: HttpClient) {
    super(http);
  }

  refreshToken(token: IRefreshToken): Observable<any> {
    return this.post(APIS.AUTH, '/accounts/token/refresh', token);
  }
}
