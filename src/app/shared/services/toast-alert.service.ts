import { Injectable } from '@angular/core';
import {
  NbComponentStatus,
  NbGlobalPhysicalPosition,
  NbToastrService,
} from '@nebular/theme';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root',
})
export class ToastAlertService {
  constructor(private toastrService: NbToastrService) {}

  showAlert(message, icon) {
    return Swal.fire(message, '', icon);
  }

  showErrorAlert(message) {
    return Swal.fire(message, '', 'error');
  }

  showSuccessAlert(message) {
    return Swal.fire(message, '', 'success');
  }

  showWarningAlert(message) {
    return Swal.fire(message, '', 'warning');
  }

  showInfoAlert(message) {
    return Swal.fire(message, '', 'info');
  }

  showErrorToast(header, message) {
    return this.showToast('danger', header, message);
  }

  showSuccessToast(header, message) {
    return this.showToast('success', header, message);
  }

  showWarningToast(header, message) {
    return this.showToast('warning', header, message);
  }

  showInfoToast(header, message) {
    return this.showToast('info', header, message);
  }

  showBasicToast(header, message) {
    return this.showToast('basic', header, message, false);
  }

  private showToast(
    type: NbComponentStatus,
    title: string,
    body: string,
    hasIcon: boolean = true
  ) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    this.toastrService.show(body, title, config);
  }
}
