import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { map } from 'rxjs/operators';
import { IUserAccount } from 'src/app/modules/auth/models/auth.model';
import { AuthService } from 'src/app/modules/auth/services/auth.service';
import { AuthState } from 'src/app/modules/auth/store/auth.reducers';
import { IGenericParams } from 'src/app/modules/main/models/generic.model';
import { MainService } from 'src/app/modules/main/services/main.service';
import { SchedulerService } from 'src/app/modules/main/services/scheduler.service';
import { UsersService } from 'src/app/modules/main/services/users.service';
import {
  CampaignsAction,
  DepartmentsAction,
  EmployeeAdminAction,
  EmployeeAllStaffsAction,
  EmployeeManagerAction,
  EmployeeSDRAction,
  SchedulerAction,
  UserAccountsAction,
} from 'src/app/modules/main/store/main.actions';
import { USER_ROLE } from '../constants/utils';

import { ProfileInfoAction } from './../../modules/auth/store/auth.actions';

@Injectable({
  providedIn: 'root',
})
export class DataLoaderService {
  constructor(
    private mainService: MainService,
    private usersService: UsersService,
    private authService: AuthService,
    private schedulerService: SchedulerService,
    private store: Store<AuthState>
  ) {}

  /**
   * refreshProfileData
   * @returns Promise
   */
  refreshProfileData(user?: IUserAccount): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.authService.currentUser().subscribe(
        (response: any) => {
          if (response) {
            this.store.dispatch(
              new ProfileInfoAction({ profileInfo: response, user })
            );
          }
          resolve(true);
        },
        () => {
          resolve(false);
        }
      );
    });
  }

  getAllUsers(params?: IGenericParams): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.usersService.getAllUsers(params).subscribe(
        (response: any) => {
          if (response) {
            this.store.dispatch(
              new UserAccountsAction({ userAccounts: response })
            );
          }
          resolve(true);
        },
        () => {
          resolve(false);
        }
      );
    });
  }

  getAllDepartments(params?: IGenericParams): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.mainService.getAllDepartments(params).subscribe(
        (response: any) => {
          if (response) {
            this.store.dispatch(
              new DepartmentsAction({ departments: response })
            );
          }
          resolve(true);
        },
        () => {
          resolve(false);
        }
      );
    });
  }

  getAllEmployees(params?: IGenericParams): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.usersService.getAllEmployees(params).subscribe(
        (response: any) => {
          if (response) {
            this.store.dispatch(
              new EmployeeSDRAction({ employeeSDR: response })
            );
          }
          resolve(true);
        },
        () => {
          resolve(false);
        }
      );
    });
  }

  getAllManagers(params?: IGenericParams): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.usersService.getAllManagers(params).subscribe(
        (response: any) => {
          if (response) {
            this.store.dispatch(
              new EmployeeManagerAction({ employeeMgr: response })
            );
          }
          resolve(true);
        },
        () => {
          resolve(false);
        }
      );
    });
  }

  getAllAdmins(params?: IGenericParams): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.usersService.getAllAdmins(params).subscribe(
        (response: any) => {
          if (response) {
            this.store.dispatch(
              new EmployeeAdminAction({ employeeAdmin: response })
            );
          }
          resolve(true);
        },
        () => {
          resolve(false);
        }
      );
    });
  }

  getAllStaffs(params?: IGenericParams): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.usersService.getAllStaffs(params).subscribe(
        (response: any) => {
          if (response) {
            this.store.dispatch(
              new EmployeeAllStaffsAction({ employeeAllStaffs: response })
            );
          }
          resolve(true);
        },
        () => {
          resolve(false);
        }
      );
    });
  }

  getAllCampaigns(params?: IGenericParams): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.mainService.getAllCampaigns(params).subscribe(
        (response: any) => {
          if (response) {
            this.store.dispatch(new CampaignsAction({ campaigns: response }));
          }
          resolve(true);
        },
        () => {
          resolve(false);
        }
      );
    });
  }

  getActiveScheduler(params?: IGenericParams): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.schedulerService.getActiveSchedulers(params).subscribe(
        (response) => {
          if (response) {
            this.store.dispatch(new SchedulerAction({ scheduler: response }));
          }
          resolve(true);
        },
        () => {
          resolve(false);
        }
      );
    });
  }
}
