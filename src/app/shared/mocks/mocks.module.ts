import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SchedulerMockService } from './services/scheduler.mock';
import { StatsProgressBarService } from './services/stats-progress.mock';
import { PeriodsService } from './services/periods.mock';
import { UserActivtyService } from './services/user-activty.mock';
import { UserService } from './services/employee.mock';
import { KPIListService } from './services/kpi.mock';

const SERVICES = [
  SchedulerMockService,
  StatsProgressBarService,
  PeriodsService,
  UserActivtyService,
  UserService,
  KPIListService,
];

@NgModule({
  providers: [...SERVICES],
  imports: [CommonModule],
})
export class MocksModule {
  static forRoot(): ModuleWithProviders<MocksModule> {
    return {
      ngModule: MocksModule,
      providers: [...SERVICES],
    };
  }
}
