import { Injectable } from '@angular/core';
import { ELegendItemColor } from '../../constants/enums';
import { KPIListData, KPIList } from '../data/kpi-data';
import { PeriodsService } from './periods.mock';
import { of as observableOf, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class KPIListService extends KPIListData {
  private getRandom = (roundTo: number) => Math.round(Math.random() * roundTo);
  private data = {};

  constructor(private period: PeriodsService) {
    super();
    this.data = {
      week: this.getDataWeek(),
      month: this.getDataMonth(),
      year: this.getDataYear(),
    };
  }

  private getDataWeek(): KPIList[] {
    const getFirstDateInPeriod = () => {
      const weeks = this.period.getWeeks();

      return weeks[weeks.length - 1];
    };

    return this.reduceData(this.period.getWeeks(), getFirstDateInPeriod);
  }

  private getDataMonth(): KPIList[] {
    const getFirstDateInPeriod = () => {
      const months = this.period.getMonths();

      return months[months.length - 1];
    };

    return this.reduceData(this.period.getMonths(), getFirstDateInPeriod);
  }

  private getDataYear(): KPIList[] {
    const getFirstDateInPeriod = () => {
      const years = this.period.getYears();

      return `${parseInt(years[0], 10) - 1}`;
    };

    return this.reduceData(this.period.getYears(), getFirstDateInPeriod);
  }

  private reduceData(
    timePeriods: string[],
    getFirstDateInPeriod: () => string
  ): KPIList[] {
    return timePeriods.reduce((result, timePeriod, index) => {
      const hasResult = result[index - 1];
      const prevDate = hasResult
        ? result[index - 1].comparison.nextDate
        : getFirstDateInPeriod();
      const prevValue = hasResult
        ? result[index - 1].comparison.nextValue
        : this.getRandom(100);
      const nextValue = this.getRandom(100);

      const item = {
        date: timePeriod,
        invites: {
          value: this.getRandom(1000),
          color: ELegendItemColor.PRIMARY,
        },
        connections: {
          value: this.getRandom(1000),
          color: ELegendItemColor.SUCCESS,
        },
        messages: {
          value: this.getRandom(1000),
          color: ELegendItemColor.WARNING,
        },
        total: {
          value: this.getRandom(1000),
          color: ELegendItemColor.DANGER,
        },
        comparison: {
          prevDate,
          prevValue,
          nextDate: timePeriod,
          nextValue,
        },
      };

      return [...result, item];
    }, []);
  }

  getKPIListData(period: string): Observable<KPIList> {
    return observableOf(this.data[period]);
  }
}
