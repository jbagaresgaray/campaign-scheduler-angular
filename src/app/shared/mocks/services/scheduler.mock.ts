import { Injectable } from '@angular/core';
import { of as observableOf, Observable } from 'rxjs';
import { v4 as uuidv4 } from 'uuid';
import {
  uniqueNamesGenerator,
  Config,
  adjectives,
  colors,
  animals,
} from 'unique-names-generator';

import { IScheduler } from 'src/app/modules/main/models/scheduler.model';
import { SchedulerData } from '../data/scheduler.data';

@Injectable()
export class SchedulerMockService extends SchedulerData {
  private SchedulerMockData: IScheduler[] = [];
  private readonly MAXDATA = 3000;

  constructor() {
    super();

    this.initData();
  }

  private initData() {
    const randomName: string = uniqueNamesGenerator({
      dictionaries: [adjectives, colors, animals],
    });

    for (let index = 0; index < this.MAXDATA; index++) {
      const element: IScheduler = {
        id: uuidv4(),
        sdrId: uuidv4(),
        campaignId: '1',
        firstName: randomName,
        lastName: randomName,
        middleName: '',
        company: randomName,
        jobTitle: randomName,
        industry: randomName,
        city: randomName,
        state: randomName,
        linkedInUrl: 'http://ca.linkedin.com/in/linkedinyourname',
        connectionRequest: true,
        date: '',
        requestAccepted: false,
        isMessage1: false,
        dateMessage1: '',
        isMessage2: false,
        dateMessage2: '',
        isMessage3: false,
        dateMessage3: '',
        isMessage4: false,
        dateMessage4: '',
        isMessage5: false,
        dateMessage5: '',
        neutral: '',
        negative: '',
      };
      this.SchedulerMockData.push(element);
    }
  }

  getActiveScheduler(): Observable<IScheduler[]> {
    return observableOf(this.SchedulerMockData);
  }
}
