import { Observable } from 'rxjs';

export interface TrafficBar {
  data: number[];
  labels: string[];
  formatter: string;
}

export interface TrafficList {
  date: string;
  value: number;
  delta: {
    up: boolean;
    value: number;
  };
  comparison: {
    prevDate: string;
    prevValue: number;
    nextDate: string;
    nextValue: number;
  };
}

export abstract class TrafficListData {
  abstract getTrafficListData(period: string): Observable<TrafficList>;
}

export abstract class TrafficBarData {
  abstract getTrafficBarData(period: string): Observable<TrafficBar>;
}
