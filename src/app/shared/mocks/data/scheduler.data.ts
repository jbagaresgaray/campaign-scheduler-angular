import { Observable } from 'rxjs';
import { IScheduler } from 'src/app/modules/main/models/scheduler.model';

export abstract class SchedulerData {
  abstract getActiveScheduler(): Observable<IScheduler[]>;
}
