import { Observable } from 'rxjs';
import { ELegendItemColor } from '../../constants/enums';

export interface KPIList {
  date: string;
  invites: {
    value: number;
    color: ELegendItemColor;
  };
  connections: {
    value: number;
    color: ELegendItemColor;
  };
  messages: {
    value: number;
    color: ELegendItemColor;
  };
  total: {
    value: number;
    color: ELegendItemColor;
  };
  comparison: {
    prevDate: string;
    prevValue: number;
    nextDate: string;
    nextValue: number;
  };
}

export abstract class KPIListData {
  abstract getKPIListData(period: string): Observable<KPIList>;
}
