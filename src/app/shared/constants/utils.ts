export const CURRENT_USER = 'scheduler_user';
export const CURRENT_USER_TOKEN = 'scheduler_current_user_token';
export const CURRENT_USER_PROFILE = 'scheduler_current_user_profile';

export const ADMIN_USER_ACCOUNTS = 'scheduler_admin_user_accounts';
export const ADMIN_DEPARTMENTS = 'scheduler_admin_departments';
export const ADMIN_EMPLOYEES = 'scheduler_admin_employees';
export const ADMIN_MANAGERS = 'scheduler_admin_managers';
export const ADMIN_ADMINS = 'scheduler_admin_admins';
export const ADMIN_STAFFS = 'scheduler_admin_staffs';
export const ADMIN_CAMPAIGNS = 'scheduler_admin_campaigns';

export const ACTIVE_CAMPAIGN = 'active_campaign';
export const ACTIVE_SCHEDULER = 'active_scheduler';

export const IS_USER_PROFILE_LOADED = 'is_scheduler_user_profile_loaded';

export const emailValidationRegex =
  '[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}';
export const emailRegex = '[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}';
export const passwordValidationRegex =
  '^((?=.*[0-9])|(?=.*[!@#$%^&*]))[a-zA-Z0-9!@#$%^&*-_+=()]{8,50}$';

// Regular expression used for basic parsing of the uuid.
export const GUID_UUID_Pattern = /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[0-9a-f]{4}-[0-9a-f]{12}$/i;

export const MAX_ITEMS_PER_PAGE = 25;
export const MAX_PAGE_SIZE = 5;

export const GLOBAL_LOADING_TEMPLATE =
  '<span class="ag-overlay-loading-center"><ion-spinner></ion-spinner> Please wait while data are loading</span>';

export const GLOBAL_AG_GRID_OPTIONS = {
  defaultColDef: {
    sortable: true,
    resizable: true,
    editable: false,
    suppressMovable: true,
    suppressSizeToFit: true,
    wrapText: true,
    autoHeight: true,
  },
  headerHeight: null,
  overlayLoadingTemplate: GLOBAL_LOADING_TEMPLATE,
  suppressNoRowsOverlay: true,
};

export const FORMSTATE = {
  ADD: 'add',
  UPDATE: 'update',
  DELETE: 'delete',
  PATCH: 'patch',
  READ: 'read',
  EDIT: 'edit',
};

export const APIS = {
  AUTH: 'authBaseUrl',
  USERS: 'userBaseUrl',
  CORE: 'coreBaseUrl',
  MAIN: 'mainBaseUrl',
  BLOB: 'blobBaseUrl',
};

export const PERMISSION_ACTION = {
  CREATE: 1,
  UPDATE: 2,
  DELETE: 3,
  READ: 5,
};
export const PERMISSION_ACTION_DESC = {
  CREATE: 'CREATE',
  UPDATE: 'UPDATE',
  DELETE: 'DELETE',
  READ: 'READ',
};

export const RESET_CHANGE_PASSWORD_ACTION = {
  CHANGE: 'change-password',
  RESET: 'reset-passord',
};

export const USER_ROLE = {
  ADMIN: 1,
  SDR: 2,
  MANAGER: 3,
  STAFF: 4,
};

export const USER_ROLE_DESC = {
  ADMIN: 'admin',
  SDR: 'sdr',
  MANAGER: 'manager',
  STAFF: 'staff',
};

export const USER_ACCOUNT_STATUS = {
  ACTIVE: 'Active',
  WAITING: 'Waiting',
  TERMINATED: 'Terminated',
  SUSPENDED: 'Suspended',
};

export const PENDING_APPROVAL_STATUS_ENUM = {
  PENDING: 1,
  APPROVED: 2,
  DENIED: 3,
};

export const USER_ACCOUNT_STATUS_ENUM = {
  ACTIVE: 1,
  WAITING: 2,
  TERMINATED: 3,
  SUSPENDED: 4,
};

export const EMPLOYEE_STATUS = {
  ACTIVE: 'Active',
  TERMINATED: 'Terminated',
};

export const CAMPAIGN_STATUS_ITEM = {
  ACTIVE: 'Active',
  INACTIVE: 'Inactive',
  TERMINATED: 'Terminated',
  ARCHIVED: 'Archived',
};

export const CAMPAIGN_STATUS = {
  ACTIVE: 1,
  INACTIVE: 2,
  TERMINATED: 3,
  ARCHIVED: 4,
};

export const CAMPAIGN_SDR_STATUS = {
  ACTIVE: 'Active',
  INACTIVE: 'Inactive',
  TERMINATED: 'Terminated',
  AVAILABLE: 'Available',
};

export const CAMPAIGN_SDR_STATUS_ENUM = {
  ACTIVE: 1,
  INACTIVE: 2,
  TERMINATED: 3,
  AVAILABLE: 4,
};

export const CAMPAIGN_TYPE_ENUM = {
  GROWTHLEVER: 1,
  CLIENT: 2,
};

export const CAMPAIGN_CONTENT = {
  INVITE_MESSAGE: {
    LABEL: 'Invite Message',
    KEY: 'inviteMessage',
  },
  MESSAGE_1: {
    LABEL: 'Message 1',
    KEY: 'message1',
  },
  MESSAGE_2: {
    LABEL: 'Message 2',
    KEY: 'message2',
  },
  MESSAGE_3: {
    LABEL: 'Message 3',
    KEY: 'message3',
  },
  MESSAGE_4: {
    LABEL: 'Message 4',
    KEY: 'message4',
  },
  MESSAGE_5: {
    LABEL: 'Message 5',
    KEY: 'message5',
  },
};

export const CAMPAIGN_DURATION = {
  MONTHLY: {
    LABEL: 'Monthly',
    KEY: 'monthly',
    VALUE: 1,
  },
  QUARTERLY: {
    LABEL: 'Quarterly',
    KEY: 'quarterly',
    VALUE: 2,
  },
  ANNUAL: {
    LABEL: 'Annual',
    KEY: 'annual',
    VALUE: 3,
  },
};

export const SDR_TYPE = [
  {
    id: 1,
    name: 'Senior SDR',
  },
  {
    id: 2,
    name: 'Associate SDR',
  },
  {
    id: 3,
    name: 'SDR',
  },
];
