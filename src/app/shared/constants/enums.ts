export enum EUserAccountStatus {
  ACTIVE = 1,
  WAITING = 2,
  TERMINATED = 3,
  SUSPENDED = 4,
}

export enum ECampaignStatus {
  ACTIVE = 1,
  INACTIVE = 2,
  TERMINATED = 3,
  ARCHIVED = 4,
}

export enum ELegendItemColor {
  PRIMARY = 'primary',
  SECONDARY = 'secondary',
  SUCCESS = 'success',
  DANGER = 'danger',
  WARNING = 'warning',
  INFO = 'info',
}
