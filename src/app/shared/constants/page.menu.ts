import { NbMenuItem } from '@nebular/theme';
import { USER_ROLE } from './utils';
import lowerCase from 'lodash-es/lowerCase';

export const MENU_ITEM_NAME = {
  DASHBOARD: {
    NAME: 'Dashboard',
    TAG: 'dashboard',
    ACCESS: {
      READ: [USER_ROLE.ADMIN, USER_ROLE.SDR, USER_ROLE.MANAGER],
    },
  },
  CAMPAIGN: {
    NAME: 'Campaign',
    TAG: 'campaign',
    ACCESS: {
      CREATE: [USER_ROLE.ADMIN],
      UPDATE: [USER_ROLE.ADMIN],
      DELETE: [USER_ROLE.ADMIN],
      READ: [USER_ROLE.ADMIN, USER_ROLE.SDR, USER_ROLE.MANAGER],
    },
  },
  SCHEDULER: {
    NAME: 'Scheduler',
    TAG: 'scheduler',
    ACCESS: {
      CREATE: [USER_ROLE.ADMIN, USER_ROLE.SDR],
      UPDATE: [USER_ROLE.ADMIN, USER_ROLE.SDR],
      DELETE: [USER_ROLE.ADMIN],
      READ: [USER_ROLE.ADMIN, USER_ROLE.SDR, USER_ROLE.MANAGER],
    },
  },
  REPORTS: {
    NAME: 'Reports',
    TAG: 'reports',
    ACCESS: {
      CREATE: [USER_ROLE.ADMIN],
      UPDATE: [USER_ROLE.ADMIN],
      DELETE: [USER_ROLE.ADMIN],
      READ: [USER_ROLE.ADMIN, USER_ROLE.SDR, USER_ROLE.MANAGER],
    },
  },
  ACTIVITY_FEATURE: {
    NAME: 'Activity Feature',
    TAG: 'activity_feature',
    ACCESS: {
      CREATE: [USER_ROLE.ADMIN],
      UPDATE: [USER_ROLE.ADMIN],
      DELETE: [USER_ROLE.ADMIN],
      READ: [USER_ROLE.MANAGER],
    },
  },
  SETUP_MANAGER: {
    NAME: 'Setup Manager',
    TAG: 'setup_manager',
    ACCESS: {
      CREATE: [USER_ROLE.ADMIN],
      UPDATE: [USER_ROLE.ADMIN],
      DELETE: [USER_ROLE.ADMIN],
      READ: [USER_ROLE.ADMIN],
    },
  },
  EMPLOYEE: {
    NAME: 'Employees',
    TAG: 'employees',
    ACCESS: {
      CREATE: [USER_ROLE.ADMIN, USER_ROLE.MANAGER],
      UPDATE: [USER_ROLE.ADMIN],
      DELETE: [USER_ROLE.ADMIN],
      READ: [USER_ROLE.ADMIN],
    },
  },
  USERS: {
    NAME: 'Users',
    TAG: 'users',
    ACCESS: {
      CREATE: [USER_ROLE.ADMIN],
      UPDATE: [USER_ROLE.ADMIN],
      DELETE: [USER_ROLE.ADMIN],
      READ: [USER_ROLE.ADMIN],
    },
  },
  DEPARTMENTS: {
    NAME: 'Departments',
    TAG: 'departments',
    ACCESS: {
      CREATE: [USER_ROLE.ADMIN],
      UPDATE: [USER_ROLE.ADMIN],
      DELETE: [USER_ROLE.ADMIN],
      READ: [USER_ROLE.ADMIN, USER_ROLE.SDR],
    },
  },
  PAYROLL: {
    NAME: 'Payroll',
    TAG: 'payroll',
    ACCESS: {
      CREATE: [USER_ROLE.ADMIN],
      UPDATE: [USER_ROLE.ADMIN],
      DELETE: [USER_ROLE.ADMIN],
      READ: [USER_ROLE.ADMIN, USER_ROLE.ADMIN],
    },
  },
  KPI: {
    NAME: 'KPI',
    TAG: 'kpi',
    ACCESS: {
      CREATE: [USER_ROLE.ADMIN],
      UPDATE: [USER_ROLE.ADMIN],
      DELETE: [USER_ROLE.ADMIN],
      READ: [USER_ROLE.ADMIN, USER_ROLE.STAFF],
    },
  },
  PRICING: {
    NAME: 'Pricing',
    TAG: 'pricing',
    ACCESS: {
      CREATE: [USER_ROLE.ADMIN],
      UPDATE: [USER_ROLE.ADMIN],
      DELETE: [USER_ROLE.ADMIN],
      READ: [USER_ROLE.ADMIN, USER_ROLE.STAFF],
    },
  },
  APPROVAL: {
    NAME: 'Pending Approvals',
    TAG: 'pending_approval',
    ACCESS: {
      CREATE: [USER_ROLE.ADMIN],
      UPDATE: [USER_ROLE.ADMIN],
      DELETE: [USER_ROLE.ADMIN],
      READ: [USER_ROLE.ADMIN, USER_ROLE.STAFF],
    },
  },
};

const getEmployeeFeature = (hidden = false, role: any[]): NbMenuItem => {
  return {
    title: MENU_ITEM_NAME.EMPLOYEE.NAME,
    icon: 'people-outline',
    link: '/main/employee',
    hidden,
    data: {
      key: lowerCase(MENU_ITEM_NAME.EMPLOYEE.TAG),
      role,
      access: MENU_ITEM_NAME.EMPLOYEE.ACCESS,
    },
  };
};

const getUsersFeature = (hidden = false): NbMenuItem => {
  return {
    title: MENU_ITEM_NAME.USERS.NAME,
    icon: 'lock-outline',
    link: '/main/users',
    hidden,
    data: {
      key: lowerCase(MENU_ITEM_NAME.USERS.TAG),
      role: [USER_ROLE.ADMIN],
      access: MENU_ITEM_NAME.USERS.ACCESS,
    },
  };
};

const getDepartmentsFeature = (hidden = false): NbMenuItem => {
  return {
    title: MENU_ITEM_NAME.DEPARTMENTS.NAME,
    icon: 'grid-outline',
    link: '/main/departments',
    hidden,
    data: {
      key: lowerCase(MENU_ITEM_NAME.DEPARTMENTS.TAG),
      role: [USER_ROLE.ADMIN],
      access: MENU_ITEM_NAME.DEPARTMENTS.ACCESS,
    },
  };
};

const getCampaignFeature = (hidden = false): NbMenuItem => {
  return {
    title: MENU_ITEM_NAME.PRICING.NAME,
    icon: 'pricetags-outline',
    link: '/main/pricing',
    hidden,
    data: {
      key: lowerCase(MENU_ITEM_NAME.PRICING.TAG),
      role: [USER_ROLE.ADMIN],
      access: MENU_ITEM_NAME.PRICING.ACCESS,
    },
  };
};

const getPayrollFeature = (hidden = false): NbMenuItem => {
  return {
    title: MENU_ITEM_NAME.PAYROLL.NAME,
    icon: 'credit-card-outline',
    link: '/main/payroll',
    hidden,
    data: {
      key: lowerCase(MENU_ITEM_NAME.PAYROLL.TAG),
      role: [USER_ROLE.ADMIN],
      access: MENU_ITEM_NAME.PAYROLL.ACCESS,
    },
  };
};

const getKPIFeature = (hidden = false): NbMenuItem => {
  return {
    title: MENU_ITEM_NAME.KPI.NAME,
    icon: 'layers-outline',
    link: '/main/kpi',
    hidden,
    data: {
      key: lowerCase(MENU_ITEM_NAME.KPI.TAG),
      role: [USER_ROLE.ADMIN],
      access: MENU_ITEM_NAME.KPI.ACCESS,
    },
  };
};

const getApprovals = (hidden = false): NbMenuItem => {
  return {
    title: MENU_ITEM_NAME.APPROVAL.NAME,
    icon: 'person-done-outline',
    link: '/main/approvals',
    hidden,
    badge: {
      text: '0',
      status: 'primary',
    },
    data: {
      key: lowerCase(MENU_ITEM_NAME.APPROVAL.TAG),
      role: [USER_ROLE.ADMIN],
      access: MENU_ITEM_NAME.APPROVAL.ACCESS,
    },
  };
};

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: MENU_ITEM_NAME.DASHBOARD.NAME,
    icon: 'home-outline',
    link: '/main/dashboard',
    home: true,
    data: {
      key: lowerCase(MENU_ITEM_NAME.DASHBOARD.TAG),
      role: [USER_ROLE.ADMIN, USER_ROLE.SDR, USER_ROLE.MANAGER],
      access: MENU_ITEM_NAME.DASHBOARD.ACCESS,
    },
  },
  {
    title: 'FEATURES',
    group: true,
  },
  {
    title: MENU_ITEM_NAME.CAMPAIGN.NAME,
    icon: 'keypad-outline',
    link: '/main/campaign',
    data: {
      key: lowerCase(MENU_ITEM_NAME.CAMPAIGN.TAG),
      role: [USER_ROLE.ADMIN, USER_ROLE.SDR, USER_ROLE.MANAGER],
      access: MENU_ITEM_NAME.CAMPAIGN.ACCESS,
    },
  },
  {
    title: MENU_ITEM_NAME.SCHEDULER.NAME,
    icon: 'browser-outline',
    link: '/main/scheduler',
    data: {
      key: lowerCase(MENU_ITEM_NAME.SCHEDULER.TAG),
      role: [USER_ROLE.SDR],
      access: MENU_ITEM_NAME.SCHEDULER.ACCESS,
    },
  },
  {
    title: MENU_ITEM_NAME.REPORTS.NAME,
    icon: 'file-text-outline',
    data: {
      key: lowerCase(MENU_ITEM_NAME.REPORTS.TAG),
      role: [USER_ROLE.ADMIN, USER_ROLE.MANAGER, USER_ROLE.SDR],
      access: MENU_ITEM_NAME.REPORTS.ACCESS,
    },
  },
  {
    title: MENU_ITEM_NAME.ACTIVITY_FEATURE.NAME,
    icon: 'activity-outline',
    data: {
      key: lowerCase(MENU_ITEM_NAME.ACTIVITY_FEATURE.TAG),
      role: [USER_ROLE.ADMIN],
      access: MENU_ITEM_NAME.ACTIVITY_FEATURE.ACCESS,
    },
    children: [getPayrollFeature(), getKPIFeature()],
  },
  {
    title: MENU_ITEM_NAME.SETUP_MANAGER.NAME,
    icon: 'settings-2-outline',
    data: {
      key: lowerCase(MENU_ITEM_NAME.SETUP_MANAGER.TAG),
      role: [USER_ROLE.ADMIN],
      access: MENU_ITEM_NAME.SETUP_MANAGER.ACCESS,
    },
    children: [
      getEmployeeFeature(false, [USER_ROLE.ADMIN]),
      getUsersFeature(),
      getDepartmentsFeature(),
      getCampaignFeature(),
    ],
  },
  getEmployeeFeature(false, [USER_ROLE.MANAGER]),
  getEmployeeFeature(true, [USER_ROLE.ADMIN]),
  getUsersFeature(true),
  getDepartmentsFeature(true),
  getPayrollFeature(true),
  getKPIFeature(true),
  getCampaignFeature(true),
  getApprovals(),
];
