import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarManagerInfoComponent } from './sidebar-manager-info.component';

describe('SidebarManagerInfoComponent', () => {
  let component: SidebarManagerInfoComponent;
  let fixture: ComponentFixture<SidebarManagerInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SidebarManagerInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarManagerInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
