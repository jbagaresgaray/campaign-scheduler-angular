import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { IEmployees } from 'src/app/modules/main/models/employee.model';

@Component({
  selector: 'app-sidebar-manager-info',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './sidebar-manager-info.component.html',
  styleUrls: ['./sidebar-manager-info.component.scss'],
})
export class SidebarManagerInfoComponent implements OnInit {
  @Input() manager: IEmployees;

  constructor() {}

  ngOnInit(): void {}
}
