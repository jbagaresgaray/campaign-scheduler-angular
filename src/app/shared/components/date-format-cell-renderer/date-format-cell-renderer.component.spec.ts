import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DateFormatCellRendererComponent } from './date-format-cell-renderer.component';

describe('DateFormatCellRendererComponent', () => {
  let component: DateFormatCellRendererComponent;
  let fixture: ComponentFixture<DateFormatCellRendererComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DateFormatCellRendererComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DateFormatCellRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
