import { Component, ViewEncapsulation } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
  selector: 'app-date-format-cell-renderer',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './date-format-cell-renderer.component.html',
  styleUrls: ['./date-format-cell-renderer.component.scss'],
})
export class DateFormatCellRendererComponent
  implements ICellRendererAngularComp {
  params: any;
  date: any;

  constructor() {}

  ngOnInit(): void {}

  agInit(params: any): void {
    this.params = params;
    if (params && params.data) {
      this.date = params.data.date || params.data.started;
    }
  }

  refresh(): boolean {
    return false;
  }
}
