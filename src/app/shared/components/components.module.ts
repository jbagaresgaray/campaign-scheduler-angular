import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  NbCardModule,
  NbFormFieldModule,
  NbIconModule,
  NbInputModule,
  NbSidebarModule,
  NbTabsetModule,
  NbUserModule,
} from '@nebular/theme';

import { NgxEchartsModule } from 'ngx-echarts';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { FormsModule } from '@angular/forms';
import { ClipboardModule } from 'ngx-clipboard';
import { IonicModule } from '@ionic/angular';

import { PipesModule } from '../pipes/pipes.module';

import { AuthBlockComponent } from './auth-block/auth-block.component';
import { BackHeaderComponent } from './back-header/back-header.component';
import { SearchBarInputComponent } from './search-bar-input/search-bar-input.component';
import { SkeletonTextComponent } from './skeleton-text/skeleton-text.component';
import { NoAssignedCampaignsComponent } from './no-assigned-campaigns/no-assigned-campaigns.component';
import { InfoButtonComponent } from './info-button/info-button.component';
import { DashboardCardComponent } from './dashboard-card/dashboard-card.component';
import { NoAssignedSchedulerComponent } from './no-assigned-scheduler/no-assigned-scheduler.component';
import { NoAssignedSdrComponent } from './no-assigned-sdr/no-assigned-sdr.component';
import { DashboardChartsComponent } from './dashboard-charts/dashboard-charts.component';
import { StatusCellRendererComponent } from './status-cell-renderer/status-cell-renderer.component';
import { PaginationComponent } from './pagination/pagination.component';
import { AvatarCellRendererComponent } from './avatar-cell-renderer/avatar-cell-renderer.component';
import { ContentModalComponent } from './content-modal/content-modal.component';
import { SidebarCampaignInfoComponent } from './sidebar-campaign-info/sidebar-campaign-info.component';
import { SidebarManagerInfoComponent } from './sidebar-manager-info/sidebar-manager-info.component';
import { RoleCellRendererComponent } from './role-cell-renderer/role-cell-renderer.component';
import { CurrencyCellRendererComponent } from './currency-cell-renderer/currency-cell-renderer.component';
import { DateFormatCellRendererComponent } from './date-format-cell-renderer/date-format-cell-renderer.component';

const COMPONENTS = [
  AuthBlockComponent,
  BackHeaderComponent,
  SearchBarInputComponent,
  SkeletonTextComponent,
  NoAssignedCampaignsComponent,
  InfoButtonComponent,
  DashboardCardComponent,
  NoAssignedSchedulerComponent,
  NoAssignedSdrComponent,
  DashboardChartsComponent,
  StatusCellRendererComponent,
  PaginationComponent,
  AvatarCellRendererComponent,
  ContentModalComponent,
  SidebarCampaignInfoComponent,
  SidebarManagerInfoComponent,
  RoleCellRendererComponent,
  CurrencyCellRendererComponent,
  DateFormatCellRendererComponent,
];
@NgModule({
  declarations: [...COMPONENTS],
  imports: [
    CommonModule,
    NbCardModule,
    NbIconModule,
    NbFormFieldModule,
    NbInputModule,
    NbTabsetModule,
    NbSidebarModule,
    NgxEchartsModule,
    NgxChartsModule,
    PaginationModule,
    FormsModule,
    NbUserModule,
    PipesModule,
    ClipboardModule,
    IonicModule,
  ],
  exports: [...COMPONENTS],
})
export class ComponentsModule {}
