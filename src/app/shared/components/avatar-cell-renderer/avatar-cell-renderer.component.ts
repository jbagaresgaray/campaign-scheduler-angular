import { Component, ViewEncapsulation } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
  selector: 'app-avatar-cell-renderer',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './avatar-cell-renderer.component.html',
  styleUrls: ['./avatar-cell-renderer.component.scss'],
})
export class AvatarCellRendererComponent implements ICellRendererAngularComp {
  params: any;

  name: string;
  picture: string;

  constructor() {}

  agInit(params: any): void {
    this.params = params;
    if (params && params.data) {
      this.name = params.data.name || params.data.firstName;
      this.picture = params.data.imageUrl;
    }
  }

  refresh(): boolean {
    return false;
  }
}
