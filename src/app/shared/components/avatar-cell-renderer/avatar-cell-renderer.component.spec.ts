import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AvatarCellRendererComponent } from './avatar-cell-renderer.component';

describe('AvatarCellRendererComponent', () => {
  let component: AvatarCellRendererComponent;
  let fixture: ComponentFixture<AvatarCellRendererComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AvatarCellRendererComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AvatarCellRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
