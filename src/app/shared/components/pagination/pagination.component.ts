import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewEncapsulation,
} from '@angular/core';

@Component({
  selector: 'app-pagination',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
})
export class PaginationComponent implements OnInit {
  @Input() currentPage: number;
  @Input() itemsPerPage: number;
  @Input() totalItems: number;
  @Input() hasPager: boolean;
  @Input() disabled: boolean;

  // tslint:disable-next-line: no-output-on-prefix
  @Output() onPageChanged = new EventEmitter<any>();

  numOfPages = 0;
  maxSize = 10;

  constructor() {}

  ngOnInit(): void {}
}
