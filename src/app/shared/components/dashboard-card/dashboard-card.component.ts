import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-dashboard-card',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './dashboard-card.component.html',
  styleUrls: ['./dashboard-card.component.scss'],
})
export class DashboardCardComponent implements OnInit {
  @Input() title: string;
  @Input() subTitle: string;
  @Input() size: string;

  constructor() {}

  ngOnInit(): void {}
}
