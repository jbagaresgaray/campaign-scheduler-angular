import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import isEmpty from 'lodash-es/isEmpty';

import { ICampaign } from 'src/app/modules/main/models/campaign.model';
import { CAMPAIGN_CONTENT } from '../../constants/utils';
import { ContentModalComponent } from '../content-modal/content-modal.component';
import { ToastAlertService } from '../../services/toast-alert.service';

@Component({
  selector: 'app-sidebar-campaign-info',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './sidebar-campaign-info.component.html',
  styleUrls: ['./sidebar-campaign-info.component.scss'],
})
export class SidebarCampaignInfoComponent implements OnInit {
  @Input() campaign: ICampaign;

  readonly CAMPAIGN_CONTENT = CAMPAIGN_CONTENT;

  constructor(
    private modalService: BsModalService,
    private toastr: ToastAlertService
  ) {}

  ngOnInit(): void {}

  viewContent(key: string, title: string) {
    if (this.campaign && this.campaign.content) {
      const initialState = {
        content: this.campaign.content[key],
        title,
      };
      this.modalService.show(ContentModalComponent, { initialState });
    } else {
      return this.toastr.showWarningToast(
        'WARNING',
        'No campaign content! Please contact the administration.'
      );
    }
  }
}
