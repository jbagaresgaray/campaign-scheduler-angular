import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarCampaignInfoComponent } from './sidebar-campaign-info.component';

describe('SidebarCampaignInfoComponent', () => {
  let component: SidebarCampaignInfoComponent;
  let fixture: ComponentFixture<SidebarCampaignInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SidebarCampaignInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarCampaignInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
