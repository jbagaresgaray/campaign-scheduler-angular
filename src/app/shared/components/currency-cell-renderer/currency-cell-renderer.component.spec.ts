import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrencyCellRendererComponent } from './currency-cell-renderer.component';

describe('CurrencyCellRendererComponent', () => {
  let component: CurrencyCellRendererComponent;
  let fixture: ComponentFixture<CurrencyCellRendererComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CurrencyCellRendererComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrencyCellRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
