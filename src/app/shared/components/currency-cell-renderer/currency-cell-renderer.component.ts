import { Component, ViewEncapsulation } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
  selector: 'app-currency-cell-renderer',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './currency-cell-renderer.component.html',
  styleUrls: ['./currency-cell-renderer.component.scss'],
})
export class CurrencyCellRendererComponent implements ICellRendererAngularComp {
  params: any;
  pricing: any;

  constructor() {}

  ngOnInit(): void {}

  agInit(params: any): void {
    this.params = params;
    if (params && params.data) {
      this.pricing = params.data.pricing;
    }
  }

  refresh(): boolean {
    return false;
  }
}
