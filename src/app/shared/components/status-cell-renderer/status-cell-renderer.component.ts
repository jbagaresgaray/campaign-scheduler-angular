import { Component, ViewEncapsulation } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { EUserAccountStatus } from '../../constants/enums';
import { USER_ACCOUNT_STATUS } from '../../constants/utils';

@Component({
  selector: 'app-status-cell-renderer',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './status-cell-renderer.component.html',
  styleUrls: ['./status-cell-renderer.component.scss'],
})
export class StatusCellRendererComponent implements ICellRendererAngularComp {
  params: any;
  statusId: any;

  constructor() {}

  agInit(params: any): void {
    this.params = params;
    if (params && params.data) {
      this.statusId = params.data.statusId;
    }
  }

  refresh(): boolean {
    return false;
  }

  getStatus(statusId: EUserAccountStatus) {
    switch (statusId) {
      case EUserAccountStatus.ACTIVE:
        return {
          class: 'success',
          label: USER_ACCOUNT_STATUS.ACTIVE,
        };
      case EUserAccountStatus.SUSPENDED:
        return {
          class: 'warning',
          label: USER_ACCOUNT_STATUS.SUSPENDED,
        };
      case EUserAccountStatus.TERMINATED:
        return {
          class: 'danger',
          label: USER_ACCOUNT_STATUS.TERMINATED,
        };
      case EUserAccountStatus.WAITING:
        return {
          class: 'info',
          label: USER_ACCOUNT_STATUS.WAITING,
        };
    }
  }
}
