import { Component, ViewEncapsulation } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { USER_ROLE, USER_ROLE_DESC } from '../../constants/utils';

@Component({
  selector: 'app-role-cell-renderer',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './role-cell-renderer.component.html',
  styleUrls: ['./role-cell-renderer.component.scss'],
})
export class RoleCellRendererComponent implements ICellRendererAngularComp {
  params: any;
  roleId: any;

  constructor() {}

  agInit(params: any): void {
    this.params = params;
    if (params && params.data) {
      this.roleId = params.data.roleId;
    }
  }

  refresh(): boolean {
    return false;
  }

  getStatus(roleId: number) {
    switch (roleId) {
      case USER_ROLE.ADMIN:
        return {
          class: 'success',
          label: USER_ROLE_DESC.ADMIN,
        };
      case USER_ROLE.MANAGER:
        return {
          class: 'warning',
          label: USER_ROLE_DESC.MANAGER,
        };
      case USER_ROLE.SDR:
        return {
          class: 'primary',
          label: USER_ROLE_DESC.SDR,
        };
      case USER_ROLE.STAFF:
        return {
          class: 'danger',
          label: USER_ROLE_DESC.STAFF,
        };
    }
  }
}
