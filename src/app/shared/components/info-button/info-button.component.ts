import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NbSidebarService } from '@nebular/theme';

@Component({
  selector: 'app-info-button',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './info-button.component.html',
  styleUrls: ['./info-button.component.scss'],
})
export class InfoButtonComponent implements OnInit {
  constructor(private sidebarService: NbSidebarService) {}

  ngOnInit(): void {}

  toggleSidebar(): boolean {
    this.sidebarService.toggle(false, 'settings-sidebar');
    return false;
  }
}
