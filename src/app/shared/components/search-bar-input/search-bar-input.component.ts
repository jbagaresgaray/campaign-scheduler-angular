import {
  Component,
  ElementRef,
  EventEmitter,
  OnInit,
  Output,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import isEmpty from 'lodash-es/isEmpty';

@Component({
  selector: 'app-search-bar-input',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './search-bar-input.component.html',
  styleUrls: ['./search-bar-input.component.scss'],
})
export class SearchBarInputComponent implements OnInit {
  @Output() onClear = new EventEmitter<any>();
  @Output() onEnter = new EventEmitter<any>();
  @Output() onInput = new EventEmitter<any>();

  showCloseButton = false;

  @ViewChild('searchInput') searchInput: ElementRef;

  constructor() {}

  ngOnInit(): void {}

  onChangeEvent(ev: any) {
    if (ev && ev.target) {
      const val = ev.target.value;
      this.showCloseButton = isEmpty(val) ? false : true;
    }

    this.onInput.emit(ev);
  }

  onClearEvent(ev: any) {
    this.searchInput.nativeElement.value = '';
    this.showCloseButton = isEmpty(this.searchInput.nativeElement.value)
      ? false
      : true;

    this.onClear.emit(ev);
  }
}
