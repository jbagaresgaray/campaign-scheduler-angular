import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoAssignedSdrComponent } from './no-assigned-sdr.component';

describe('NoAssignedSdrComponent', () => {
  let component: NoAssignedSdrComponent;
  let fixture: ComponentFixture<NoAssignedSdrComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NoAssignedSdrComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoAssignedSdrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
