import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-no-assigned-sdr',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './no-assigned-sdr.component.html',
  styleUrls: ['./no-assigned-sdr.component.scss'],
})
export class NoAssignedSdrComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
