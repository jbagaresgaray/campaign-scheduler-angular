import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ClipboardService, IClipboardResponse } from 'ngx-clipboard';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { ToastAlertService } from '../../services/toast-alert.service';

@Component({
  selector: 'app-content-modal',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './content-modal.component.html',
  styleUrls: ['./content-modal.component.scss'],
})
export class ContentModalComponent implements OnInit, OnDestroy {
  content: string;
  title: string;

  private unsubscribe$ = new Subject<any>();

  constructor(
    public bsModalRef: BsModalRef,
    private toastr: ToastAlertService,
    private clipboardService: ClipboardService
  ) {
    this.clipboardService.copyResponse$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res: IClipboardResponse) => {
        if (res.isSuccess) {
          this.toastr.showSuccessToast('Copied', null);
        }
      });
  }

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
