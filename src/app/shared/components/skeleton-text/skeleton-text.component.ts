import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-skeleton-text',

  templateUrl: './skeleton-text.component.html',
  styleUrls: ['./skeleton-text.component.scss'],
})
export class SkeletonTextComponent implements OnInit {
  @Input() animated: boolean;
  @Input() inMedia: boolean;

  constructor() {}

  ngOnInit(): void {}
}
