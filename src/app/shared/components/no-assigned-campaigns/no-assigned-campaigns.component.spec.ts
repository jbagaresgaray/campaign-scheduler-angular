import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoAssignedCampaignsComponent } from './no-assigned-campaigns.component';

describe('NoAssignedCampaignsComponent', () => {
  let component: NoAssignedCampaignsComponent;
  let fixture: ComponentFixture<NoAssignedCampaignsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NoAssignedCampaignsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoAssignedCampaignsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
