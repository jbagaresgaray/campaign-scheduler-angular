import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-no-assigned-campaigns',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './no-assigned-campaigns.component.html',
  styleUrls: ['./no-assigned-campaigns.component.scss'],
})
export class NoAssignedCampaignsComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
