import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-no-assigned-scheduler',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './no-assigned-scheduler.component.html',
  styleUrls: ['./no-assigned-scheduler.component.scss'],
})
export class NoAssignedSchedulerComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
