import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoAssignedSchedulerComponent } from './no-assigned-scheduler.component';

describe('NoAssignedSchedulerComponent', () => {
  let component: NoAssignedSchedulerComponent;
  let fixture: ComponentFixture<NoAssignedSchedulerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NoAssignedSchedulerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoAssignedSchedulerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
