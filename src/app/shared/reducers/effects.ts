import { AuthEffects } from '../../modules/auth/store/auth.effects';
import { MainEffects } from '../../modules/main/store/main.effects';

export const effectsList = [AuthEffects, MainEffects];
