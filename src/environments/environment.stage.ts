export const environment = {
  production: true,
  authBaseUrl: 'https://scheduler-auth-api.azurewebsites.net/api',
  userBaseUrl: 'https://scheduler-user-api.azurewebsites.net/api',
  coreBaseUrl: 'https://scheduler-core-api.azurewebsites.net/api',
  mainBaseUrl: 'https://scheduler-main-api.azurewebsites.net/api',
  blobBaseUrl: 'https://scheduler-azure-api.azurewebsites.net/api'
};
