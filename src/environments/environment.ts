// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  authBaseUrl: 'https://scheduler-auth-api.azurewebsites.net/api',
  userBaseUrl: 'https://scheduler-user-api.azurewebsites.net/api',
  coreBaseUrl: 'https://scheduler-core-api.azurewebsites.net/api',
  mainBaseUrl: 'https://scheduler-main-api.azurewebsites.net/api',
  blobBaseUrl: 'https://scheduler-azure-api.azurewebsites.net/api'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
