param(
    [string]$fireBaseToken,
    [string]$fireBaseProject
)
$dir = Split-Path $MyInvocation.MyCommand.Path
Push-Location $dir

npm i -g firebase-tools
write-host "starting deploy...";
Write-Host "Using an fireBaseToken: $(fireBaseToken)"
Write-Host "Using an fireBaseProject: $(fireBaseProject)"
firebase --version;
firebase deploy --token $fireBaseToken --project $fireBaseProject;
write-host "deployment completed";

Pop-Location
